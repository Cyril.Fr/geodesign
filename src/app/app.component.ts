import {Component, OnInit} from '@angular/core';
import localeEn from '@angular/common/locales/en';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {registerLocaleData} from '@angular/common';

@Component({
  selector: 'geodesign-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  constructor(private translate: TranslateService) {
    translate.setDefaultLang('fr');
    this.onLangChanged('fr');

    translate.use('fr');

    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.onLangChanged(event.lang);
    });
  }

  onLangChanged(lang: string): any {
    // LTR languages (Left To Right)

    switch (lang) {
      case 'fr':
        registerLocaleData(localeFr, 'fr', localeFrExtra);
        break;
      case 'en':
        registerLocaleData(localeEn, 'en');
        break;
      default:
        registerLocaleData(localeEn, 'en');
        break;
    }

  }
}
