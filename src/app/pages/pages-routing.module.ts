import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainPageComponent} from './main-page/main-page.component';
import {NetworkResolver} from '../core/resolvers/network.resolver';

const routes: Routes = [{
  path: ':instance',
  component: MainPageComponent,
  resolve: {data: NetworkResolver},
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
