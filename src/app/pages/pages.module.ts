import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PagesRoutingModule} from './pages-routing.module';
import {MainPageComponent} from './main-page/main-page.component';
import {PanelDataComponent} from './main-page/components/panel-data/panel-data.component';
import {MapComponent} from './main-page/components/map/map.component';
import {ToolbarComponent} from './main-page/components/map/toolbar/toolbar.component';
import {EmitPopupComponent} from './main-page/components/map/emit-popup/emit-popup.component';
import {PanelLineComponent} from './main-page/components/panel-line/panel-line.component';
import {SearchBarComponent} from './main-page/components/panel-line/search-bar/search-bar.component';
import {PeriodsComponent} from './main-page/components/panel-line/periods/periods.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {CoreModule} from '../core/core.module';
import {TranslateModule} from '@ngx-translate/core';
import {StopCardComponent} from './main-page/components/panel-data/stop-card/stop-card.component';
import {LineCardComponent} from './main-page/components/panel-data/line-card/line-card.component';
import {EmitterGeneratorCardComponent} from './main-page/components/panel-data/emitter-generator-card/emitter-generator-card.component';
import {MobilityServiceCardComponent} from './main-page/components/panel-data/mobility-service-card/mobility-service-card.component';
import {SocioDemoCardComponent} from './main-page/components/panel-data/socio-demo-card/socio-demo-card.component';
import {FluxCardComponent} from './main-page/components/panel-data/flux-card/flux-card.component';
import {IsochronesCardComponent} from './main-page/components/panel-data/isochrones-card/isochrones-card.component';
import {DragScrollModule} from 'ngx-drag-scroll';
import {AngularMaterialModule} from '../angular-material.module';
import {LineStatisticsComponent} from './main-page/components/panel-line/line-statistics/line-statistics.component';
import {LineTerritoryComponent} from './main-page/components/panel-line/line-territory/line-territory.component';
import {LineActionsComponent} from './main-page/components/panel-line/line-actions/line-actions.component';
import {LineCommercialSpeedComponent} from './main-page/components/panel-line/line-commercial-speed/line-commercial-speed.component';
import {LinePeriodsComponent} from './main-page/components/panel-line/line-periods/line-periods.component';
import {LineAntennaComponent} from './main-page/components/panel-line/line-antenna/line-antenna.component';
import {LineRoundTripComponent} from './main-page/components/panel-line/line-round-trip/line-round-trip.component';
import {AddPointsPipe} from '../core/pipes/add-points.pipe';
import {StopPopupComponent} from './main-page/components/panel-data/stop-card/stop-popup/stop-popup.component';
import {StopPopupParametersComponent}
from './main-page/components/panel-data/stop-card/stop-popup-parameters/stop-popup-parameters.component';
import {LineCreationComponent} from './main-page/components/panel-line/line-creation/line-creation.component';
import {ServiceMobPopupComponent} from './main-page/components/map/service-mob-popup/service-mob-popup.component';
import {LineTriggeringComponent} from './main-page/components/panel-line/line-triggering/line-triggering.component';
import {LineNumberRoundTripComponent}
  from './main-page/components/panel-line/line-periods/line-number-round-trip/line-number-round-trip.component';
import { DndModule } from 'ngx-drag-drop';

@NgModule({
  declarations: [
    MainPageComponent,
    PanelDataComponent,
    MapComponent,
    ToolbarComponent,
    EmitPopupComponent,
    PanelLineComponent,
    SearchBarComponent,
    PeriodsComponent,
    StopCardComponent,
    LineCardComponent,
    EmitterGeneratorCardComponent,
    MobilityServiceCardComponent,
    SocioDemoCardComponent,
    FluxCardComponent,
    IsochronesCardComponent,
    LineStatisticsComponent,
    LineTerritoryComponent,
    LineActionsComponent,
    LineCommercialSpeedComponent,
    LinePeriodsComponent,
    LineAntennaComponent,
    LineRoundTripComponent,
    AddPointsPipe,
    StopPopupComponent,
    StopPopupParametersComponent,
    LineCreationComponent,
    ServiceMobPopupComponent,
    LineTriggeringComponent,
    LineNumberRoundTripComponent,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule,
    CoreModule,
    ReactiveFormsModule,
    FormsModule,
    LeafletModule.forRoot(),
    TranslateModule,
    DragScrollModule,
    AngularMaterialModule,
    DndModule,
  ],
})

export class PagesModule {
}
