import {Component, Input, OnInit} from '@angular/core';
import {Option} from '../../../../../core/models/data-panel.model';
import {MapService} from '../../../../../core/services/map/map.service';
import {EmitsLayerService} from '../../../../../core/services/map/emits/emits-layer.service';
import {EmitsDataService} from '../../../../../core/services/map/emits/emits-data.service';
import {DataPanelService} from '../../../../../core/services/data-panel.service';

@Component({
  selector: 'geodesign-emitter-generator-card',
  templateUrl: './emitter-generator-card.component.html',
  styleUrls: ['./emitter-generator-card.component.scss'],
})
export class EmitterGeneratorCardComponent implements OnInit {

  @Input() text: string;

  options: Option[] = [
    {label: 'DATA_PANEL_EMIT_GEN.ALL', value: ''},
    {label: 'DATA_PANEL_EMIT_GEN.TRADE', value: 'Commerces'},
    {label: 'DATA_PANEL_EMIT_GEN.H_COLLECTIF', value: 'Habitats dense'},
    {label: 'DATA_PANEL_EMIT_GEN.H_HOUSE', value: 'Habitats pavillonaire'},
    {label: 'DATA_PANEL_EMIT_GEN.HOBBY', value: 'Loisirs'},
    {label: 'DATA_PANEL_EMIT_GEN.HEALTH', value: 'Santés'},
    {label: 'DATA_PANEL_EMIT_GEN.SCHOOL', value: 'Scolaires'},
    {label: 'DATA_PANEL_EMIT_GEN.SERVICE', value: 'ZA tertiaire'},
    {label: 'DATA_PANEL_EMIT_GEN.TRANSPORT', value: 'Transports'},
    {label: 'DATA_PANEL_EMIT_GEN.INDUSTRIAL', value: 'ZI'},
    {label: 'DATA_PANEL_EMIT_GEN.LVL1', value: 'DATA_PANEL_EMIT_GEN.LVL1'},
    {label: 'DATA_PANEL_EMIT_GEN.LVL2', value: 'DATA_PANEL_EMIT_GEN.LVL2'},
    {label: 'DATA_PANEL_EMIT_GEN.PROJECT', value: 't'},
    {label: 'DATA_PANEL_EMIT_GEN.NO_PROJECT', value: 'f'},
  ];

  selectedOption: string = '';
  show: boolean = false;

  private _sliderNumberValue: number = 1;
  set sliderNumberValue(val: number) {
    this._sliderNumberValue = val;
    this.emitsLayerService.sliderValue = val;
    if (this.show) {
      this.showEmits(true);
    }
  }

  get sliderNumberValue(): number {
    return this._sliderNumberValue;
  }

  private _assetName: string;
  @Input('assetName')
  set assetName(str: string) {
    this._assetName = str;
  }

  get assetName(): string {
    return this._assetName;
  }

  get assetSrc(): string {
    return `/assets/images/panelData/${this._assetName}`;
  }

  constructor(private mapService: MapService,
              private emitsLayerService: EmitsLayerService,
              private emitsDataService: EmitsDataService,
              private dataPanelService: DataPanelService) {
  }

  ngOnInit(): void {
  }

  handleAdd(): void {
  }

  handleDisplay(): void {
    if (!this.show) {
      this.show = true;
      this.showEmits();
    } else {
      this.show = false;
      this.dataPanelService.hideLayer('emits');
    }
  }

  showEmits(forceRecreate: boolean = false): void {
    this.emitsDataService.filter = this.getFilterType();
    if (forceRecreate) {
      this.dataPanelService.hideLayer('emits');
    }
    this.dataPanelService.showLayer('emits', forceRecreate);
  }

  handleLoad(): void {
    // TODO
  }

  onOptionChange(option: string): void {
    this.selectedOption = option;
    if (this.show) {
      this.showEmits(true);
    }
  }

  getFilterType(): string {
    if (!this.selectedOption) {
      return null;
    }
    if (this.options.find((option: Option) => option.value === this.selectedOption)) {
      return this.selectedOption;
    }
  }
}
