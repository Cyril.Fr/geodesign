import {Component, Input, OnInit} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import {DATA_SIDENAV_WIDTH} from '../../../../core/constants/popup.const';
import {IsochronesLayerService} from '../../../../core/services/map/isochrones/isochrones-layer.service';

@Component({
  selector: 'geodesign-panel-data',
  templateUrl: './panel-data.component.html',
  styleUrls: ['./panel-data.component.scss'],
})
export class PanelDataComponent implements OnInit {

  sidenavWidth: string = DATA_SIDENAV_WIDTH + 'px';
  @Input() sidenav: MatSidenav;

  constructor(public isochroneService: IsochronesLayerService) {
  }

  ngOnInit(): void {
  }
}
