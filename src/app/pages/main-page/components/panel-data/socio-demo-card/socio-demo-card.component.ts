import {Component, Input, OnInit} from '@angular/core';
import {Option} from '../../../../../core/models/data-panel.model';
import {InseeLayerService} from '../../../../../core/services/map/insee/insee-layer.service';
import {DataPanelService} from '../../../../../core/services/data-panel.service';

@Component({
  selector: 'geodesign-socio-demo-card',
  templateUrl: './socio-demo-card.component.html',
})
export class SocioDemoCardComponent implements OnInit {

  options: Option[] = [
    {label: 'DATA_PANEL_SOCIO_DEMO.POP', value: 'DATA_PANEL_SOCIO_DEMO.POP'},
    {label: 'DATA_PANEL_SOCIO_DEMO.EMP', value: 'DATA_PANEL_SOCIO_DEMO.EMP'},
  ];

  selectedOption: string = this.options[0].label;
  show: boolean = false;

  private _assetName: string;
  @Input('assetName')
  set assetName(str: string) {
    this._assetName = str;
  }

  get assetName(): string {
    return this._assetName;
  }

  get assetSrc(): string {
    return `/assets/images/panelData/${this._assetName}`;
  }

  @Input() text: string;

  constructor(private inseeLayerService: InseeLayerService,
              private dataPanelService: DataPanelService) {
  }

  ngOnInit(): void {
  }

  handleDisplay(): void {
    if (!this.show) {
      this.show = true;
    } else {
      this.show = !this.show;
    }
    this.updateMap();
  }

  handleHelp(): void {
    // TODO
  }

  updateMap(inputShow?: boolean, additionalKey?: string): void {
    const show: boolean = typeof inputShow === 'boolean' ? inputShow : this.show;
    switch (this.selectedOption) {
      case 'DATA_PANEL_SOCIO_DEMO.POP':
        show ? this.dataPanelService.showLayer('insee', false, 'p') : this.dataPanelService.hideLayer('insee', additionalKey || 'p');
        break;
      case 'DATA_PANEL_SOCIO_DEMO.EMP':
        show ? this.dataPanelService.showLayer('insee', false, 'e') : this.dataPanelService.hideLayer('insee', additionalKey || 'e');
        break;
    }
  }

  onOptionChange(option: string): void {
    const key: string = option === 'DATA_PANEL_SOCIO_DEMO.POP' ? 'e' : 'p';
    this.updateMap(false, key);
    this.selectedOption = option;
    this.updateMap();
  }
}
