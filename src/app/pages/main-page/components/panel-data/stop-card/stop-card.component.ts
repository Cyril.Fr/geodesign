import {Component, Input, OnInit} from '@angular/core';
import {StopOption} from '../../../../../core/models/data-panel.model';
import {StopsLayerService} from '../../../../../core/services/map/stops-layer.service';
import {CoversLayerService} from '../../../../../core/services/map/covers/covers-layer.service';
import {MouvementsLayerService} from '../../../../../core/services/map/mouvements-layer.service';
import {PopempLayerService} from '../../../../../core/services/map/popemp-layer.service';
import {NbCoursesLayerService} from '../../../../../core/services/map/nb-courses/nb-courses-layer.service';
import {DataPanelService} from '../../../../../core/services/data-panel.service';

@Component({
  selector: 'geodesign-stop-card',
  templateUrl: './stop-card.component.html',
})
export class StopCardComponent implements OnInit {
  @Input('assetName')
  set assetName(str: string) {
    this._assetName = str;
  }

  get assetName(): string {
    return this._assetName;
  }

  get assetSrc(): string {
    return `/assets/images/panelData/${this._assetName}`;
  }

  get option(): any {
    return this.options.find((option: StopOption) => option.value === this.assetName);
  }

  constructor(private stopsLayerService: StopsLayerService,
              private coversLayerService: CoversLayerService,
              private mouvementsLayerService: MouvementsLayerService,
              private popEmpService: PopempLayerService,
              private nbCoursesLayerService: NbCoursesLayerService,
              private dataPanelService: DataPanelService) {
  }

  options: StopOption[] = [
    {label: 'DATA_PANEL_STOP.STOP_POINTS', value: 'stops_prox.PNG', display: true, add: true, load: false, param: false},
    {label: 'DATA_PANEL_STOP.DESERVED', value: 'stops_basic.PNG', display: true, add: false, load: false, param: false},
    {label: 'DATA_PANEL_STOP.COVER', value: 'stops_couverture.PNG', display: true, add: false, load: false, param: false},
    {label: 'DATA_PANEL_STOP.UP_DOWN', value: 'stops_MD.PNG', display: true, add: false, load: false, param: true},
    {label: 'DATA_PANEL_STOP.MOVEMENTS', value: 'stops_mvt.png', display: true, add: false, load: false, param: true},
    {label: 'DATA_PANEL_STOP.LOW_MOVEMENTS', value: 'stops_low.PNG', display: true, add: false, load: false, param: true},
    {label: 'DATA_PANEL_STOP.JOB_PROPORTION', value: 'stops_popEmp.PNG', display: true, add: false, load: false, param: false},
    {label: 'DATA_PANEL_STOP.COUNT_STOP_NBR', value: 'stops_courses.PNG', display: true, add: false, load: false, param: false},
    {label: 'DATA_PANEL_STOP.RACE_NBR_COMP', value: 'stops_evolCourses.PNG', display: true, add: false, load: false, param: true},
  ];

  show: boolean = false;

  private _assetName: string;

  @Input() text: string;

  ngOnInit(): void {
  }

  handleAdd(): void {
    // TODO
  }

  handleDisplay(): void {
    if (!this.show) {
      this.show = true;
    } else {
      this.show = !this.show;
    }
    this.updateMap();
  }

  updateMap(inputShow?: boolean): void {
    const show: boolean = typeof inputShow === 'boolean' ? inputShow : this.show;
    switch (this.option.label) {
      case 'DATA_PANEL_STOP.STOP_POINTS':
        show ? this.stopsLayerService.showStops() : this.stopsLayerService.hideStops();
        break;
      case 'DATA_PANEL_STOP.DESERVED':
        show ? this.stopsLayerService.showDesserveStops() : this.stopsLayerService.hideDesserveStops();
        break;
      case 'DATA_PANEL_STOP.COVER':
        show ? this.dataPanelService.showLayer('covers') : this.dataPanelService.hideLayer('covers');
        break;
      case 'DATA_PANEL_STOP.MOVEMENTS':
        show ? this.mouvementsLayerService.showMouvements() : this.mouvementsLayerService.hideMouvements();
        break;
      case 'DATA_PANEL_STOP.UP_DOWN':
        show ? this.mouvementsLayerService.showMonteeDescente() : this.mouvementsLayerService.hideMonteeDescente();
        break;
      case 'DATA_PANEL_STOP.JOB_PROPORTION':
        show ? this.popEmpService.showPopEmp() : this.popEmpService.hidePopEmp();
        break;
      case 'DATA_PANEL_STOP.LOW_MOVEMENTS':
        show ? this.mouvementsLayerService.showLowMouvements() : this.mouvementsLayerService.hideLowMouvements();
        break;
      case 'DATA_PANEL_STOP.COUNT_STOP_NBR':
        show ? this.dataPanelService.showLayer('nbCourses') : this.dataPanelService.showLayer('nbCourses');
        break;
    }
  }

  handleLoad(): void {
    // TODO
  }

  handleParam(): void {
    // TODO
  }

  onOptionChange(option: string): void {
    this.updateMap(false);
    this.assetName = option;
    this.updateMap();
  }
}
