import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {StopsLayerService} from '../../../../../../core/services/map/stops-layer.service';
import {StopPopupInfo} from '../../../../../../core/models/stops.model';
import {LeafletMouseEvent} from 'leaflet';
import {STOPS_POPUP_SIZE} from '../../../../../../core/constants/popup.const';
import {GeoJsonStopFeature} from '../../../../../../core/models/map/stops.model';

@Component({
  selector: 'geodesign-stop-popup',
  templateUrl: './stop-popup.component.html',
  styleUrls: ['./stop-popup.component.scss'],
})
export class StopPopupComponent implements OnInit {

  name: string;
  feature: GeoJsonStopFeature;
  infos: StopPopupInfo;
  mouseEvent: LeafletMouseEvent;
  xStrPosition: string;
  yStrPosition: string;
  popupWidth: string = `${STOPS_POPUP_SIZE.X}px`;
  popupHeight: string = `${STOPS_POPUP_SIZE.Y}px`;
  show: boolean = false;
  id: number;
  popupExtendend: boolean = false;

  constructor(public stopsLayerService: StopsLayerService) {
    this.stopsLayerService.stopPopupInfos$.subscribe((popupInfos: any) => {
      if (popupInfos) {
        this.name = popupInfos.name;
        this.id = popupInfos.id;
        this.feature = popupInfos.feature;
        this.infos = popupInfos.infos;
        this.mouseEvent = popupInfos.mouseEvent;
        if (popupInfos.mouseEvent && popupInfos.mouseEvent.containerPoint) {
          this.xStrPosition = `${popupInfos.mouseEvent.containerPoint.x}px`;
          this.yStrPosition = `${popupInfos.mouseEvent.containerPoint.y}px`;
        }
        this.handlePopupHeight();
        this.show = popupInfos.show;
      }
    });
  }

  ngOnInit(): void {
  }

  async onExpand(): Promise<void> {
    this.stopsLayerService.extendedPopup = !this.stopsLayerService.extendedPopup;
    await this.stopsLayerService.showStopPopup(null, !this.stopsLayerService.extendedPopup);
    this.handlePopupHeight();
  }

  handlePopupHeight(): void {
    if (this.stopsLayerService.extendedPopup) {
      this.popupHeight = `${STOPS_POPUP_SIZE.Y_EXTENDED}px`;
      this.popupExtendend = true;
    } else {
      this.popupHeight = `${STOPS_POPUP_SIZE.Y}px`;
      this.popupExtendend = false;
    }
  }
}
