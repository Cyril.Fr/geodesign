import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Option} from '../../../../../../core/models/data-panel.model';
import {NetworkService} from '../../../../../../core/services/network.service';
import {Network} from '../../../../../../core/models/network.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {StopService} from '../../../../../../core/services/stop.service';
import {StopsLayerService} from '../../../../../../core/services/map/stops-layer.service';
import {GeoJsonStopFeature} from '../../../../../../core/models/map/stops.model';

@Component({
  selector: 'geodesign-stop-popup-parameters',
  templateUrl: './stop-popup-parameters.component.html',
  styleUrls: ['./stop-popup-parameters.component.scss'],
})
export class StopPopupParametersComponent implements OnInit {

  @Input() expanded: boolean;
  @Output() expand: EventEmitter<any> = new EventEmitter<any>();

  private _stop: GeoJsonStopFeature;
  @Input('stop')
  set stop(value: GeoJsonStopFeature) {
    this._stop = value;
    if (value) {
      this.stopId = this._stop.properties.id;
      if (this._stop.geometry.coordinates && typeof this._stop.geometry.coordinates[0] === 'number') {
        this.stopLat = this._stop.geometry.coordinates[0];
        this.stopLng = this._stop.geometry.coordinates[1];
      }
      this.stopParametersFormGroup.patchValue({
        name: this._stop.properties.name,
        color: this._stop.properties.color,
        lat: this.stopLat,
        lng: this.stopLng,
      });
    }
  }

  get stop(): GeoJsonStopFeature {
    return this._stop;
  }

  stopParametersFormGroup: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    color: new FormControl('', [Validators.required]),
    lat: new FormControl('', [Validators.required, Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')]),
    lng: new FormControl('', [Validators.required, Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')]),
    scenarios: new FormControl([]),
    sourcebase: new FormControl(false),
  });

  stopId: number;
  stopLat: any;
  stopLng: any;
  options: Option[];

  constructor(private networkService: NetworkService,
              private stopService: StopService,
              private stopLayerService: StopsLayerService) {
    if (this.networkService.networks) {
      this.updateOptions(this.networkService.networks);
    } else {
      this.networkService.getNetworks().subscribe((networks: Network[]) => this.updateOptions(networks));
    }
  }

  ngOnInit(): void {
  }

  updateOptions(networks: Network[]): void {
    this.options = networks.map((network: Network) => ({label: network.name, value: network.name}));
  }

  onExpand(): void {
    this.expand.emit();
  }

  async onSubmit(): Promise<void> {
    this.stopParametersFormGroup.markAllAsTouched();
    if (!this.stopParametersFormGroup.invalid) {
      const body: any = this.createBody();
      await this.stopService.setStopInfo(body).toPromise();
      const newStopFeature: GeoJsonStopFeature = this.createNewFeature();
      this.stopLayerService.closeStopPopup();
      this.stopLayerService.updateStopInfo(newStopFeature);
    }
  }

  createBody(): object {
    return {
      schema: NetworkService.getNetworkName(),
      stopId: this.stopId,
      stopName: this.stopParametersFormGroup.get('name').value,
      x: this.stopParametersFormGroup.get('lat').value,
      y: this.stopParametersFormGroup.get('lng').value,
      color: this.stopParametersFormGroup.get('color').value,
      sch: this.stopParametersFormGroup.get('scenarios').value,
      sch_src: this.stopParametersFormGroup.get('sourcebase').value,
    };
  }

  createNewFeature(): GeoJsonStopFeature {
    return {
      type: this._stop.type,
      properties: {
        ...this._stop.properties,
        name: this.stopParametersFormGroup.get('name').value,
        color: this.stopParametersFormGroup.get('color').value,
      },
      geometry: {
        ...this._stop.geometry,
        coordinates: [
          parseFloat(this.stopParametersFormGroup.get('lat').value),
          parseFloat(this.stopParametersFormGroup.get('lng').value),
        ],
      },
    };
  }
}
