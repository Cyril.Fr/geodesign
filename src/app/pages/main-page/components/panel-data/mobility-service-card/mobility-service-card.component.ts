import {Component, Input, OnInit} from '@angular/core';
import {Option} from '../../../../../core/models/data-panel.model';
import {MobserviceLayerService} from '../../../../../core/services/map/mobility-service/mobservice-layer.service';
import {TranslateService} from '@ngx-translate/core';
import {MobserviceDataService} from '../../../../../core/services/map/mobility-service/mobservice-data.service';

@Component({
  selector: 'geodesign-mobility-service-card',
  templateUrl: './mobility-service-card.component.html',
  styleUrls: ['./mobility-service-card.component.scss'],
})
export class MobilityServiceCardComponent implements OnInit {
  set sliderNumberValue(val: number) {
    this._sliderNumberValue = val;
    this.mobserviceLayerService.sliderValue = val;
    if (this.show) {
      this.showServiceMobs();
    }
  }

  get sliderNumberValue(): number {
    return this._sliderNumberValue;
  }
  @Input('assetName')
  set assetName(str: string) {
    this._assetName = str;
  }

  get assetName(): string {
    return this._assetName;
  }

  get assetSrc(): string {
    return `/assets/images/panelData/${this._assetName}`;
  }

  constructor(private mobserviceLayerService: MobserviceLayerService,
              private mobserviceDataService: MobserviceDataService,
              private translate: TranslateService) {
  }

  static options: (Option | {label: string, value: number})[] = [
    {label: 'DATA_PANEL_MOBILITY_SERVICE.ALL', value: ''},
    {label: 'DATA_PANEL_MOBILITY_SERVICE.CARPOOLING', value: 'aire_covoit'},
    {label: 'DATA_PANEL_MOBILITY_SERVICE.PR', value: 'p_r'},
    {label: 'DATA_PANEL_MOBILITY_SERVICE.AUTOSHARE', value: 'autopartage'},
    {label: 'DATA_PANEL_MOBILITY_SERVICE.BIKE_PARKING', value: 'parking_velo'},
    {label: 'DATA_PANEL_MOBILITY_SERVICE.SCOOTER_PARKING', value: 'parking_trot'},
    {label: 'DATA_PANEL_MOBILITY_SERVICE.EXCHANGE', value: 'pem'},
    {label: 'DATA_PANEL_MOBILITY_SERVICE.CONSTRUCTION', value: 'zone_travaux'},
    {label: 'DATA_PANEL_MOBILITY_SERVICE.LVL1', value: 1},
  ];

  @Input() text: string;

  selectedOption: string = '';
  show: boolean = false;
  options: (Option | {label: string, value: number})[] = MobilityServiceCardComponent.options;

  private _sliderNumberValue: number = 1;

  private _assetName: string;

  ngOnInit(): void {
  }

  handleDisplay(): void {
    if (this.show) {
      this.hideServiceMobs();
    } else {
      this.showServiceMobs();
    }
    this.show = !this.show;
  }

  showServiceMobs(): void {
    this.mobserviceLayerService.showServiceMobs();
  }

  hideServiceMobs(): void {
    this.mobserviceLayerService.hideServiceMobs();
  }

  onOptionChange(newOption: string): void {
    this.selectedOption = newOption;
    this.mobserviceDataService.currentFilter = newOption;
    if (this.show) {
      this.showServiceMobs();
    }
  }
}
