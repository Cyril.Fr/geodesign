import {Component, Input, OnInit} from '@angular/core';
import {Option} from '../../../../../core/models/data-panel.model';

@Component({
  selector: 'geodesign-flux-card',
  templateUrl: './flux-card.component.html',
})
export class FluxCardComponent implements OnInit {
  options: Option[] = [];

  selectedOption: string;

  private _assetName: string;
  @Input('assetName')
  set assetName(str: string) {
    this._assetName = str;
  }

  get assetName(): string {
    return this._assetName;
  }

  get assetSrc(): string {
    return `/assets/images/panelData/${this._assetName}`;
  }

  @Input() text: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  handleDisplay(): void {
    // TODO
  }

  handleParam(): void {
    // TODO
  }
}
