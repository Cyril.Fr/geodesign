import {Component, Input, OnInit} from '@angular/core';
import {MapService} from '../../../../../core/services/map/map.service';
import {LinesLayerService} from '../../../../../core/services/map/lines-layer.service';

@Component({
  selector: 'geodesign-line-card',
  templateUrl: './line-card.component.html',
})
export class LineCardComponent implements OnInit {

  show: boolean = false;
  private _assetName: string;
  @Input('assetName')
  set assetName(str: string) {
    this._assetName = str;
  }

  get assetName(): string {
    return this._assetName;
  }

  get assetSrc(): string {
    return `/assets/images/panelData/${this._assetName}`;
  }

  @Input() text: string;

  constructor(private mapService: MapService, private linesLayerService: LinesLayerService) {
  }

  ngOnInit(): void {
  }

  handleShow(): void {
    this.show = !this.show;
    if (this.show) {
      this.linesLayerService.showLines();
    } else {
      this.linesLayerService.hideLines();
    }

  }

  handleLoad(): void {
    // TODO
  }
}
