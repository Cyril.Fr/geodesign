import {Component, Input, OnInit} from '@angular/core';
import {IsochronesLayerService} from '../../../../../core/services/map/isochrones/isochrones-layer.service';
import {LoggerService} from '../../../../../core/services/logger.service';
import {GeoJsonStop, GeoJsonStopFeature} from '../../../../../core/models/map/stops.model';
import {DataService} from '../../../../../core/services/data.service';
import {DataPanelService} from '../../../../../core/services/data-panel.service';
import {IsochronesDataService} from '../../../../../core/services/map/isochrones/isochrones-data.service';

@Component({
  selector: 'geodesign-isochrones-card',
  templateUrl: './isochrones-card.component.html',
})
export class IsochronesCardComponent implements OnInit {

  private _assetName: string;
  @Input('assetName')
  set assetName(str: string) {
    this._assetName = str;
  }

  get assetName(): string {
    return this._assetName;
  }

  get assetSrc(): string {
    return `/assets/images/panelData/${this._assetName}`;
  }

  @Input() text: string;
  @Input() stops: GeoJsonStop;
  stopOptionList: { label: string, value: GeoJsonStopFeature }[];

  selectedStop: GeoJsonStopFeature;
  lastGeneratedStop: GeoJsonStopFeature;
  show: boolean = false;
  enbableShow: boolean = false;

  constructor(public isochronesLayerService: IsochronesLayerService,
              private isochronesDataService: IsochronesDataService,
              private dataService: DataService,
              private logger: LoggerService,
              private dataPanelService: DataPanelService) {
  }

  ngOnInit(): void {
    // TODO replace this by the stops as input from stopsDataService when its implemented
    this.dataService.stops$.subscribe((stops: GeoJsonStop) => {
      this.extractOptionList(stops);
    });
  }

  handleShow(): void {
    if (this.show) {
      this.dataPanelService.hideLayer('isochrones');
    } else {
      this.dataPanelService.showLayer('isochrones');
    }
    this.show = !this.show;
  }

  async handleGenerate(): Promise<void> {
    // Condition to generate
    this.lastGeneratedStop = this.selectedStop;
    try {
      this.enbableShow = await this.isochronesDataService.generateIsochrones(this.selectedStop);
    } catch (e) {
      this.logger.error(e);
    }
  }

  async onSelectedStopChange(newSelectedStop: GeoJsonStopFeature): Promise<void> {
    if (newSelectedStop) {
      this.selectedStop = newSelectedStop;
      this.enbableShow = false;
      this.dataPanelService.hideLayer('isochrones');
      this.isochronesLayerService.removeIsochronesLayer();
      if (this.show) {
        await this.handleGenerate();
        this.dataPanelService.showLayer('isochrones');
      }
    }
  }

  private extractOptionList(stops: GeoJsonStop): any {
    if (stops && stops.features) {
      const stopOptionList: any = stops.features.map((feature: GeoJsonStopFeature) => {
        return {
          label: feature.properties.name,
          value: feature,
        };
      });
      if (stopOptionList) {
        this.stopOptionList = stopOptionList;
      }
    }
  }
}
