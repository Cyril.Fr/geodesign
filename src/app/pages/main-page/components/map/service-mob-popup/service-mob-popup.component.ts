import {Component, OnInit} from '@angular/core';
import {MobserviceLayerService} from '../../../../../core/services/map/mobility-service/mobservice-layer.service';
import {Option} from '../../../../../core/models/data-panel.model';
import {LoggerService} from '../../../../../core/services/logger.service';
import {LeafletMouseEvent} from 'leaflet';
import {MobilityServiceCardComponent} from '../../panel-data/mobility-service-card/mobility-service-card.component';
import {GeoJsonServiceMobFeature} from '../../../../../core/models/map/service-mob.model';

@Component({
  selector: 'geodesign-service-mob-popup',
  templateUrl: './service-mob-popup.component.html',
  styleUrls: ['./service-mob-popup.component.scss'],
})
export class ServiceMobPopupComponent implements OnInit {
  showPopup: boolean = false;
  xPosition: number;
  yPosition: number;
  typeValue: any;
  sizeValue: any;
  feature: GeoJsonServiceMobFeature;
  typeOptions: Option[] = [
    ...MobilityServiceCardComponent.options.filter(
      (option: Option) => option.value && typeof option.value === 'string') as Option[],
    {label: 'Non défini', value: 'non_defini'},
  ];
  sizeOptions: Option[] = [
    {label: '1', value: '1'},
    {label: '2', value: '2'},
  ];
  private leafletMouseEvent: LeafletMouseEvent;

  constructor(public serviceMobsService: MobserviceLayerService,
              private logger: LoggerService) {
  }

  ngOnInit(): void {
    this.serviceMobsService.displayPopup$.subscribe(([showPopup, mouseEvent]: [boolean, LeafletMouseEvent]) => {
      this.showPopup = showPopup;
      this.leafletMouseEvent = mouseEvent;
      if (this.leafletMouseEvent && this.leafletMouseEvent.containerPoint) {
        this.xPosition = this.leafletMouseEvent.containerPoint.x;
        this.yPosition = this.leafletMouseEvent.containerPoint.y;
      }
      if (this.leafletMouseEvent && this.leafletMouseEvent.target && this.leafletMouseEvent.target.feature) {
        this.updateFeature(mouseEvent.target.feature);
      }
    });
  }

  async onTypeChange(type: any): Promise<void> {
    if (this.typeValue !== type) {
      try {
        await this.updateServiceMob('type', type);
      } catch (e) {
        this.logger.error(e);
      }
    }
  }

  async onSizeChange(size: any): Promise<void> {
    if (this.sizeValue !== size) {
      try {
        await this.updateServiceMob('level', parseInt(size, 10));
      } catch (e) {
        this.logger.error(e);
      }
    }
  }

  private async updateServiceMob(clef: string, type: any): Promise<void> {
    const id: number = this.feature.properties.id;
    const body: any = {
      cle: clef,
      valeur: type,
      id,
    };
    try {
      await this.serviceMobsService.changeServiceMobInfos(body).toPromise();
      const updatedServiceMobFeature: GeoJsonServiceMobFeature = this.serviceMobsService.updateFeature(clef, type);
      this.updateFeature(updatedServiceMobFeature);
      this.serviceMobsService.showServiceMobs();
    } catch (e) {
      this.logger.error(e);
    }
  }

  updateFeature(updatedFeature: GeoJsonServiceMobFeature): void {
    this.feature = updatedFeature;
    this.typeValue = updatedFeature && updatedFeature.properties &&
    updatedFeature.properties.type ? updatedFeature.properties.type : null;
    this.sizeValue = updatedFeature && updatedFeature.properties &&
    updatedFeature.properties.level ? updatedFeature.properties.level.toString() : null;
  }
}
