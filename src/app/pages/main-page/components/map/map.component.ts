import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import 'leaflet-bing-layer';
import 'leaflet.pm';
import 'leaflet-geometryutil';
import 'leaflet-almostover';
import 'leaflet-curve';
import 'leaflet-polylinedecorator';
import 'leaflet-semicircle';
import 'leaflet.vectorgrid';
import {MapService} from '../../../../core/services/map/map.service';

@Component({
  selector: 'geodesign-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {

  @ViewChild('map', {static: false})
  set element(elementRef: ElementRef) {
    if (elementRef) {
      this.mapService.setMapElementRef(elementRef);
    }
  }
  fullScreen: boolean = false;
  darkMode: boolean = false;

  constructor(private mapService: MapService) {
  }

  ngOnInit(): void {
    this.mapService.fullScreen$.subscribe((fullScreen: boolean) => this.fullScreen = fullScreen);
    this.mapService.darkMode$.subscribe((darkMode: boolean) => this.darkMode = darkMode);
  }
}
