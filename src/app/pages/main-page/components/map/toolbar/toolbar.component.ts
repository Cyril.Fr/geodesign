import { DataSidenavService } from 'src/app/core/services/data-sidenav.service';
import { Component, OnInit, ViewChild, ElementRef, Renderer2, EventEmitter } from '@angular/core';
import { MapService } from 'src/app/core/services/map/map.service';
import { ToolbarService } from 'src/app/core/services/toolbar.service';
import { DndDropEvent } from 'ngx-drag-drop';
import { LatLng } from 'leaflet';

@Component({
  selector: 'geodesign-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  private zoomElement: ElementRef;
  private layerElement: ElementRef;

  fullScreen: boolean;
  darkMode: boolean;
  dataPanelIsOpen: boolean;

  // Insert the zoom control in custom toolbar instead default leaflet position
  @ViewChild('zoomControl', { static: false })
  set contentZoom(value: ElementRef) {
    this.zoomElement = value;
    if (value) {
      this.renderer.appendChild(
        this.zoomElement.nativeElement,
        this.mapService.getZoomControl()
      );
    }
  }

  @ViewChild('layerControl', { static: false })
  set contentLayer(value: ElementRef) {
    this.layerElement = value;
    if (value) {
      this.renderer.appendChild(
        this.layerElement.nativeElement,
        this.mapService.getLayerControl()
      );
    }
  }

  draggable: any = {
    data: 'myDragData',
    effectAllowed: 'move',
    disable: false,
  };

  constructor(
    private mapService: MapService,
    private renderer: Renderer2,
    private dataSidenavService: DataSidenavService,
    public toolbarService: ToolbarService
  ) {
  }

  ngOnInit(): void {
    this.mapService.fullScreen$.subscribe(
      (fullScreen: boolean) => (this.fullScreen = fullScreen)
    );
    this.mapService.darkMode$.subscribe(
      (darkMode: boolean) => (this.darkMode = darkMode)
    );
    this.dataSidenavService.sidenav$.subscribe(
      (dataPanelIsOpen: boolean) => (this.dataPanelIsOpen = dataPanelIsOpen)
    );
  }

  onDragEnd(event: DragEvent): void {
    const offset: number = this.fullScreen ? 0 : 70;
    const coordinates: LatLng = this.mapService.getLatLngFromScreenXY(event.clientX, event.clientY - offset);
    const url: string = 'https://www.google.com/maps?layer=c&cbll=' + coordinates.lat + ',' + coordinates.lng;
    const win: Window = window.open(url, '_blank');
    win.focus();
  }
}
