import {Component, OnInit} from '@angular/core';
import {MapService} from '../../../../../core/services/map/map.service';
import {Option} from '../../../../../core/models/data-panel.model';
import {SERVICE_ICON_TO_NUMBER} from '../../../../../core/constants/panel-data.const';
import {EmitsLayerService} from '../../../../../core/services/map/emits/emits-layer.service';
import {LoggerService} from '../../../../../core/services/logger.service';
import {GeoJsonEmitAndGenFeature} from '../../../../../core/models/map/emit-gen.model';
import {DataPanelService} from '../../../../../core/services/data-panel.service';
import {EmitsDataService} from '../../../../../core/services/map/emits/emits-data.service';
import {EmitsPopupService} from '../../../../../core/services/map/emits/emits-popup.service';

@Component({
  selector: 'geodesign-popin',
  templateUrl: './emit-popup.component.html',
  styleUrls: ['./emit-popup.component.scss'],
})
export class EmitPopupComponent implements OnInit {

  constructor(private mapService: MapService,
              private emitsLayerService: EmitsLayerService,
              private emitsDataService: EmitsDataService,
              private emitsPopupService: EmitsPopupService,
              private dataPanelService: DataPanelService,
              private logger: LoggerService) {
  }

  showPopup: boolean;
  xPosition: number;
  yPosition: number;
  typeOptions: Option[] = Object.keys(SERVICE_ICON_TO_NUMBER).map((key: string) => {
    return {label: key, value: key};
  });
  sizeOptions: Option[] = [
    {label: '1', value: '1'},
    {label: '2', value: '2'},
    {label: '3', value: '3'},
  ];
  public sizeValue: string;
  public typeValue: string;
  public inProjectValue: boolean;

  private extractProjectValue(event: boolean): string {
    return event ? 't' : 'f';
  }

  ngOnInit(): void {
    this.emitsPopupService.displayPopup$.subscribe(([show, x, y, feature]: any[]) => {
      this.showPopup = show;
      this.xPosition = x;
      this.yPosition = y;
      this.emitsDataService.updateFeature(feature);
      this.updateFormValues(feature);
    });
  }

  private resetEmits(): void {
    this.dataPanelService.hideLayer('emits');
    this.dataPanelService.showLayer('emits', true);
  }

  async onChange(key: string, value: string): Promise<void> {
    if (this.isNewValue(key, value)) {
      try {
        await this.emitsDataService.updateMarkerInfos(key, value);
        const updatedFeature: GeoJsonEmitAndGenFeature = this.emitsDataService.updateEmit(key, value);
        this.updateFormValues(updatedFeature);
        this.resetEmits();
      } catch (e) {
        this.logger.error(e);
      }
    }
  }

  private updateFormValues(updatedFeature: GeoJsonEmitAndGenFeature): void {
    this.typeValue = updatedFeature && updatedFeature.properties &&
    updatedFeature.properties.type ? updatedFeature.properties.type : 'null';
    this.sizeValue = updatedFeature && updatedFeature.properties &&
    updatedFeature.properties.taille ? updatedFeature.properties.taille.toString() : '0';
    this.inProjectValue = updatedFeature && updatedFeature.properties &&
      updatedFeature.properties.projet && updatedFeature.properties.projet === 't';
  }

  private isNewValue(key: string, value: string): boolean {
    const prevValue: string | boolean = this.extractValueFromKey(key);
    return value !== prevValue;
  }

  private extractValueFromKey(key: string): string | boolean {
    switch (key) {
      case 'projet':
        return this.extractProjectValue(this.inProjectValue);
      case 'taille':
        return this.sizeValue;
      case 'type':
        return this.typeValue;
      default:
        return null;
    }
  }
}
