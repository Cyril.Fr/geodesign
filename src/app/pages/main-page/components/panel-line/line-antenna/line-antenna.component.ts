import {Component, Input, OnInit} from '@angular/core';
import {Line} from '../../../../../core/models/line.model';

// TODO Adding antenna!
@Component({
  selector: 'geodesign-line-antenna',
  templateUrl: './line-antenna.component.html',
  styleUrls: ['./line-antenna.component.scss'],
})
export class LineAntennaComponent implements OnInit {

  private _line: Line;

  @Input()
  set line(line: Line) {
    this._line = line;
  }

  get line(): Line {
    return this._line;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
