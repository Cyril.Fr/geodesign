import {Component, Input, OnInit} from '@angular/core';
import {Line} from '../../../../../../core/models/line.model';
import {LineUtilService} from '../../../../../../core/services/line-util.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NetworkService} from '../../../../../../core/services/network.service';
import {LineService} from '../../../../../../core/services/line.service';
import {ParametersService} from '../../../../../../core/services/parameters.service';
import {LoggerService} from '../../../../../../core/services/logger.service';
import {CourseService} from '../../../../../../core/services/course.service';

@Component({
  selector: 'geodesign-line-number-round-trip',
  templateUrl: './line-number-round-trip.component.html',
  styleUrls: ['./line-number-round-trip.component.scss'],
})
export class LineNumberRoundTripComponent implements OnInit {

  private _line: Line;
  private _selectedPeriod: string;
  formGroup: FormGroup = new FormGroup({
    courseNumber: new FormControl('', {validators: [Validators.required], updateOn: 'blur'}),
  });

  @Input() indexSelectedPeriod: number = 0;
  nbCoursePeriod: number = 0;

  @Input()
  set line(line: Line) {
    this._line = line;
    this.getNbCoursePeriod();
  }

  get line(): Line {
    return this._line;
  }

  @Input()
  set selectedPeriod(period: string) {
    this._selectedPeriod = period;
    this.getNbCoursePeriod();

    if (this._line && this._line.course) {
      this.formGroup.get('courseNumber').patchValue(this._line.course[this.indexSelectedPeriod]);
    }
  }

  get selectedPeriod(): string {
    return this._selectedPeriod;
  }

  constructor(private parametersService: ParametersService,
              private logger: LoggerService,
              private courseService: CourseService) {
  }

  ngOnInit(): void {
    this.formGroup.valueChanges.subscribe(async () => {
      await this.onBlur();
    });
  }

  getNbCoursePeriod(): void {
    if (this._line && this._selectedPeriod) {
      this.nbCoursePeriod = LineUtilService.getNbCourseFromPeriod(this._line, this._selectedPeriod);
    }
  }

  async onBlur(): Promise<void> {
    this.formGroup.markAllAsTouched();
    // TODO Add bloque if scenario is blocked!!
    if (!this.formGroup.invalid) {
      try {
        const newCourse: number[] = this._line.course;
        newCourse[this.indexSelectedPeriod] = this.formGroup.value.courseNumber;
        await this.courseService.updateCourse({
          schema: NetworkService.getNetworkName(),
          dataId: this._line.id,
          course: JSON.stringify(newCourse),
        }).toPromise();
        LineService.updateLine(this._line, 'course', newCourse);
        this.parametersService.reloadInformationLine();
      } catch (e) {
        this.logger.error(e);
      }
    }
  }

}
