import {Component, Input, OnInit} from '@angular/core';
import {Line} from '../../../../../core/models/line.model';
import {LineUtilService} from '../../../../../core/services/line-util.service';
import {ParametersService} from '../../../../../core/services/parameters.service';
import {NbJours} from '../../../../../core/models/nb-jours.model';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatTabChangeEvent} from '@angular/material';
import {LineService} from '../../../../../core/services/line.service';
import {LoggerService} from '../../../../../core/services/logger.service';
import {NetworkService} from '../../../../../core/services/network.service';
import {EncodeHourPipe} from '../../../../../shared/pipes/encode-hour.pipe';
import {time} from '../../../../../shared/validators/time.validator';
import {DecodeHourPipe} from '../../../../../shared/pipes/decode-hour.pipe';

@Component({
  selector: 'geodesign-line-periods',
  templateUrl: './line-periods.component.html',
  styleUrls: ['./line-periods.component.scss'],
})
export class LinePeriodsComponent implements OnInit {

  private _line: Line;
  // TODO Replace with behavior subject
  periods: NbJours[] = ParametersService.NB_JOUR_PERIODE;
  selectedPeriod: string = 'PSCO';
  indexSelectedPeriod: number = 0;

  nbCoursePeriod: number = 0;
  formGroup: FormGroup;
  isLoading: boolean = false;
  isTadLine: boolean = false;

  @Input()
  set line(line: Line) {
    this._line = line;
    this.isTadLine = LineUtilService.isTadLine(this._line);
    this.initializeFormGroup();
    this.nbCoursePeriod = LineUtilService.getNbCourseFromPeriod(this._line, this.selectedPeriod);
  }

  get line(): Line {
    return this._line;
  }

  constructor(private parametersService: ParametersService,
              private lineService: LineService,
              private logger: LoggerService,
              private encodeHour: EncodeHourPipe,
              private decodeHour: DecodeHourPipe) {
  }

  ngOnInit(): void {
    this.parametersService.nbJours$.subscribe((nbJours: NbJours[]) => {
      if (nbJours && nbJours.length > 0) {
        this.periods = nbJours;
        this.initializeFormGroup();
      }
    });
  }

  initializeFormGroup(): void {
    this.isLoading = true;
    this.formGroup = new FormGroup({});
    this.periods.forEach((period: NbJours) => {
      this.formGroup.addControl(period.periode, new FormArray([]));
      const operatingInfo: any = this._line.operating_info[period.periode];
      if (operatingInfo) {
        operatingInfo.forEach((op: any) => {
          const formGroup: FormGroup = new FormGroup({
            debut: new FormControl({
              value: this.encodeHour.transform(op.debut),
              disabled: this._line.modecourse,
            }, [Validators.required]),
            fin: new FormControl({
              value: this.encodeHour.transform(op.fin),
              disabled: this._line.modecourse,
            }, [Validators.required]),
            frequence: new FormControl({
              value: op.frequence,
              disabled: this._line.modecourse,
            }, {validators: [Validators.required], updateOn: 'blur'}),
          }, [time()]);

          if (this.isTadLine) {
            formGroup.addControl('veh', new FormControl({
              value: op.veh,
              disabled: this._line.modecourse,
            }, [Validators.required]));
          }

          (this.formGroup.get(period.periode) as FormArray).push(formGroup);
        });
      }
    });

    this.listenOnFormGroupChanges();

    this.isLoading = false;
  }

  listenOnFormGroupChanges(): void {
    this.formGroup.valueChanges.subscribe(async () => {
      this.formGroup.markAllAsTouched();
      if (!this.formGroup.invalid) {
        Object.keys(this.formGroup.value).forEach((period: string) => {
          this.formGroup.value[period] = this.formGroup.value[period].map((op: any) => {
            return {
              debut: this.decodeHour.transform(op.debut),
              fin: this.decodeHour.transform(op.fin),
              frequence: op.frequence,
            };
          });
        });

        this._line.operating_info = this.formGroup.value;

        await this.changeOperational();
      }
    });
  }

  changeSelectedPeriod(changeEvent: MatTabChangeEvent): void {
    this.indexSelectedPeriod = changeEvent.index;
    this.selectedPeriod = changeEvent.tab.textLabel;
    this.nbCoursePeriod = LineUtilService.getNbCourseFromPeriod(this._line, this.selectedPeriod);
  }

  async addPeriod(): Promise<void> {
    this.formGroup.markAllAsTouched();
    if (!this.formGroup.invalid) {

      // TODO Adding block system
      const periodToAdd: any = {
        debut: 5,
        fin: 7,
        frequence: 40,
      };

      const opPeriod: any[] = this._line.operating_info[this.selectedPeriod];

      // TODO Adding TAD
      if (opPeriod && opPeriod.length > 0) {
        periodToAdd.debut = opPeriod[opPeriod.length - 1].fin;
        periodToAdd.fin = opPeriod[opPeriod.length - 1].fin + 2;
        periodToAdd.frequence = opPeriod[opPeriod.length - 1].frequence;
      }

      this._line.operating_info[this.selectedPeriod].push(periodToAdd);
      await this.changeOperational();
    }
  }

  async removePeriod(index: number): Promise<void> {
    this._line.operating_info[this.selectedPeriod].splice(index, 1);
    await this.changeOperational();
  }

  async changeOperational(): Promise<void> {
    try {
      await this.lineService.changeOperational({
        schema: NetworkService.getNetworkName(),
        json: JSON.stringify(this._line.operating_info),
        dataId: this._line.id,
      }).toPromise();
      this.initializeFormGroup();
      this.parametersService.reloadInformationLine();
      this.nbCoursePeriod = LineUtilService.getNbCourseFromPeriod(this._line, this.selectedPeriod);
    } catch (e) {
      this.logger.error(e);
    }
  }
}
