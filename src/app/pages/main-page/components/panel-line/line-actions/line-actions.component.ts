import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'geodesign-line-actions',
  templateUrl: './line-actions.component.html',
  styleUrls: ['./line-actions.component.scss'],
})
export class LineActionsComponent implements OnInit {

  @Output() modifyLine: EventEmitter<void> = new EventEmitter<void>();
  @Output() deleteLine: EventEmitter<void> = new EventEmitter<void>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onModifyLine(): void {
    this.modifyLine.emit();
  }

  onDeleteLine(): void {
    this.deleteLine.emit();
  }

}
