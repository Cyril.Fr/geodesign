import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Line} from '../../../../../core/models/line.model';
import {SpeedService} from '../../../../../core/services/speed.service';
import {NetworkService} from '../../../../../core/services/network.service';
import {LoggerService} from '../../../../../core/services/logger.service';
import {LineService} from '../../../../../core/services/line.service';
import {ParametersService} from '../../../../../core/services/parameters.service';

@Component({
  selector: 'geodesign-line-commercial-speed',
  templateUrl: './line-commercial-speed.component.html',
  styleUrls: ['./line-commercial-speed.component.scss'],
})
export class LineCommercialSpeedComponent implements OnInit {

  private _line: Line;

  @Input()
  set line(line: Line) {
    this._line = line;
    this.formGroup.get('commercialSpeed').patchValue(Math.round(this._line.speed));
  }

  get line(): Line {
    return this._line;
  }

  formGroup: FormGroup = new FormGroup({
    commercialSpeed: new FormControl('', {
      validators: [Validators.required, Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')],
      updateOn: 'blur',
    }),
  });

  constructor(private speedService: SpeedService,
              private parametersService: ParametersService,
              private logger: LoggerService) {
  }

  ngOnInit(): void {
    this.formGroup.valueChanges.subscribe(async () => {
      await this.onBlur();
    });
  }

  async onBlur(): Promise<void> {
    this.formGroup.markAllAsTouched();
    // TODO Add bloque if scenario is blocked!!
    if (!this.formGroup.invalid) {
      try {
        const speed: number = this.formGroup.value.commercialSpeed;
        await this.speedService.updateSpeed({
          schema: NetworkService.getNetworkName(),
          dataId: this._line.id,
          speed,
        }).toPromise();
        LineService.updateLine(this._line, 'speed', speed);
        this.parametersService.reloadInformationLine();
      } catch (e) {
        this.logger.error(e);
      }
    }
  }

}
