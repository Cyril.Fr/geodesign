import {Component, Input, OnInit} from '@angular/core';
import {Line} from '../../../../../core/models/line.model';
import {LineService} from '../../../../../core/services/line.service';
import {LoggerService} from '../../../../../core/services/logger.service';
import {NetworkService} from '../../../../../core/services/network.service';
import {ParametersService} from '../../../../../core/services/parameters.service';

@Component({
  selector: 'geodesign-line-round-trip',
  templateUrl: './line-round-trip.component.html',
  styleUrls: ['./line-round-trip.component.scss'],
})
export class LineRoundTripComponent implements OnInit {

  private _line: Line;
  public roundTrip: number = 0;
  public toggleRoundTrip: boolean = false;
  public disableRoundTrip: boolean = false;

  @Input()
  set line(line: Line) {
    this._line = line;
    this.toggleRoundTrip = this._line.aller_retour === 't';
  }

  get line(): Line {
    return this._line;
  }

  constructor(private lineService: LineService,
              private logger: LoggerService,
              private parametersService: ParametersService) {
  }

  ngOnInit(): void {
  }

  async saveAR(): Promise<void> {
    try {
      await this.lineService.setAR({
        dataId: this._line.id,
        schema: NetworkService.getNetworkName(),
        AR: this.toggleRoundTrip,
      }).toPromise();
      LineService.updateLine(this._line, 'aller_retour', this.toggleRoundTrip ? 't' : 'f');
      this.parametersService.reloadInformationLine();
      this.disableRoundTrip = !this.toggleRoundTrip;
    } catch (e) {
      this.logger.error(e);
    }
  }
}
