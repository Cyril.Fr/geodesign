import {Component, Input, OnInit} from '@angular/core';
import {Line} from '../../../../../core/models/line.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ParametersService} from '../../../../../core/services/parameters.service';
import {LoggerService} from '../../../../../core/services/logger.service';
import {LineService} from '../../../../../core/services/line.service';
import {TriggeringService} from '../../../../../core/services/triggering.service';

@Component({
  selector: 'geodesign-line-triggering',
  templateUrl: './line-triggering.component.html',
  styleUrls: ['./line-triggering.component.scss'],
})
export class LineTriggeringComponent implements OnInit {

  private _line: Line;

  @Input()
  set line(line: Line) {
    this._line = line;
    this.formGroup.get('triggering').patchValue(Math.round(this._line.declenchement));
  }

  get line(): Line {
    return this._line;
  }

  formGroup: FormGroup = new FormGroup({
    triggering: new FormControl('', {
      validators: [Validators.required, Validators.pattern('^[0-9]+$'), Validators.max(100), Validators.min(0)],
      updateOn: 'blur',
    }),
  });

  constructor(private triggeringService: TriggeringService,
              private parametersService: ParametersService,
              private logger: LoggerService) {
  }

  ngOnInit(): void {
    this.formGroup.valueChanges.subscribe(async () => {
      await this.onBlur();
    });
  }

  async onBlur(): Promise<void> {
    this.formGroup.markAllAsTouched();
    // TODO Add bloque if scenario is blocked!!
    if (!this.formGroup.invalid) {
      try {
        const triggering: number = this.formGroup.value.triggering;
        await this.triggeringService.changeTriggering(this._line, triggering).toPromise();
        LineService.updateLine(this._line, 'declenchement', triggering);
        this.parametersService.reloadInformationLine();
      } catch (e) {
        this.logger.error(e);
      }
    }
  }
}
