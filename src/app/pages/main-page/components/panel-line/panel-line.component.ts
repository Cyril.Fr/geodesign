import {Component, OnInit} from '@angular/core';
import {DataSidenavService} from '../../../../core/services/data-sidenav.service';
import {LineService} from '../../../../core/services/line.service';
import {Line} from '../../../../core/models/line.model';
import {LoggerService} from '../../../../core/services/logger.service';
import {NetworkService} from '../../../../core/services/network.service';
import {ParametersService} from '../../../../core/services/parameters.service';
import {LineUtilService} from '../../../../core/services/line-util.service';
import {DialogService} from '../../../../core/services/dialog.service';
import {StatsDialogComponent} from '../../../../core/header/dialogs/stats-dialog/stats-dialog.component';

@Component({
  selector: 'geodesign-panel-line',
  templateUrl: './panel-line.component.html',
  styleUrls: ['./panel-line.component.scss'],
})
export class PanelLineComponent implements OnInit {

  isOpened: boolean = false;
  lines: Line[] = [];
  selectedLine: Line;
  showCreateForm: boolean = false;
  isModify: boolean = false;
  isLinesOpened: boolean = false;

  isTadLine: any = LineUtilService.isTadLine;

  constructor(public sidenavService: DataSidenavService,
              private lineService: LineService,
              private logger: LoggerService,
              private parametersService: ParametersService,
              private dialogService: DialogService) {
  }

  ngOnInit(): void {
    // Loading not handled, tmp behavior subject
    this.lineService.lines$.subscribe((lines: Line[]) => this.lines = lines);
    this.lineService.openLine$.subscribe((line: Line) => {
      if (line) {
        this.selectedLine = line;
      }
    });
  }

  toggleSidenav(): void {
    this.sidenavService.toggleSidenavLine();
    this.isOpened = this.sidenavService.isOpenSidenavLine();
  }

  openLine(line: Line): void {
    this.selectedLine = line;
  }

  onModifyLine(): void {
    this.showCreateForm = true;
    this.isModify = true;
  }

  openCreation(): void {
    this.isModify = false;
    this.showCreateForm = !this.showCreateForm;
  }

  async onDeleteLine(): Promise<void> {
    // TODO Remove line from line
    try {
      await this.lineService.deleteLine(this.selectedLine.id, NetworkService.getNetworkName()).toPromise();
      this.parametersService.reloadInformationLine();
      this.selectedLine = null;
    } catch (e) {
      this.logger.error(e);
    }
  }

  openStatsDialog(): void {
    this.dialogService.show(StatsDialogComponent, {
      data: {
        tab: 'lines',
      },
    });
  }

  toggleLine(): void {
    this.isLinesOpened = !this.isLinesOpened;
  }

  trackByFn(index: number): number {
    return index;
  }

}
