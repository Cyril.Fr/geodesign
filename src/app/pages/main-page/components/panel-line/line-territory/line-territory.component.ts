import {Component, Input, OnInit} from '@angular/core';
import {Line} from '../../../../../core/models/line.model';
import {LineService} from '../../../../../core/services/line.service';
import {LoggerService} from '../../../../../core/services/logger.service';
import {InfoPopulation} from '../../../../../core/models/info-population.model';
import {NetworkService} from '../../../../../core/services/network.service';
import {ParametersService} from '../../../../../core/services/parameters.service';
import {ParamsCommun} from '../../../../../core/models/params-commun.model';

@Component({
  selector: 'geodesign-line-territory',
  templateUrl: './line-territory.component.html',
  styleUrls: ['./line-territory.component.scss'],
})
export class LineTerritoryComponent implements OnInit {

  private _line: Line;
  isLoading: boolean = false;

  popLine: number = 0;
  popScoLine: number = 0;
  pop65Line: number = 0;
  popEmpLine: number = 0;

  showPopLine: boolean = false;
  showPopScoLine: boolean = false;
  showPop65Line: boolean = false;
  showPopEmpLine: boolean = false;

  @Input()
  set line(line: Line) {
    this._line = line;
    if (line) {
      this.displayInfo();
    }
  }

  get line(): Line {
    return this._line;
  }

  constructor(private lineService: LineService,
              private parametersService: ParametersService,
              private logger: LoggerService) {
  }

  ngOnInit(): void {
    if (ParametersService.PARAMS_SD) {
      this.showInformationFromParams(ParametersService.PARAMS_SD);
    }
    this.parametersService.paramsSd$.subscribe((paramsSd: ParamsCommun) => {
      if (paramsSd) {
        this.showPopLine = false;
        this.showPopScoLine = false;
        this.showPop65Line = false;
        this.showPopEmpLine = false;
        this.showInformationFromParams(paramsSd);
      }
    });
  }

  async displayInfo(): Promise<void> {
    this.isLoading = true;
    try {
      // TODO Replace with the real buffer when parameters will be ready
      const infoPops: InfoPopulation[] = await this.lineService.getInfoPop({
        buffer: [1000, 700, 400, 300, 300, 500],
        schema: NetworkService.getNetworkName(),
      }).toPromise();

      const infoPop: InfoPopulation = infoPops.find((info: InfoPopulation) => info.id === this._line.id);
      if (infoPop) {
        this.popLine = infoPop.pop;
        this.popScoLine = infoPop.pop_sco;
        this.pop65Line = infoPop.pop65;
        this.popEmpLine = infoPop.emp;
      }
    } catch (e) {
      this.logger.error(e);
    }
    this.isLoading = false;
  }

  showInformationFromParams(paramsUo: ParamsCommun): void {
    paramsUo.value.split(', ').forEach((value: string) => {
      switch (value) {
        case 'pop':
          this.showPopLine = true;
          break;
        case 'popSco':
          this.showPopScoLine = true;
          break;
        case 'pop65':
          this.showPop65Line = true;
          break;
        case 'emp':
          this.showPopEmpLine = true;
          break;
      }
    });
  }

}
