import {Component, Input, OnInit} from '@angular/core';
import {Line} from '../../../../../core/models/line.model';
import {LineService} from '../../../../../core/services/line.service';
import {LoggerService} from '../../../../../core/services/logger.service';
import {ParametersService} from '../../../../../core/services/parameters.service';
import {ParamsCommun} from '../../../../../core/models/params-commun.model';
import {LineInformation} from '../../../../../core/models/line-information.model';

@Component({
  selector: 'geodesign-line-statistics',
  templateUrl: './line-statistics.component.html',
  styleUrls: ['./line-statistics.component.scss'],
})
export class LineStatisticsComponent implements OnInit {

  private _line: Line;
  isLoading: boolean = false;

  lineInformation: LineInformation;
  showLengthLine: boolean = false;
  showRotationTimeLine: boolean = false;
  showNbBusLine: boolean = false;
  showCommercialKmLine: boolean = false;

  @Input()
  set line(line: Line) {
    this._line = line;
    if (line) {
      this.displayInfo();
    }
  }

  get line(): Line {
    return this._line;
  }

  constructor(private lineService: LineService,
              private logger: LoggerService,
              private parametersService: ParametersService) {
  }

  ngOnInit(): void {
    if (ParametersService.PARAMS_UO) {
      this.showInformationFromParams(ParametersService.PARAMS_UO);
    }
    this.parametersService.paramsUo$.subscribe((paramsUo: ParamsCommun) => {
      if (paramsUo) {
        this.showLengthLine = false;
        this.showRotationTimeLine = false;
        this.showNbBusLine = false;
        this.showCommercialKmLine = false;
        this.showInformationFromParams(paramsUo);
      }
    });
    this.parametersService.reloadLineInformation$.subscribe(async () => {
      try {
        await this.displayInfo();
      } catch (e) {
        this.logger.error(e);
      }
    });
  }

  displayInfo(): void {
    this.isLoading = true;
    try {
      this.lineInformation = this.lineService.getInformationLine(this._line);
      this.isLoading = false;
    } catch (e) {
      this.isLoading = false;
      this.logger.error(e);
    }
  }

  showInformationFromParams(paramsUo: ParamsCommun): void {
    paramsUo.value.split(', ').forEach((value: string) => {
      switch (value) {
        case 'dist':
          this.showLengthLine = true;
          break;
        case 'time':
          this.showRotationTimeLine = true;
          break;
        case 'nbBusMax':
          this.showNbBusLine = true;
          break;
        case 'nbKm':
          this.showCommercialKmLine = true;
          break;
      }
    });
  }

}
