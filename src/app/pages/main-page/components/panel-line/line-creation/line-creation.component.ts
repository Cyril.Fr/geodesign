import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Line} from '../../../../../core/models/line.model';
import {LineService} from '../../../../../core/services/line.service';
import {DRAW_MODE, LINE_TYPE, OFFER_MODE, TAD_TYPE} from '../../../../../core/constants/line.const';
import {NetworkService} from '../../../../../core/services/network.service';
import {LoggerService} from '../../../../../core/services/logger.service';
import {nameLine} from '../../../../../shared/validators/name-line.validator';
import {abbrLine} from '../../../../../shared/validators/abbr-line.validator';
import {OperatingInfo} from '../../../../../core/models/operatingInfo.model';

@Component({
  selector: 'geodesign-line-creation',
  templateUrl: './line-creation.component.html',
  styleUrls: ['./line-creation.component.scss'],
})
export class LineCreationComponent implements OnInit {

  typeOptions: { label: string; value: string }[];
  offreModeOptions: { label: string; value: string }[];
  drawModeOptions: { label: string; value: string }[];
  tadTypeOptions: { label: string; value: string }[];
  lineThOptions: { label: string; value: number }[] = [];
  lines: Line[] = [];
  isLoading: boolean = false;
  isError: boolean = false;

  formGroup: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    abbr: new FormControl('', [Validators.required, Validators.maxLength(4)]),
    type: new FormControl(''),
    dataId: new FormControl(''),
    offreMode: new FormControl(false),
    mode: new FormControl('route'),
    toggleTod: new FormControl(false),
    color: new FormControl('#FF0000'),
  });

  @Output() closeCreation: EventEmitter<void> = new EventEmitter();
  @Input() isModify: boolean = false;

  private _line: Line;

  @Input()
  set line(line: Line) {
    this._line = line;
  }

  get line(): Line {
    return this._line;
  }

  constructor(private lineService: LineService,
              private logger: LoggerService) {
  }

  ngOnInit(): void {
    this.lineThOptions = LineService.currentLines.map((line: Line) => ({
      label: line.nom,
      value: line.id,
    }));

    this.setDefaultValue();
    this.addValidatorsFormGroup();

    this.tadTypeOptions = TAD_TYPE;
    this.typeOptions = LINE_TYPE;
    this.offreModeOptions = OFFER_MODE;
    this.drawModeOptions = DRAW_MODE;

    this.formGroup.get('toggleTod').valueChanges.subscribe((res: boolean) => {
      if (res) {
        this.formGroup.removeControl('type');
        this.formGroup.removeControl('offreMode');
        this.formGroup.removeControl('mode');
        this.formGroup.addControl('tadType', new FormControl('', [Validators.required]));
      } else {
        this.formGroup.addControl('type', new FormControl(''));
        this.formGroup.addControl('offreMode', new FormControl(false));
        this.formGroup.addControl('mode', new FormControl('route'));
        this.formGroup.removeControl('tadType');
      }
      this.setDefaultValue();
    });
  }

  addValidatorsFormGroup(): void {
    if (this.isModify && this._line) {
      this.formGroup.get('name').setValidators([Validators.required]);
      this.formGroup.get('abbr').setValidators([Validators.required, Validators.maxLength(4)]);
    } else {
      this.formGroup.get('name').setValidators([Validators.required, nameLine(this._line)]);
      this.formGroup.get('abbr').setValidators([Validators.required, Validators.maxLength(4), abbrLine(this._line)]);
    }
  }

  setDefaultValue(): void {
    if (this.isModify && this._line) {
      this.formGroup.get('name').patchValue(this._line.nom);
      this.formGroup.get('abbr').patchValue(this._line.abbr);
      this.formGroup.get('color').patchValue(this._line.couleur);
      this.formGroup.get('type').patchValue(this._line.linetype);
      this.formGroup.get('offreMode').patchValue(this._line.modecourse);
    }
  }

  async onSubmit(): Promise<void> {
    this.formGroup.markAllAsTouched();
    this.isError = false;
    if (!this.formGroup.invalid && !this.isLoading) {
      this.isLoading = true;

      try {
        if (!this.formGroup.value.toggleTod) {
          const lastJSON: any = await this.getLastJson('regular', this.isModify);
          if (this.isModify) {
            await this.modifyRegularLine(lastJSON);
          } else {
            await this.createRegularLine(lastJSON);
          }
        } else {
          const lastJSON: any = await this.getLastJson('tad', this.isModify);
          if (this.isModify) {
            await this.modifyTadLine(lastJSON);
          } else {
            await this.createTadLine(lastJSON);
          }
        }
        await this.lineService.getLines({
          schema: NetworkService.getNetworkName(),
        }).toPromise();
        this.lineService.openLine(LineService.currentLines.reduce((previousLine: Line, currentLine: Line) => {
          return currentLine.id > previousLine.id ? currentLine : previousLine;
        }));
        this.onCloseCreation();
      } catch (e) {
        this.isError = true;
        this.logger.error(e);
      }
    }

    this.isLoading = false;
  }

  async modifyRegularLine(lastJSON: any): Promise<void> {
    await this.lineService.modifyRegularLine({
        ...this.formGroup.value,
        dataId: this._line.id,
        lastJSON: JSON.stringify(lastJSON),
      },
      NetworkService.getNetworkName()
    ).toPromise();
  }

  async modifyTadLine(lastJSON: any): Promise<void> {
    await this.lineService.modifyRegularLine({
        ...this.formGroup.value,
        dataId: this._line.id,
        lastJSON: JSON.stringify(lastJSON),
      },
      NetworkService.getNetworkName()
    ).toPromise();
  }

  async createRegularLine(lastJSON: any): Promise<void> {
    await this.lineService.createRegularLine({...this.formGroup.value, lastJSON: JSON.stringify(lastJSON)},
      NetworkService.getNetworkName()
    ).toPromise();
  }

  async createTadLine(lastJSON: any): Promise<void> {
    await this.lineService.createTadLine(
      {
        ...this.formGroup.value,
        dataId: LineService.getLastLineFromType(null).id + 1,
        json: JSON.stringify(lastJSON),
      },
      NetworkService.getNetworkName()
    ).toPromise();
  }

  // TODO Refacto
  async getLastJson(lineType: string, modification: boolean = false): Promise<any> {
    let lastJSON: any;
    if (this.formGroup.value.dataId) {
      lastJSON = LineService.currentLines.find(
        (line: Line) => line.id === this.formGroup.value.dataId
      ).operating_info;
    } else if (modification) {
      lastJSON = this._line.operating_info;
    } else if (LineService.currentLines.length > 0) {
      lastJSON = LineService.currentLines.reduce((previousLine: Line, currentLine: Line) => {
        return currentLine.id > previousLine.id ? currentLine : previousLine;
      }).operating_info;
      if (lineType === 'tad') {
        lastJSON = this.addVehiculeToOperatingInfo(lastJSON);
      } else {
        lastJSON = this.removeVehiculeToOperatingInfo(lastJSON);
      }
    } else {
      try {
        lastJSON = await this.lineService
          .getDefaultOperionalJSON(NetworkService.getNetworkName(), lineType)
          .toPromise();
      } catch (e) {
        this.isError = true;
        this.logger.error(e);
      }
    }

    return lastJSON;
  }

  addVehiculeToOperatingInfo(op: any): any {
    const newJSON: any = {};

    Object.keys(op).forEach((period: string) => {
      newJSON[period] = op[period].map((operatingInfo: OperatingInfo) => ({...operatingInfo, veh: 1}));
    });

    return newJSON;
  }

  removeVehiculeToOperatingInfo(op: any): any {
    const newJSON: any = {};

    Object.keys(op).forEach((period: string) => {
      newJSON[period] = op[period].map((operatingInfo: OperatingInfo) => {
        if (operatingInfo.veh) {
          delete operatingInfo.veh;
        }

        return operatingInfo;
      });
    });

    return newJSON;
  }

  onCloseCreation(): void {
    this.closeCreation.emit();
  }
}
