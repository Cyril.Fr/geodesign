import {Component, OnInit, ViewChild} from '@angular/core';
import {LoggerService} from '../../core/services/logger.service';
import {ParametersService} from '../../core/services/parameters.service';
import {NetworkService} from '../../core/services/network.service';
import {DialogService} from '../../core/services/dialog.service';
import {StartNetworkDialogComponent} from '../../core/header/dialogs/start-network-dialog/start-network-dialog.component';
import {ActivatedRoute} from '@angular/router';
import {Network} from '../../core/models/network.model';
import {DataSidenavService} from '../../core/services/data-sidenav.service';
import {MatSidenav} from '@angular/material/sidenav';
import {MapService} from '../../core/services/map/map.service';
import {ImportDataService} from '../../core/services/import-data.service';
import {LineService} from '../../core/services/line.service';
import * as L from 'leaflet';
import {LeafletLine} from '../../core/models/map/lines.model';

@Component({
  selector: 'geodesign-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit {

  private sidenav: MatSidenav;
  @ViewChild('sidenav', {static: false})
  set content(content: MatSidenav) {
    this.sidenav = content;
    if (content) {
      this.dataSidenavService.setSidenav(this.sidenav);
    }
  }
  @ViewChild('sidenavLine', {static: true}) sidenavLine: MatSidenav;

  isLoading: boolean = true;
  fullScreen: boolean = false;

  public networks: Network[];
  public lines: LeafletLine[];

  constructor(
    private route: ActivatedRoute,
    private networkService: NetworkService,
    private dialogService: DialogService,
    private parametersService: ParametersService,
    private logger: LoggerService,
    private dataSidenavService: DataSidenavService,
    private mapService: MapService,
    private importService: ImportDataService,
    private lineService: LineService
  ) {}

  // Trully init the app
  async ngOnInit(): Promise<void> {
    try {
      await this.importService.importData();
    } catch (e) {
      this.logger.error('Data error ', e);
    }
    try {
      // Fetch network lines ( !== Leaflet Lines )
      await this.lineService.getLines({schema: NetworkService.getNetworkName()}).toPromise();
      this.fixSpaceBetweenTiles();
      this.mapService.initMap();
      this.dataSidenavService.setSidenavLine(this.sidenavLine);

      const schema: string = NetworkService.getNetworkName();
      await this.parametersService.getParam({schema}).toPromise();
      await this.parametersService.getNbJour({schema}).toPromise();
      await this.parametersService.getParamCommun().toPromise();
      await this.networkService.getNetworkPop().toPromise();
    } catch (e) {
      this.logger.error(e);
    }

    this.route.paramMap.subscribe(() => {
      if (!NetworkService.getNetworkName()) {
        this.showStartNetworkDialog();
      }
    });

    this.mapService.fullScreen$.subscribe((fullScreen: boolean) => this.fullScreen = fullScreen);

    this.isLoading = false;
  }

  onSubmit(): void {
  }

  showStartNetworkDialog(): void {
    this.dialogService.show(StartNetworkDialogComponent, {disableClose: true}).subscribe();
  }

  // Hack to hide space between tiles on fractionnals zooms
  fixSpaceBetweenTiles(): void {
    const originalInitTile: any = (L.GridLayer.prototype as any)._initTile;
    L.GridLayer.include({
        _initTile: (tile: any): void => {
            originalInitTile.call(L.gridLayer(), tile);

            const tileSize: any = L.gridLayer().getTileSize();

            tile.style.width = tileSize.x + 1 + 'px';
            tile.style.height = tileSize.y + 1 + 'px';
        },
    });
  }
}
