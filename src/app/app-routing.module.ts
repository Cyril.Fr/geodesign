import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NetworkResolver} from './core/resolvers/network.resolver';

const routes: Routes = [{
  path: 'geodesign',
  loadChildren: (): Promise<any> => import('./pages/pages.module').then((m: any) => m.PagesModule),
}, {
  path: '',
  redirectTo: '/geodesign',
  pathMatch: 'full',
}, {
  path: '**',
  redirectTo: '/geodesign',
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [NetworkResolver],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
