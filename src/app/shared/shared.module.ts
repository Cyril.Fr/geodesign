import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LinkComponent} from './components/link/link.component';
import {SelectComponent} from './components/select/select.component';
import {InputTextComponent} from './components/input-text/input-text.component';
import {TooltipComponent} from './components/tooltip/tooltip.component';
import {DragDropListComponent} from './components/drag-drop-list/drag-drop-list.component';
import {InputCheckboxComponent} from './components/input-checkbox/input-checkbox.component';
import {StatisticsComponent} from './components/statistics/statistics.component';
import {RoundTextPictoComponent} from './components/round-text-picto/round-text-picto.component';
import {InputSliderComponent} from './components/input-slider/input-slider.component';
import {ButtonComponent} from './components/button/button.component';
import {TabsComponent} from './components/tabs/tabs.component';
import {InputRadioComponent} from './components/input-radio/input-radio.component';
import {InputFileComponent} from './components/input-file/input-file.component';
import {ErrorFormComponent} from './components/error-form/error-form.component';
import {InputColorComponent} from './components/input-color/input-color.component';
import {InputToggleComponent} from './components/input-toggle/input-toggle.component';
import {TableComponent} from './components/table/table.component';
import {FleetGraphicComponent} from './components/fleet-graphic/fleet-graphic.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ColorPickerModule} from 'ngx-color-picker';
import {AngularMaterialModule} from '../angular-material.module';
import {AlertComponent} from './components/alert/alert.component';
import {TranslateModule} from '@ngx-translate/core';
import {LoaderComponent} from './components/loader/loader.component';
import {CalcTimePipe} from './pipes/calc-time.pipe';
import {FormatNumberPipe} from './pipes/format-number.pipe';
import {InputToggleWithLabelComponent} from './components/input-toggle-with-label/input-toggle-with-label.component';
import {InputTimePickerComponent} from './components/input-time-picker/input-time-picker.component';
import {EncodeHourPipe} from './pipes/encode-hour.pipe';
import {DecodeHourPipe} from './pipes/decode-hour.pipe';
import { ConvertMinInHourPipe } from './pipes/convert-min-in-hour.pipe';
import { ConvertKmInMeterPipe } from './pipes/convert-km-in-meter.pipe';
import {AddPxPipe} from './pipes/add-px.pipe';

@NgModule({
  declarations: [
    LinkComponent,
    SelectComponent,
    InputTextComponent,
    TooltipComponent,
    DragDropListComponent,
    InputCheckboxComponent,
    StatisticsComponent,
    RoundTextPictoComponent,
    InputSliderComponent,
    ButtonComponent,
    TabsComponent,
    InputRadioComponent,
    InputFileComponent,
    ErrorFormComponent,
    InputColorComponent,
    InputToggleComponent,
    TableComponent,
    FleetGraphicComponent,
    AlertComponent,
    LoaderComponent,
    CalcTimePipe,
    FormatNumberPipe,
    InputToggleWithLabelComponent,
    InputTimePickerComponent,
    EncodeHourPipe,
    DecodeHourPipe,
    ConvertMinInHourPipe,
    ConvertKmInMeterPipe,
    AddPxPipe,
  ],
  exports: [
    InputTextComponent,
    LinkComponent,
    SelectComponent,
    ButtonComponent,
    InputCheckboxComponent,
    InputSliderComponent,
    AlertComponent,
    InputToggleComponent,
    InputColorComponent,
    LoaderComponent,
    CalcTimePipe,
    FormatNumberPipe,
    InputToggleWithLabelComponent,
    InputColorComponent,
    InputTimePickerComponent,
    ConvertMinInHourPipe,
    ConvertKmInMeterPipe,
    AddPxPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularMaterialModule,
    ColorPickerModule,
    TranslateModule,
  ],
  providers: [
    EncodeHourPipe,
    DecodeHourPipe,
  ],
})
export class SharedModule {
}
