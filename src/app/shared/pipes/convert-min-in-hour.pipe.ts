import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertMinInHour',
})
export class ConvertMinInHourPipe implements PipeTransform {

  transform(value: number): number {
    return value / 60;
  }

}
