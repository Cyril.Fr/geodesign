import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertKmInMeter',
})
export class ConvertKmInMeterPipe implements PipeTransform {

  transform(value: number): number {
    return value / 1000;
  }

}
