import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'decodeHour',
})
export class DecodeHourPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const hoursMinutes: string[] = value.toString().split(/[.:]/);
    return parseInt(hoursMinutes[0], 10) + (hoursMinutes[1] ? parseInt(hoursMinutes[1], 10) : 0) / 60;
  }

}
