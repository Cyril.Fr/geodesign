import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Pipe({
  name: 'formatNumber',
})
export class FormatNumberPipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  transform(value: number): any {
    const dist: number = Math.round(value);
    if (dist > 1000000) {
      return `${Math.round(dist / 10000) / 100} ${this.translateService.instant('UNIT.MILLION')}`;
    } else if (dist > 999) {
      return dist.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    }
    return dist;
  }

}
