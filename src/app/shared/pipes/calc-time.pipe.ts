import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Pipe({
  name: 'calcTime',
})
export class CalcTimePipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  transform(value: number): any {
    return `${Math.floor(value)} \
    ${this.translateService.instant('UNIT.ABBR.MINUTE')} \
    ${this.translateService.instant('UNIT.AND')} \
    ${Math.round((value - Math.floor(value)) * 60)}${this.translateService.instant('UNIT.ABBR.SECOND')}`;
  }

}
