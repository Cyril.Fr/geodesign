import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'addPx',
})
export class AddPxPipe implements PipeTransform {

  transform(value: number, ...args: any[]): string {
      return value ? `${value}px` : '';
  }
}
