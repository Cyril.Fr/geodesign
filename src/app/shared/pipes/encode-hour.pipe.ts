import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'encodeHour',
})
export class EncodeHourPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return moment().startOf('day').add(value, 'minutes').format('m:ss');
  }

}
