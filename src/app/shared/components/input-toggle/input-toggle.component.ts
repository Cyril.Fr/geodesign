import {Component, OnInit, Input, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'geodesign-input-toggle',
  templateUrl: './input-toggle.component.html',
  styleUrls: ['./input-toggle.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputToggleComponent),
    multi: true,
  }],
})
export class InputToggleComponent implements OnInit, ControlValueAccessor {

  @Input() id: string = '';
  @Input() radioToggle: boolean = false;
  private _value: any;

  constructor() {
  }

  ngOnInit(): void {
  }

  @Input('value')
  set value(val) {
    this._value = val;
    this.onChange(this._value);
  }

  get value(): any {
    return this._value;
  }

  onChange: any = () => {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(value): void {
    this.value = value;
  }

  setValue(checked: boolean): void {
    this.value = checked;
  }

}
