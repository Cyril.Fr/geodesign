import {Component, EventEmitter, forwardRef, Input, OnInit, Output} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'geodesign-input-checkbox',
  templateUrl: './input-checkbox.component.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputCheckboxComponent),
    multi: true,
  }],
})
export class InputCheckboxComponent implements OnInit, ControlValueAccessor {

  @Input() labelPosition: 'before' | 'after' = 'after';
  @Input() label: string;
  @Input() defaultCheckbox: boolean = true;
  @Output() checkboxChange: EventEmitter<void> = new EventEmitter();

  id: string = Math.random().toString(36).substr(2, 9);

  private _value: any;

  constructor() {
  }

  ngOnInit(): void {
  }

  @Input('value')
  set value(val) {
    this._value = val;
    this.onChange(this._value);
  }

  get value(): any {
    return this._value;
  }

  onChange: any = () => {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(value): void {
    this.value = value;
  }

  onCheckboxChange(): void {
    this.checkboxChange.emit();
  }

}
