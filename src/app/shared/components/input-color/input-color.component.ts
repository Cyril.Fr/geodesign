import {Component, OnInit, Input, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'geodesign-input-color',
  templateUrl: './input-color.component.html',
  styleUrls: ['./input-color.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputColorComponent),
    multi: true,
  }],
})
export class InputColorComponent implements OnInit, ControlValueAccessor {
  private _value: any;

  @Input() label: string = '';
  @Input() fromStopPopup: boolean = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  @Input('value')
  set value(val) {
    this._value = val;
    this.onChange(this._value);
  }

  get value(): any {
    return this._value;
  }

  onChange: any = () => {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(value): void {
    this.value = value;
  }

  setValue(color: string): void {
    this.value = color;
  }

}
