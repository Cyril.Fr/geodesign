import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'geodesign-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {

  @Output() clickButton: EventEmitter<void> = new EventEmitter<void>();
  @Input() type: 'button' | 'submit' = 'button';
  @Input() styles: 'primary' | 'secondary' | 'icons' | 'tertiary' = 'primary';

  constructor() {
  }

  ngOnInit(): void {
  }

  buttonClick(): void {
    this.clickButton.emit();
  }
}
