import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'geodesign-link',
  templateUrl: './link.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class LinkComponent implements OnInit {

  @Input() routerLink: string;

  @Input() href: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
