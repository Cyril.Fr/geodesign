import {ChangeDetectionStrategy, Component, forwardRef, Injector, Input, OnInit, Type} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';
import {ErrorService} from '../../../core/services/error.service';

@Component({
  selector: 'geodesign-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputTextComponent),
    multi: true,
  }],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputTextComponent implements OnInit, ControlValueAccessor {

  @Input() disabled: boolean = false;
  @Input() placeholder: string = '';
  @Input() options: string[] = [];
  @Input() maxLength: number = null;
  @Input() hintStart: string;
  @Input() hintEnd: string;
  @Input() suffix: string;
  @Input() prefix: string;
  @Input() type: 'text' | 'number' = 'text';

  filteredOptions: string[];
  errorStateMatcher: ErrorStateMatcher = {
    isErrorState: (): boolean => {
      return (this._control && this.invalid) ? (this._control.dirty || this._control.touched) : false;
    },
  };

  public errorTxt: string = '';
  private _value: any;
  private _control: NgControl;

  @Input('value')
  set value(val) {
    this._value = val;
    if (this.options.length > 0) {
      const filterValue: string = val ? val.toLowerCase() : '';
      this.filteredOptions = this.options.filter((option: string) => option.toLowerCase().includes(filterValue));
    }
    this.onChange(this._value);
  }

  // get value
  get value(): any {
    return this._value;
  }

  constructor(private _injector: Injector, private errorService: ErrorService) {
  }

  ngOnInit(): void {
    this._control = this._injector.get<NgControl>(NgControl as Type<NgControl>);
    this._control.valueAccessor = this;
  }

  // check if this formControl is invalid
  get invalid(): boolean {
    return this._control ? this._control.invalid : false;
  }

  get showError(): boolean {
    this.errorTxt = this.errorService.getErrorTxt(this._control);
    return (this._control && this.invalid) ? (this._control.dirty || this._control.touched) : false;
  }

  onChange: any = () => {
  }

  onTouched: any = () => {
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  // call when a new value is apply
  writeValue(value): void {
    this.value = value;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  trackByFn(index: number): number {
    return index;
  }

}
