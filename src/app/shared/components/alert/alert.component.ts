import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'geodesign-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {

  @Input() type: 'error' | 'warning' | 'success' = 'error';

  constructor() {
  }

  ngOnInit(): void {
  }

}
