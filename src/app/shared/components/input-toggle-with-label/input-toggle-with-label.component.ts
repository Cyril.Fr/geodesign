import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'geodesign-input-toggle-with-label',
  templateUrl: './input-toggle-with-label.component.html',
  styleUrls: ['./input-toggle-with-label.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputToggleWithLabelComponent),
    multi: true,
  }],
})
export class InputToggleWithLabelComponent implements OnInit, ControlValueAccessor {

  @Input() label: string;
  @Input() label2: string;
  @Input() disabled: boolean = false;
  id: string = Math.random().toString(36).substr(2, 9);
  id2: string = Math.random().toString(36).substr(2, 9);
  private _value: any;

  constructor() {
  }

  ngOnInit(): void {
  }

  @Input('value')
  set value(val) {
    this._value = val;
    this.onChange(this._value);
  }

  get value(): any {
    return this._value;
  }

  onChange: any = () => {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(value): void {
    this.value = value;
  }

}
