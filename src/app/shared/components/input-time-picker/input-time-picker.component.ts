import {Component, EventEmitter, Injector, Input, OnInit, Output, Type} from '@angular/core';
import {ErrorStateMatcher} from '@angular/material';
import {ControlValueAccessor, NgControl} from '@angular/forms';
import {ErrorService} from '../../../core/services/error.service';

@Component({
  selector: 'geodesign-input-time-picker',
  templateUrl: './input-time-picker.component.html',
})
export class InputTimePickerComponent implements OnInit, ControlValueAccessor {

  @Input() placeholder: string = '';
  @Input() minlength: number = 8;
  @Output() timeSet: EventEmitter<any> = new EventEmitter();

  public disabled: boolean = false;
  public errorTxt: string = '';
  private _value: any;
  private _control: NgControl;

  errorStateMatcher: ErrorStateMatcher = {
    isErrorState: (): boolean => {
      return (this._control && this.invalid) ? (this._control.dirty || this._control.touched) : false;
    },
  };

  @Input('value')
  set value(val) {
    this._value = val;
    this.onChange(this._value);
  }

  // get value
  get value(): any {
    return this._value;
  }

  constructor(private _injector: Injector, private errorService: ErrorService) {
  }

  ngOnInit(): void {
    this._control = this._injector.get<NgControl>(NgControl as Type<NgControl>);
    this._control.valueAccessor = this;
  }

  // check if this formControl is invalid
  get invalid(): boolean {
    return this._control ? this._control.invalid : false;
  }

  get showError(): boolean {
    this.errorTxt = this.errorService.getErrorTxt(this._control);
    return (this._control && this.invalid) ? (this._control.dirty || this._control.touched) : false;
  }

  onChange: any = () => {
  }

  onTouched: any = () => {
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouched = fn;
  }

  // call when a new value is apply
  writeValue(value): void {
    this.value = value;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onTimeSet(e): void {
    this.timeSet.emit(e);
  }
}
