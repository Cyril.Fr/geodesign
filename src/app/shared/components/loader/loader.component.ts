import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'geodesign-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {

  @Input() size: 'large' | 'small' = 'small';
  @Input() color: '#fff' | '#f00' = '#f00';

  constructor() {
  }

  ngOnInit(): void {
  }

}
