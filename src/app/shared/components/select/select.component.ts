import {ChangeDetectionStrategy, Component, Injector, Input, OnInit, Type} from '@angular/core';
import {ControlValueAccessor, NgControl} from '@angular/forms';
import {Options} from '../../../core/models/options.model';
import {ErrorStateMatcher} from '@angular/material';
import {ErrorService} from '../../../core/services/error.service';

@Component({
  selector: 'geodesign-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectComponent implements OnInit, ControlValueAccessor {

  itemSize: number = 42;

  disabled: boolean = false;
  @Input() options: Options[] | { label: string, value: string, disabled?: boolean }[];
  @Input() label: string;
  @Input() selected: string;
  @Input() group: boolean = false;
  @Input() multiple: boolean = false;

  public errorTxt: string = '';
  private _value: any;
  private _control: NgControl;

  errorStateMatcher: ErrorStateMatcher = {
    isErrorState: (): boolean => {
      return (this._control && this.invalid) ? (this._control.dirty || this._control.touched) : false;
    },
  };

  @Input('value')
  set value(val) {
    this._value = val;
    this.onChange(this._value);
  }

  // get value
  get value(): any {
    return this._value;
  }

  constructor(private _injector: Injector, private errorService: ErrorService) {
  }

  ngOnInit(): void {
    this._control = this._injector.get<NgControl>(NgControl as Type<NgControl>);
    this._control.valueAccessor = this;
  }

  // check if this formControl is invalid
  get invalid(): boolean {
    return this._control ? this._control.invalid : false;
  }

  get showError(): boolean {
    this.errorTxt = this.errorService.getErrorTxt(this._control);
    return (this._control && this.invalid) ? (this._control.dirty || this._control.touched) : false;
  }

  onChange: any = () => {
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
  }

  // call when a new value is apply
  writeValue(value): void {
    this.value = value;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  trackByFn(index: number): number {
    return index;
  }

  calculateHeightSelect(): number {
    return this.options.length > 5 ? 5 * this.itemSize : this.options.length * this.itemSize;
  }
}
