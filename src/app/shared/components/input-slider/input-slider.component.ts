import {Component, forwardRef, Input, OnInit, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {MatSlider} from '@angular/material/slider';

@Component({
  selector: 'geodesign-input-slider',
  templateUrl: './input-slider.component.html',
  styleUrls: ['./input-slider.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputSliderComponent),
      multi: true,
    },
  ],
})
export class InputSliderComponent implements OnInit, ControlValueAccessor {

  constructor() {
  }

  private _unit: string;

  @Input() max: number;
  @Input() min: number = 0;
  @Input() thumbLabel: boolean = true;
  @Input() step: number;
  @Input() tickInterval: number;
  @Input() label: string;
  @Input() size: 'normal' | 'large' = 'normal';

  @Input('unit')
  set unit(unit) {
    this._unit = unit;
  }

  get unit(): string {
    return this._unit;
  }

  private _value: any;

  @Input('value')
  set value(val) {
    this._value = val;
    this.onChange(this._value);
  }

  // get value
  get value(): any {
    return this._value;
  }

  ngOnInit(): void {
  }

  onChange: any = () => {
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(value): void {
    this.value = value;
  }

  displayWith = (value: number): string | number => {
    return this.unit ? `${value}${this.unit}` : value;
  }
}
