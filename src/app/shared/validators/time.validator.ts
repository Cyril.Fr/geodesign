import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';
import {DecodeHourPipe} from '../pipes/decode-hour.pipe';

export function time(): ValidatorFn {
  return (group: FormGroup): ValidationErrors | null => {
    const decodeHourPipe: DecodeHourPipe = new DecodeHourPipe();
    return decodeHourPipe.transform(group.value.debut) > decodeHourPipe.transform(group.value.fin) ? {time: true} : null;
  };
}
