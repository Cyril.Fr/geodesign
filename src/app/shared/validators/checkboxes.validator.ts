import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export function minCheckboxes(min: number): ValidatorFn {
  return (group: FormGroup): ValidationErrors | null => {
    const selectedNumber: number = Object.keys(group.value).filter((n: string) => group.value[n]).length;
    return selectedNumber < min ? {min: true} : null;
  };
}

export function maxCheckboxes(max: number): ValidatorFn {
  return (group: FormGroup): ValidationErrors | null => {
    const selectedNumber: number = Object.keys(group.value).filter((n: string) => group.value[n]).length;
    return selectedNumber > max ? {max: true} : null;
  };
}
