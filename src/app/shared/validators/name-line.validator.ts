import {FormControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import {Line} from '../../core/models/line.model';
import {LineService} from '../../core/services/line.service';

export function nameLine(line: Line): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const foundLine: Line = LineService.currentLines.find(
      (currentLine: Line) => line ? currentLine.nom === control.value && currentLine.id !== line.id : currentLine.nom === control.value);
    return foundLine ? {nameExist: true} : null;
  };
}
