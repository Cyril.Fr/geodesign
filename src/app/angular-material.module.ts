import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatExpansionModule,
  MatMenuModule,
  MatIconModule,
  MatSidenavModule,
  MatTabsModule,
  MatSlideToggleModule, MatSortModule,
} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatSliderModule} from '@angular/material/slider';
import {MatSelectModule} from '@angular/material/select';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatListModule} from '@angular/material/list';
import {ScrollingModule} from '@angular/cdk/scrolling';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatSelectModule,
    MatMenuModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatButtonModule,
    MatExpansionModule,
    MatCardModule,
    MatTabsModule,
    MatIconModule,
    NgxMaterialTimepickerModule,
    MatListModule,
    MatSortModule,
    ScrollingModule,
  ],
  exports: [
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatMenuModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatButtonModule,
    MatExpansionModule,
    MatCardModule,
    MatTabsModule,
    MatIconModule,
    NgxMaterialTimepickerModule,
    MatListModule,
    MatSortModule,
    ScrollingModule,
  ],
})

export class AngularMaterialModule {
}
