import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addPoints',
})
export class AddPointsPipe implements PipeTransform {

  transform(value: string): any {
    if (typeof value === 'string') {
      return `${value}:`;
    }
    return '';
  }

}
