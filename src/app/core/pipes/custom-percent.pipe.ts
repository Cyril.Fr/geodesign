import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customPercent',
})
export class CustomPercentPipe implements PipeTransform {

  transform(value: any, ...args: any[]): string {
    if (value && typeof value === 'number') {
      return `${value}%`;
    }
    return null;
  }

}
