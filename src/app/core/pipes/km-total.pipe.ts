import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'kmTotal',
})
export class KmTotalPipe implements PipeTransform {

  transform(value: number, ...args: any[]): string | number {
    if (value && typeof value === 'number') {
      const nbr: number | string = (value > 1000000) ? (value / 1000000).toFixed(2) : value;
      return `${nbr} ${(value > 1000000) ? 'M' : ''}km`;
    }
    return 0;
  }

}
