import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coutTotal',
})
export class CoutTotalPipe implements PipeTransform {

  transform(value: any, ...args: any[]): string | number {
    const currency: string = args[0].currency;
    if (value && typeof value === 'number') {
      return `${Math.round(value / 1000 )}k ${currency}`;
    }
    return 0;
  }

}
