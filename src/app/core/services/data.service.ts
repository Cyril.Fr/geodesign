import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {NetworkService} from './network.service';
import {map} from 'rxjs/operators';
import {GeoJsonLines} from '../models/map/lines.model';
import {GeoJsonStop, GeoJsonStopFeature} from '../models/map/stops.model';
import {GeoJsonIsochrones, GeoJsonIsochronesFeature} from '../models/map/isochrones.model';
import {GeoJsonServiceMob, GeoJsonServiceMobFeature} from '../models/map/service-mob.model';
import {GeoJsonMouvementFeature, GeoJsonMouvements} from '../models/map/mouvements.model';
import {GeoJsonStopInfos, GeoJsonStopInfosFeature} from '../models/map/popemp.model';
import {GeoJsonInsee, GeoJsonInseeFeature} from '../models/map/insee.model';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private _lines: GeoJsonLines;
  public lines$: BehaviorSubject<GeoJsonLines> = new BehaviorSubject(this._lines);

  set lines(value: GeoJsonLines) {
    this._lines = value;
    this.lines$.next(this._lines);
  }

  private _stops: GeoJsonStop;
  public stops$: BehaviorSubject<GeoJsonStop> = new BehaviorSubject<GeoJsonStop>(this._stops);

  set stops(value: GeoJsonStop) {
    this._stops = value;
    this.stops$.next(this._stops);
  }

  get stops(): GeoJsonStop {
    return this._stops;
  }

  private _isochrones: GeoJsonIsochrones;
  public isochrones$: BehaviorSubject<GeoJsonIsochrones> = new BehaviorSubject(this._isochrones);

  set isochrones(value: GeoJsonIsochrones) {
    this._isochrones = value;
    this.isochrones$.next(this._isochrones);
  }

  get isochrones(): GeoJsonIsochrones {
    return this._isochrones;
  }

  private _serviceMobs: GeoJsonServiceMob;
  public serviceMobs$: BehaviorSubject<GeoJsonServiceMob> = new BehaviorSubject<GeoJsonServiceMob>(this._serviceMobs);

  set serviceMobs(value: GeoJsonServiceMob) {
    this._serviceMobs = value;
    this.serviceMobs$.next(this._serviceMobs);
  }

  get serviceMobs(): GeoJsonServiceMob {
    return this._serviceMobs;
  }

  public currentEnquete$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private _currentEnquete: string;
  set currentEnquete(strArray: string) {
    this._currentEnquete = strArray;
    if (this._currentEnquete) {
      this.currentEnquete$.next(this._currentEnquete);
    }
  }

  private _mouvements: GeoJsonMouvements;
  public mouvements$: BehaviorSubject<GeoJsonMouvements> = new BehaviorSubject<GeoJsonMouvements>(this._mouvements);

  set mouvements(mv: GeoJsonMouvements) {
    this._mouvements = mv;
    if (this._mouvements) {
      this.mouvements$.next(this._mouvements);
    }
  }

  private _stopInfos: GeoJsonStopInfos;
  public stopInfos$: BehaviorSubject<GeoJsonStopInfos> = new BehaviorSubject<GeoJsonStopInfos>(this._stopInfos);

  set stopInfos(si: GeoJsonStopInfos) {
    this._stopInfos = si;
    if (this._stopInfos) {
      this.stopInfos$.next(this._stopInfos);
    }
  }

  constructor(public apiService: ApiService) {
  }

  importData(body: any, url: string,
             createFn: (data: any) => any,
             parseFn: (str: string) => any = (str: string): any => JSON.parse(str)): Observable<any> {
    body = {
      ...body,
      schema: NetworkService.getNetworkName(),
    };
    return this.apiService.prepareRequest('POST', url, body, 'text').pipe(
      map(parseFn),
      map(createFn));
  }

  importLines(body?: any): Observable<any> {
    if (!body) {
      body = {schema: NetworkService.getNetworkName()};
    }
    return this.apiService.prepareRequest('POST', '/api/importPGLine.php', body, 'text').pipe(
      map((geoLinesString: string) => JSON.parse(geoLinesString)),
      map((geoLines: any) => this.lines = new GeoJsonLines(geoLines)));
  }

  importStops(body?: any): Observable<any> {
    if (!body) {
      body = {schema: NetworkService.getNetworkName()};
    }
    return this.apiService.prepareRequest('POST', '/api/importPGStop.php', body, 'text').pipe(
      map((geoStopsString: string) => JSON.parse(geoStopsString)),
      map((geoStops: GeoJsonStopFeature) => this.stops = new GeoJsonStop(geoStops)));
  }

  importAllDesserveStops(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/getAllStopProx.php', body, 'text').pipe(
      map((geoDesserveStopGroupedString: string) => JSON.parse(geoDesserveStopGroupedString)),
      map((geoDesserveStopGroupedFeature: GeoJsonStopFeature) => new GeoJsonStop(geoDesserveStopGroupedFeature))
    );
  }

  importIsochrones(body: any): Observable<GeoJsonIsochrones> {
    return this.apiService.prepareRequest('POST', '/api/getIsochrone.php', body, 'text').pipe(
      map((geoIsochronesString: string) => JSON.parse(geoIsochronesString)),
      map((geoIsochrones: GeoJsonIsochronesFeature) => this.isochrones = new GeoJsonIsochrones(geoIsochrones)));
  }

  importActualEnquete(body?: any): Observable<any> {
    if (!body) {
      body = {schema: NetworkService.getNetworkName()};
    }
    return this.apiService.prepareRequest('POST', '/api/getActualEnquete.php', body, 'text').pipe(
      map((enqueteString: string) => this.currentEnquete = JSON.parse(enqueteString)[0].last_md)
    );
  }

  importMouvements(currentEnquete: string): Observable<any> {
    const body: object = {
      schema: NetworkService.getNetworkName(),
      enquete: currentEnquete,
    };
    if (currentEnquete) {
      return this.apiService.prepareRequest('POST', '/api/createODLayer.php', body, 'text').pipe(
        map((mvString: string) => JSON.parse(mvString)),
        map((mouvements: GeoJsonMouvementFeature) => this.mouvements = new GeoJsonMouvements(mouvements))
      );
    } else {
      return of(null);
    }
  }

  importPopEmp(): Observable<any> {
    const body: object = {
      schema: NetworkService.getNetworkName(),
    };
    return this.apiService.prepareRequest('POST', '/api/createPopEmpLayer.php', body, 'text').pipe(
      map((stopInfosStr: string) => JSON.parse(stopInfosStr)),
      map((stopInfos: GeoJsonStopInfosFeature) => this.stopInfos = new GeoJsonStopInfos(stopInfos))
    );
  }
}
