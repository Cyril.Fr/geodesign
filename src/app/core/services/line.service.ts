import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Line} from '../models/line.model';
import {ApiService} from './api.service';
import {PartielForLine} from '../models/partiel-for-line.model';
import {map} from 'rxjs/operators';
import {LineUtilService} from './line-util.service';
import {InfoPopulation} from '../models/info-population.model';
import {LoggerService} from './logger.service';
import {LineInformation} from '../models/line-information.model';
import {InfoPopType} from '../models/info-pop-type.model';

@Injectable({
  providedIn: 'root',
})
export class LineService {

  static currentLines: Line[] = [];
  public lines$: BehaviorSubject<Line[]> = new BehaviorSubject<Line[]>(LineService.currentLines);
  public openLine$: BehaviorSubject<Line> = new BehaviorSubject(null);

  static updateLine(line: Line, property: string, value: any): void {
    LineService.currentLines.find((lineSearch: Line) => line.id === lineSearch.id)[property] = value;
  }

  /**
   * Return the last line
   * @param lineType 'regular' | 'tad' | null
   */
  static getLastLineFromType(lineType: 'regular' | 'tad' | null): Line {
    return LineService.currentLines.reduce((previousLine: Line, currentLine: Line) => {
      return currentLine.id > previousLine.id ? currentLine : previousLine;
    });
  }

  constructor(private apiService: ApiService,
              private logger: LoggerService) {
  }

  openLine(line: Line): void {
    this.openLine$.next(line);
  }

  getLines(body: any): Observable<Line[]> {
    return this.apiService.prepareRequest('POST', '/api/getInfo.php', body).pipe(
      map((lines: any) => {
        LineService.currentLines = lines.map((line: any) => new Line(line));
        this.lines$.next(LineService.currentLines);
        return LineService.currentLines;
      })
    );
  }

  getLinesName(networkName: string): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/getLineName.php', {schema: networkName});
  }

  createRegularLine(body: any, schema: string): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/createLine.php', {schema, ...body}, 'text');
  }

  modifyRegularLine(body: any, schema: string): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/modifLine.php', {schema, ...body});
  }

  deleteLine(id: number, schema: string): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/delete.php', {dataId: id, schema}).pipe(
      map(() => {
        LineService.currentLines = LineService.currentLines.filter((line: Line) => line.id !== id);
        this.lines$.next(LineService.currentLines);
      })
    );
  }

  createTadLine(body: any, schema: string): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/createTadLine.php', {schema, ...body}, 'text');
  }

  getPartielForLine(body: any): Observable<PartielForLine[]> {
    return this.apiService.prepareRequest('POST', '/api/getPartielForLine.php', body).pipe(
      map((lines: any) => {
        return Array.isArray(lines) ? lines.map((line: any) => new PartielForLine(line)) : [];
      })
    );
  }

  changeOperational(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/changeOperational.php', body);
  }

  setAR(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/setAR.php', body);
  }

  getInfoPop(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/getInfoPop2.php', body).pipe(
      map((infos: any) => {
        return infos.map((info: any) => new InfoPopulation(info));
      })
    );
  }

  getInfoPopTot(body: any): Observable<InfoPopulation> {
    return this.apiService.prepareRequest('POST', '/api/getInfoPopTot2.php', body);
  }

  getInfoPopType(body: any): Observable<InfoPopType[]> {
    return this.apiService.prepareRequest('POST', '/api/getInfoPopType.php', body);
  }

  getDefaultOperionalJSON(networkName: string, lineType: string): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/createOperationalJsonFromDatabase.php', {
      schema: networkName,
      lineType,
    });
  }

  getInformationLine(line: Line): LineInformation {
    const partielsForLine: any[] = line.partiels;

    const lineInformation: LineInformation = {
      length: LineUtilService.getLengthLine(line),
      roundTripTime: LineUtilService.getRotationTimeLine(line),
      commercialKm: LineUtilService.getCommercialKm(line),
      busNeed: LineUtilService.getTotalBusMax(line),
    };

    partielsForLine.forEach((partielForLine: PartielForLine) => {
      lineInformation.busNeed += LineUtilService.getTotalBusMax(partielForLine);
      lineInformation.commercialKm += LineUtilService.getCommercialKm(partielForLine);
    });

    return lineInformation;
  }
}
