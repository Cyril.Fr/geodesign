import {Injectable} from '@angular/core';
import * as L from 'leaflet';
import {Circle, GeoJSON, LatLng} from 'leaflet';
import {TranslateService} from '@ngx-translate/core';
import {NbcoursesDataService} from './nbcourses-data.service';
import {LayerService} from '../layer.service';

@Injectable({
  providedIn: 'root',
})
export class NbCoursesLayerService extends LayerService {

  nbCoursesLayer: GeoJSON;

  constructor(private nbcoursesDataService: NbcoursesDataService, private translate: TranslateService) {
    super();
  }

  createNbCourses(feature: GeoJSON.Feature, latlng: LatLng): Circle {
    const radius: number = Math.sqrt(
      feature.properties.nbCourses * Math.pow(150, 2) / this.nbcoursesDataService.maxNbCourses);
    return L.circle(latlng, radius,
      this.styleLayer(
        '#223344',
        '#5555dd',
        0.1,
        0.5,
        0,
        1
      ))
      .bindTooltip(this.translate.instant('NB_COURSES.COURSES_VALUE', {value: feature.properties.nbCourses}),
        this.styleTooltip());
  }

  updateTooltip(map: L.Map): void {
    if (this.nbCoursesLayer) {
      const mapZoom: number = map.getZoom();
      const bounds: any = map.getBounds();
      this.nbCoursesLayer.eachLayer((layer: Circle) => {
        layer.unbindTooltip();
        const permanent: boolean = bounds.contains(layer.getLatLng()) &&
          (mapZoom > 16 || mapZoom > 15 && layer.options.radius > 50);
        layer.bindTooltip(this.translate.instant('NB_COURSES.COURSES_VALUE', {value: layer.feature.properties.nbCourses}),
          this.styleTooltip(permanent));
      });
    }
  }

  private initNbCourseLayer(): void {
    this.nbCoursesLayer = this.createLeafletData(
      this.nbcoursesDataService.nbCourses,
      (feature: GeoJSON.Feature, latlng: LatLng): Circle => this.createNbCourses(feature, latlng),
      null
    );
  }

  getLayer(): GeoJSON {
    if (!this.nbCoursesLayer) {
      this.initNbCourseLayer();
    }
    return this.nbCoursesLayer;
  }
}
