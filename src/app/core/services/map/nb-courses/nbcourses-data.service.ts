import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {GeoJsonNbCourses, GeoJsonNbCoursesFeature} from '../../../models/map/nb-courses.model';
import {ApiService} from '../../api.service';
import {DataService} from '../../data.service';

@Injectable({
  providedIn: 'root',
})
export class NbcoursesDataService extends DataService {

  constructor(apiService: ApiService) {
    super(apiService);
  }

  private _nbCourses: GeoJsonNbCourses;
  public maxNbCourses: number;

  set nbCourses(nc: GeoJsonNbCourses) {
    this._nbCourses = nc;
    if (this._nbCourses) {
      this.maxNbCourses = this.extractMaxCourses();
    }
  }
  get nbCourses(): GeoJsonNbCourses {
    return this._nbCourses;
  }

  private extractMaxCourses(): number {
    return Math.max(...this._nbCourses.features.map((feature: GeoJsonNbCoursesFeature) => feature.properties.nbCourses));
  }

  importNbCourses(): Observable<any> {
    const url: string = '/api/getInfoStopsCourseV2.php';
    const createFn: any = (data: any): GeoJsonNbCourses => this.nbCourses = new GeoJsonNbCourses(data);
    return this.importData({}, url, createFn);
  }
}
