import {Injectable} from '@angular/core';
import {ApiService} from '../../api.service';
import {DataService} from '../../data.service';
import {GeoJsonCovers} from '../../../models/map/covers.model';
import {Observable} from 'rxjs';
import {GeoJson} from '../../../models/map/map.model';

@Injectable({
  providedIn: 'root',
})
export class CoversDataService extends DataService {

  private _covers: GeoJsonCovers;
  set covers(value: GeoJsonCovers) {
    this._covers = value;
  }

  get covers(): GeoJsonCovers {
    return this._covers;
  }

  constructor(apiService: ApiService) {
    super(apiService);
  }

  importCovers(): Observable<GeoJson> {
    const url: string = '/api/importPGCouverture.php';
    const parseFn: any = (str: string): any => JSON.parse(JSON.parse(str).geom);
    const createFn: any = (data: any): GeoJsonCovers => this.covers = new GeoJsonCovers(data);
    return this.importData({}, url, createFn, parseFn);
  }
}
