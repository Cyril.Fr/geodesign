import {Injectable} from '@angular/core';
import {Layer} from 'leaflet';
import {CoversDataService} from './covers-data.service';
import {LayerService} from '../layer.service';

@Injectable({
  providedIn: 'root',
})
export class CoversLayerService extends LayerService {

  coversLayer: Layer;

  constructor(private coversDataService: CoversDataService) {
    super();
  }

  getLayer(): Layer {
    if (!this.coversLayer) {
      this.initLayer();
    }
    return this.coversLayer;
  }

  private initLayer(): void {
    this.coversLayer = this.createLeafletData(this.coversDataService.covers, null, null,
      () => this.styleLayer('#000', null, 0.65, undefined, undefined, 1));
  }
}
