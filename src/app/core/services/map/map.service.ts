import {ElementRef, Injectable} from '@angular/core';
import * as L from 'leaflet';
import {GeoJSON, LatLng, Layer, Map} from 'leaflet';
import {ApiService} from '../api.service';
import {GeoJson} from '../../models/map/map.model';
import {DataService} from '../data.service';
import 'leaflet-bing-layer';
import {BehaviorSubject} from 'rxjs';
import {GeoJsonLines} from '../../models/map/lines.model';
import {NbCoursesLayerService} from './nb-courses/nb-courses-layer.service';
import {CoversLayerService} from './covers/covers-layer.service';
import {EmitsLayerService} from './emits/emits-layer.service';
import {IsochronesLayerService} from './isochrones/isochrones-layer.service';
import {InseeLayerService} from './insee/insee-layer.service';

@Injectable({
  providedIn: 'root',
})
export class MapService {
  public updateTooltip$: BehaviorSubject<number> = new BehaviorSubject<any>(null);

  constructor(private apiService: ApiService,
              private dataService: DataService,
              private nbCoursesLayerService: NbCoursesLayerService,
              private coversLayerService: CoversLayerService,
              private emitsLayerService: EmitsLayerService,
              private inseeLayerService: InseeLayerService,
              private isochronesLayerService: IsochronesLayerService) {

  }

  mapElementRef: ElementRef;
  map: Map;

  osmLayer: L.TileLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OpenStreetMap', opacity: 0.9});

  streets: L.TileLayer = L.tileLayer(
    'https://{s}.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png' +
    '?access_token=pk.eyJ1IjoiZ2VvbWF0aXF1ZWRhbyIsImEiOiJnZGw2UVBBIn0.TzEyHXie4jpkE_cvE864Qw',
    {
      attribution: 'Fond Mapbox',
      opacity: 0.9,
    });

  OSM_T: L.TileLayer = L.tileLayer('https://{s}.tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=ceb1436a59534653ae6f5926e87c68d9', {
    attribution: 'OpenStreetMap',
    opacity: 0.9,
  });

  bingAerial: L.TileLayer = (L as any).tileLayer.bing('AvRSvH09OH2xTI0xcpB8rpeGWbk8iCSZJ7pqxNAB7scLhUUGwEdAFc_QZQ3Lx4Nh', {
    type: 'Aerial',
    opacity: 1,
  });
  zoomControl: L.Control.Zoom;
  layerControl: L.Control.Layers;

  private _fullScreen: boolean = false;
  fullScreen$: BehaviorSubject<any> = new BehaviorSubject<any>(this._fullScreen);

  private _darkMode: boolean = false;
  darkMode$: BehaviorSubject<any> = new BehaviorSubject<any>(this._darkMode);

  static createLeafletData(data: GeoJson,
                           layerFn: (feature: any, latlng: LatLng) => Layer,
                           eventFn: (feature: any, layer: Layer) => void,
                           styleFn?: (feature: any) => any): GeoJSON {
    return L.geoJSON(data, {
      pointToLayer: layerFn,
      onEachFeature: eventFn,
      style: styleFn,
    });
  }

  getMapZoom(): number {
    return this.map.getZoom();
  }

  initMap(): void {
    this.map = L.map('map', {
      zoomControl: false,
      zoomSnap: 0.25,
      zoomDelta: 0.25,
      wheelPxPerZoomLevel: 240,
    }).setView([51.505, -0.09], 13);
    this.map.createPane('serviceMob');
    this.map.on({
      moveend: (): void => this.handleTooltipUpdate(),
    });
    this.map.getPane('serviceMob').style.zIndex = '300'; // Z-index between tile and overlay
    this.map.getPane('serviceMob').style.pointerEvents = 'none';

    // this.bingAerial.addTo(this.map);
    this.osmLayer.addTo(this.map);

    this.initViewMap();

    this.zoomControl = L.control.zoom({
      position: 'topright',
    }).addTo(this.map);
    this.zoomControl.getContainer().remove();

    this.layerControl = L.control.layers({
      'Bing aérien': this.bingAerial,
      Mapbox: this.streets,
      'OSM Transport': this.OSM_T,
      OpenStreetMap: this.osmLayer,
    }).addTo(this.map);
    this.layerControl.getContainer().remove();

    L.control.scale({
      imperial: false,
      position: 'bottomright',
    }).addTo(this.map);
  }

  getZoomControl(): HTMLElement {
    return (this.zoomControl.getContainer());
  }

  getLayerControl(): any {
    return (this.layerControl.getContainer());
  }

  show(layer: Layer): void {
    this.map.addLayer(layer);
  }

  hide(layer: Layer): void {
    this.map.removeLayer(layer);
  }

  showLines(linesLayer: Layer): void {
    this.map.addLayer(linesLayer);
  }

  hideLines(linesLayer: Layer): void {
    this.map.removeLayer(linesLayer);
  }

  showStops(stopsLayer: Layer): void {
    this.map.addLayer(stopsLayer);
  }

  hideStops(stopsLayer: Layer): void {
    this.map.removeLayer(stopsLayer);
  }

  showDesserveStops(desserveStopsGroupedLayer: Layer): void {
    this.map.addLayer(desserveStopsGroupedLayer);
  }

  hideDesserveStops(desserveStopsGroupedLayer: Layer): void {
    this.map.removeLayer(desserveStopsGroupedLayer);
  }

  showServiceMobs(serviceMobsLayer: GeoJSON): void {
    this.map.addLayer(serviceMobsLayer);
  }

  hideServiceMobs(serviceMobsLayer: GeoJSON): void {
    this.map.removeLayer(serviceMobsLayer);
  }

  initViewMap(): void {
    this.dataService.lines$.subscribe((lines: GeoJsonLines) => {
      if (lines) {
        this.map.setView(
          // NOT PROPERLY IMPLEMENTED BUT WORKING
          L.GeoJSON.coordsToLatLng([lines.features[0].geometry.coordinates[0][0], lines.features[0].geometry.coordinates[0][1]]), 13
        );
      }
    });
  }

  toggleFullScreen(): void {
    this._fullScreen = !this._fullScreen;
    this.fullScreen$.next(this._fullScreen);
  }

  toggleDarkMode(): void {
    this._darkMode = !this._darkMode;
    this.darkMode$.next(this._darkMode);
    this.changeOverlaysOpacity(this._darkMode ? 0.3 : 0.9);
  }

  changeOverlaysOpacity(opacity: number): void {
    this.bingAerial.setOpacity(opacity);
    this.osmLayer.setOpacity(opacity);
    this.streets.setOpacity(opacity);
    this.OSM_T.setOpacity(opacity);
  }

  setMapElementRef(elementRef: ElementRef): void {
    this.mapElementRef = elementRef;
  }

  private handleTooltipUpdate(): void {
    this.updateTooltip$.next(this.map.getZoom());
    this.nbCoursesLayerService.updateTooltip(this.map);
  }

  getLatLngFromScreenXY(x: number, y: number): LatLng {
    return this.map.containerPointToLatLng([x, y]);
  }

  showLayer(key: string, forceRecreate: boolean = false, additionalKey?: string): void {
    const layer: GeoJSON = this.extractLayer(key, forceRecreate, additionalKey);
    if (layer) {
      this.map.addLayer(layer);
    }
  }

  hideLayer(key: string, additionalKey?: string): void {
    const layer: GeoJSON = this.extractLayer(key, false, additionalKey);
    if (layer && this.map.hasLayer(layer)) {
      this.map.removeLayer(layer);
    }
  }

  private extractLayer(key: string, forceRecreate: boolean = false, additionalKey?: string): any {
    switch (key) {
      case 'nbCourses':
        return this.nbCoursesLayerService.getLayer();
      case 'covers':
        return this.coversLayerService.getLayer();
      case 'emits':
        this.emitsLayerService.mapZoom = this.map.getZoom();
        return this.emitsLayerService.getLayer(forceRecreate);
      case 'isochrones':
        return this.isochronesLayerService.getLayer();
      case 'insee':
        return this.inseeLayerService.getLayer(additionalKey);
      default:
        return null;
    }
  }
}
