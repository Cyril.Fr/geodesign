import {Injectable} from '@angular/core';
import {Layer} from 'leaflet';
import {GeoJsonIsochronesFeature} from '../../../models/map/isochrones.model';
import {LayerService} from '../layer.service';
import {IsochronesDataService} from './isochrones-data.service';

@Injectable({
  providedIn: 'root',
})
export class IsochronesLayerService extends LayerService {

  isochronesLayer: Layer;

  constructor(private isochronesDataService: IsochronesDataService) {
    super();
  }

  private initLayer(): any {
    if (this.isochronesDataService.isochrones) {
      this.isochronesLayer = this.createLeafletData(
        this.isochronesDataService.isochrones, null, null,
        (feature: GeoJsonIsochronesFeature) => this.styleLayer(
          'white',
          this.extractFillColor(feature.properties.isochroneIndex),
          undefined,
          undefined,
          undefined,
          1)
      );
    }
  }

  getLayer(): Layer {
    if (!this.isochronesLayer) {
      this.initLayer();
    }
    return this.isochronesLayer;
  }

  public removeIsochronesLayer(): void {
    this.isochronesLayer = null;
  }

  private extractFillColor(isochroneIndex: number): string {
    return isochroneIndex === 1 ? 'green' : isochroneIndex === 2 ? 'yellow' : 'red';
  }
}
