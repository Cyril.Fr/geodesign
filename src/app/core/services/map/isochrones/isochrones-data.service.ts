import {Injectable} from '@angular/core';
import {GeoJsonIsochrones} from '../../../models/map/isochrones.model';
import {DataService} from '../../data.service';
import {ApiService} from '../../api.service';
import {GeoJsonStopFeature} from '../../../models/map/stops.model';
import {ImportDataService} from '../../import-data.service';
import {LoggerService} from '../../logger.service';

@Injectable({
  providedIn: 'root',
})
export class IsochronesDataService extends DataService {

  isochrones: GeoJsonIsochrones;

  constructor(apiService: ApiService, private importDataService: ImportDataService, private logger: LoggerService) {
    super(apiService);
  }

  async generateIsochrones(stopFeature: GeoJsonStopFeature): Promise<boolean> {
    if (stopFeature) {
      try {
        this.isochrones = await this.importDataService.importIsochrones(stopFeature).toPromise();
        return true;
      } catch (e) {
        this.logger.error(e);
        return false;
      }
    }
  }
}
