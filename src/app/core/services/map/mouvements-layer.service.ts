import {Injectable} from '@angular/core';
import {MapService} from './map.service';
import * as L from 'leaflet';
import {Circle, CircleMarker, GeoJSON, LatLng} from 'leaflet';
import {DataService} from '../data.service';
import {GeoJsonMouvementFeature, GeoJsonMouvements, LeafletMouvement} from '../../models/map/mouvements.model';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class MouvementsLayerService {

  mouvementsLayer: GeoJSON;
  mouvements: GeoJsonMouvements;

  // Stop card component => update value
  lowMouvementsLimit: number = 50;
  lowMouvementsLayer: GeoJSON;
  monteeLayer: GeoJSON;
  descenteLayer: GeoJSON;
  maxMouvements: number;
  RADIUS_MAX: number = 150;

  constructor(private mapService: MapService, private dataService: DataService, private translate: TranslateService) {
    this.mapService.updateTooltip$.subscribe((mapZoom: number) => {
      if (mapZoom) {
        this.updateTooltip(mapZoom);
      }
    });
    this.dataService.mouvements$.subscribe((mouvements: GeoJsonMouvements) => {
      this.mouvements = mouvements;
      if (mouvements) {
        this.maxMouvements = this.extractMaxMouvements(mouvements);
      }
    });
  }

  static getTooltipOption(direction?: string, permanent?: boolean): object {
    return {
      permanent: permanent || false,
      direction: direction || 'center',
      className: 'mv-tooltip',
    };
  }

  static styleLowMouvements(radius: number = 10): object {
    return {
      radius,
      color: '#e44',
      weight: 2,
      opacity: 0.9,
      fillOpacity: 0,
    };
  }

  static styleMouvement(mouvementValue: number, max: number, radiusMax: number, color?: string): object {
    if (!color) {
      color = '#dd5555';
    }
    const radius: number = Math.sqrt(mouvementValue * Math.pow(radiusMax, 2) / max);
    return {
      radius,
      fillColor: color,
      color: '#223344',
      weight: 1,
      opacity: 0.1,
      fillOpacity: 0.5,
    };
  }

  showMonteeDescente(): void {
    this.monteeLayer = MapService.createLeafletData(
      this.mouvements,
      ((feature: any, latlng: LatLng): GeoJSON => this.createMontee(feature, latlng)),
      null
    );
    this.descenteLayer = MapService.createLeafletData(
      this.mouvements,
      ((feature: any, latlng: LatLng): GeoJSON => this.createDescente(feature, latlng)),
      null
    );
    this.mapService.show(this.monteeLayer);
    this.mapService.show(this.descenteLayer);
    this.updateTooltip(this.mapService.map.getZoom());
  }

  hideMonteeDescente(): void {
    if (this.monteeLayer) {
      this.mapService.hide(this.monteeLayer);
    }
    if (this.descenteLayer) {
      this.mapService.hide(this.descenteLayer);
    }
  }

  createMontee(feature: GeoJSON.Feature, latlng: LatLng): GeoJSON {
    return (L as any).semiCircle(latlng,
      MouvementsLayerService.styleMouvement(feature.properties.montee, this.maxMouvements, this.RADIUS_MAX))
      .setDirection(0, 180)
      .bindTooltip(this.translate.instant('MOUVEMENTS.UP_VALUE', {value: feature.properties.montee}),
        MouvementsLayerService.getTooltipOption('top'));
  }

  createDescente(feature: GeoJSON.Feature, latlng: LatLng): GeoJSON {
    return (L as any).semiCircle(latlng,
      MouvementsLayerService.styleMouvement(feature.properties.desc, this.maxMouvements, this.RADIUS_MAX, '#28aa18'))
      .setDirection(180, 180)
      .bindTooltip(this.translate.instant('MOUVEMENTS.DOWN_VALUE', {value: feature.properties.desc}),
        MouvementsLayerService.getTooltipOption('bottom'));
  }

  showLowMouvements(): void {
    const lowMouvements: GeoJsonMouvements = this.extractLowMouvements(this.mouvements);
    this.lowMouvementsLayer = MapService.createLeafletData(lowMouvements,
      ((feature: any, latlng: LatLng): Circle => this.createLowMouvements(feature, latlng)),
      null);
    this.mapService.show(this.lowMouvementsLayer);
  }

  hideLowMouvements(): void {
    if (this.lowMouvementsLayer) {
      this.mapService.hide(this.lowMouvementsLayer);
    }
  }

  createLowMouvements(feature: GeoJSON.Feature, latlng: LatLng): Circle {
    return L.circle(latlng, MouvementsLayerService.styleLowMouvements(this.lowMouvementsLimit))
      .bindTooltip(this.translate.instant('MOUVEMENTS.DOWN_VALUE', {value: feature.properties.desc}),
        MouvementsLayerService.getTooltipOption('bottom'));
  }

  showMouvements(): void {
    this.mouvementsLayer = MapService.createLeafletData(
      this.mouvements,
      ((feature: any, latlng: LatLng): Circle => this.createMouvement(feature, latlng)),
      null
    );
    this.mapService.show(this.mouvementsLayer);
    this.updateTooltip(this.mapService.map.getZoom());
  }

  hideMouvements(): void {
    if (this.mouvementsLayer) {
      this.mapService.hide(this.mouvementsLayer);
    }
  }

  createMouvement(feature: GeoJSON.Feature, latlng: LatLng): Circle {
    return L.circle(latlng,
      MouvementsLayerService.styleMouvement(feature.properties.total, this.maxMouvements, this.RADIUS_MAX)
    ).bindTooltip(feature.properties.total.toString(), MouvementsLayerService.getTooltipOption());
  }

  private extractMaxMouvements(mouvements: GeoJsonMouvements): number {
    return Math.max(...mouvements.features.map((mv: GeoJsonMouvementFeature) => mv.properties.total));
  }

  private updateTooltip(mapZoom: number): void {
    if (this.mouvementsLayer) {
      this.updateTooltips(this.mouvementsLayer, mapZoom, 'total');
    }
    if (this.monteeLayer) {
      this.updateTooltips(this.monteeLayer, mapZoom, 'montee', 'top', this.translate.instant('MOUVEMENTS.UP'));
    }
    if (this.descenteLayer) {
      this.updateTooltips(this.descenteLayer, mapZoom, 'desc', 'bottom', this.translate.instant('MOUVEMENTS.DOWN'));
    }
    if (this.lowMouvementsLayer) {
      this.updateTooltips(this.lowMouvementsLayer, mapZoom, 'total');
    }
  }

  private updateTooltips(layers: GeoJSON, mapZoom: number, key: string, direction?: string, additionalTooltipMsg?: string): void {
    layers.eachLayer((layer: CircleMarker): void => {
      layer.unbindTooltip();
      const permanent: boolean = this.mapService.map.getBounds().contains(layer.getLatLng()) &&
        (mapZoom > 16 || mapZoom > 15 && layer.options.radius > 25);
      layer.bindTooltip(`${additionalTooltipMsg || ''} ${(layer.feature as GeoJsonMouvementFeature).properties[key].toString()}`,
        MouvementsLayerService.getTooltipOption(direction, permanent));
    });
  }

  private extractLowMouvements(mouvements: GeoJsonMouvements): GeoJsonMouvements {
    if (mouvements) {
      const lowFeatures: GeoJsonMouvementFeature[] = mouvements.features.filter(
        (feature: GeoJsonMouvementFeature) => (feature.properties as LeafletMouvement).total < this.lowMouvementsLimit);
      return {
        ...mouvements,
        features: lowFeatures,
      };
    }
  }
}
