import {Injectable} from '@angular/core';
import * as L from 'leaflet';
import {DataService} from '../data.service';
import {Circle, CircleMarker, GeoJSON, LatLng, Layer, LeafletMouseEvent} from 'leaflet';
import {GeoJsonFeature} from '../../models/map/map.model';
import {MapService} from './map.service';
import {ImportDataService} from '../import-data.service';
import {NetworkService} from '../network.service';
import {StopService} from '../stop.service';
import {StopPopupInfo} from '../../models/stops.model';
import {BehaviorSubject} from 'rxjs';
import {DataSidenavService} from '../data-sidenav.service';
import 'leaflet-semicircle';
import {DATA_SIDENAV_WIDTH, STOPS_POPUP_SIZE} from '../../constants/popup.const';
import {GeoJsonStop, GeoJsonStopFeature} from '../../models/map/stops.model';

@Injectable({
  providedIn: 'root',
})
export class StopsLayerService {

  stopsLayer: GeoJSON;
  stops: GeoJsonStop;
  desserveStopsGrouped: GeoJsonStop;
  desserveStopsGroupedLayer: GeoJSON;
  currentTooltip: Circle;
  extendedPopup: boolean = false;

  stopPopupInfos$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  showPopup: boolean = false;
  private popupMouseEvent: LeafletMouseEvent;
  private offsetY: number;
  private offsetX: number;
  private stopId: number;
  currentEnquete: string;

  constructor(private dataService: DataService,
              private mapService: MapService,
              private importService: ImportDataService,
              private stopService: StopService,
              private sidenavService: DataSidenavService) {
    this.dataService.stops$.subscribe((stops: GeoJsonStop) => this.stops = stops);
    this.dataService.currentEnquete$.subscribe((currentEnquete: string) => this.currentEnquete = currentEnquete);
    this.importService.desserveStopsGrouped$.subscribe(
      (desserveStopsGrouped: GeoJsonStop) => this.desserveStopsGrouped = desserveStopsGrouped
    );
    this.sidenavService.sidenav$.subscribe(() => {
      if (this.showPopup) {
        this.showStopPopup(null, null, true);
      }
    });
  }

  static styleStop(feature: GeoJSON.Feature): object {
    return {
      radius: 3,
      fillColor: feature.properties.color,
      color: '#223344',
      weight: 2,
      opacity: 0.8,
      fillOpacity: 0.8,
    };
  }

  showStops(): void {
    this.stopsLayer = MapService.createLeafletData(this.stops, (feature: any, latlng: LatLng) => this.createStop(feature, latlng),
      (feature: any, layer: GeoJSON) => this.createStopEventListener(feature, layer));
    this.mapService.showStops(this.stopsLayer);
  }

  hideStops(): void {
    if (this.stopsLayer) {
      this.mapService.hideStops(this.stopsLayer);
    }
  }

  updateStopInfo(newStopFeature: GeoJsonStopFeature): void {
    const toReplaceFeature: GeoJsonStopFeature = this.stops.features.find(
      (stopFeature: GeoJsonStopFeature) => stopFeature.properties.id === newStopFeature.properties.id
    );
    this.stops.features.splice(this.stops.features.indexOf(toReplaceFeature), 1, newStopFeature);
    this.hideStops();
    this.showStops();
  }

  showDesserveStops(): void {
    this.desserveStopsGroupedLayer = MapService.createLeafletData(this.desserveStopsGrouped,
      (feature: any, latlng: LatLng) => this.createStop(feature, latlng),
      (feature: any, layer: GeoJSON) => this.createStopEventListener(feature, layer));
    this.mapService.showDesserveStops(this.desserveStopsGroupedLayer);
  }

  hideDesserveStops(): void {
    if (this.desserveStopsGroupedLayer) {
      this.mapService.hideDesserveStops(this.desserveStopsGroupedLayer);
    }
  }

  createStop(feature: GeoJSON.Feature, latlng: LatLng): Layer {
    return L.circleMarker(latlng, StopsLayerService.styleStop(feature));
  }

  createStopEventListener(feature: GeoJSON.Feature, layer: GeoJSON): void {
    layer.on({
      mouseover: (event: any): any => { // AJout cercle 300m + label "300m"
        if (this.mapService.map.getZoom() > 11) {
          const tooltipLayer: Circle = L.circle(event.target.getLatLng(), 300, {
            weight: 2,
            fillOpacity: 0.1,
            color: 'black',
            bubblingMouseEvents: false,
          }).bindTooltip('300m', {
            permanent: true,
            offset: [0, -38 * Math.pow(2, (this.mapService.map.getZoom() - 14)) - this.mapService.map.getZoom() * 10 + 14 * 10],
            direction: 'top',
          });
          this.currentTooltip = tooltipLayer;
          this.mapService.map.addLayer(tooltipLayer);
          layer.bringToFront();
        }
      },
      mouseout: (): any => {
        if (this.currentTooltip) {
          this.mapService.map.removeLayer(this.currentTooltip);
          this.currentTooltip = null;
        }
      },
      click: (e: LeafletMouseEvent): Promise<void> => this.showStopPopup(e),
    });
  }

  async showStopPopup(e: LeafletMouseEvent, resetExtended?: boolean, fromDataPanel?: boolean): Promise<void> {
    this.handleExtendedPopup(e, resetExtended);
    const event: LeafletMouseEvent = this.handlePopupOffset(e, resetExtended, fromDataPanel);
    const stopFeature: GeoJsonFeature = event.target.feature;
    if (this.stopId !== stopFeature.properties.id) {
      this.stopId = stopFeature.properties.id;
    }
    this.showPopup = true;
    this.popupMouseEvent = event;
    const stopName: string = stopFeature.properties.name;
    const stopInfo: StopPopupInfo = await this.getStopInfo(this.stopId);
    this.stopPopupInfos$.next({
      name: stopName,
      infos: stopInfo,
      feature: stopFeature,
      mouseEvent: event,
      show: this.showPopup,
    });
  }

  closeStopPopup(): void {
    this.showPopup = false;
    this.extendedPopup = false;
    this.stopPopupInfos$.next({
      name: null,
      infos: null,
      feature: null,
      mouseEvent: null,
      show: this.showPopup,
    });
  }

  async getStopInfo(id: number): Promise<StopPopupInfo> {
    const body: object = {
      id,
      schema: NetworkService.getNetworkName(),
    };
    return await this.stopService.getStopInfos(body).toPromise();
  }

  private handlePopupOffset(e: LeafletMouseEvent, resetExtended?: boolean, fromDataPanel?: boolean): LeafletMouseEvent {
    const event: LeafletMouseEvent = e || this.popupMouseEvent;
    const resetOffsetY: number = resetExtended ? this.offsetY : 0;
    const containerY: number = event.containerPoint.y + (resetOffsetY || 0);
    const containerX: number = event.containerPoint.x;
    const refY: number = this.mapService.mapElementRef.nativeElement.offsetHeight;
    const refX: number = this.calculateRefX(fromDataPanel);
    const popupWidth: number = STOPS_POPUP_SIZE.X;
    const popupHeight: number = this.extendedPopup ? STOPS_POPUP_SIZE.Y_EXTENDED : STOPS_POPUP_SIZE.Y;
    if (refY - containerY < popupHeight) {
      // Stop is too close from the bottom border modify popup position to stay on screen
      this.offsetY = popupHeight + containerY - refY;
      event.containerPoint.y -= (this.offsetY + 10);
    } else if (resetExtended) {
      event.containerPoint.y = (containerY + 10);
    }
    if (refX - containerX < popupWidth) {
      // Stop is too close from the right border modify popup position to stay on screen
      this.offsetX = popupWidth + containerX - refX;
      event.containerPoint.x -= (this.offsetX + 10);
    }
    return event;
  }

  private calculateRefX(fromDataPanel: boolean): number {
    // Calculate the total width of the map depending on the data sidenav state.
    // This function is triggered before sidebav.isOpened is updated so the logic is reversed
    let result: number;
    const mapWidth: number = this.mapService.mapElementRef.nativeElement.offsetWidth;
    if (fromDataPanel) {
      result = this.sidenavService.dataSidenavIsOpen() ? mapWidth : mapWidth - DATA_SIDENAV_WIDTH;
    } else {
      result = this.sidenavService.dataSidenavIsOpen() ? mapWidth - DATA_SIDENAV_WIDTH : mapWidth;
    }
    return result;
  }

  private handleExtendedPopup(e: LeafletMouseEvent, resetExtended: boolean): void {
    if (!e && typeof resetExtended !== 'boolean' || e && this.extendedPopup || resetExtended) {
      this.extendedPopup = false;
    }
  }
}
