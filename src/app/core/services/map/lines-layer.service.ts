import {Injectable} from '@angular/core';
import {DataService} from '../data.service';
import {Layer} from 'leaflet';
import {MapService} from './map.service';
import {GeoJsonLines} from '../../models/map/lines.model';

@Injectable({
  providedIn: 'root',
})
export class LinesLayerService {

  lines: GeoJsonLines;
  linesLayer: Layer;

  constructor(private dataService: DataService, private mapService: MapService) {
    this.dataService.lines$.subscribe((lines: GeoJsonLines) => this.lines = lines);
  }

  static colorStyle(feature: any): any {
    return {color: feature.properties.couleur};
  }

  showLines(): void {
    if (!this.linesLayer) {
      this.linesLayer = MapService.createLeafletData(this.lines, null, null, LinesLayerService.colorStyle);
    }
    this.mapService.showLines(this.linesLayer);
  }

  hideLines(): void {
    this.mapService.hideLines(this.linesLayer);
  }
}
