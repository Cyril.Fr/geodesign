import {Injectable} from '@angular/core';
import {MapService} from './map.service';
import {DataService} from '../data.service';
import {GeoJsonStopInfos, GeoJsonStopInfosFeature, LeafletStopInfos} from '../../models/map/popemp.model';
import * as L from 'leaflet';
import {Circle, GeoJSON, LatLng, Layer, LayerGroup} from 'leaflet';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class PopempLayerService {

  constructor(private mapService: MapService,
              private dataService: DataService,
              private translate: TranslateService) {
    this.dataService.stopInfos$.subscribe((stopInfos: GeoJsonStopInfos) => {
      this.stopsInfos = stopInfos;
      if (this.stopsInfos) {
        this.maxStopInfos = this.extractMax(this.stopsInfos);
      }
    });
    this.mapService.updateTooltip$.subscribe((mapZoom: number) => {
      if (mapZoom) {
        this.updateTooltip(mapZoom);
      }
    });
  }

  stopsInfos: GeoJsonStopInfos;
  maxStopInfos: number;
  popEmpLayer: LayerGroup<any>;

  static styleStopInfos(radius: number, emp?: boolean): object {
    return {
      radius,
      fillColor: emp ? '#30acf3' : '#460b62',
      color: 'white',
      weight: 0,
      fillOpacity: 0.8,
    };
  }

  private static styleTooltip(permanent?: boolean, offset?: number, startAngle?: number, stopAngle?: number): object {
    const style: any = {
      className: 'mv-tooltip',
      direction: 'center',
      permanent: permanent ? permanent : false,
    };
    if (!isNaN(offset) && !isNaN(stopAngle) && !isNaN(startAngle)) {
      style.offset = PopempLayerService.calculateOffset(offset, startAngle, stopAngle);
    }
    return style;
  }

  static calculateOffset(offset: number, startAngle: number, stopAngle: number): [number, number] {
    const x: number = offset * Math.cos(((startAngle / 2) + (stopAngle / 2) + 90) * (Math.PI / 180));
    const y: number = offset * Math.sin(((startAngle / 2) + (stopAngle / 2) + 90) * (Math.PI / 180));
    return [x, y];
  }

  createStopInfos(feature: GeoJSON.Feature, latlng: LatLng): LayerGroup<any> {
    const emp: number = (feature.properties as LeafletStopInfos).emp;
    const pop: number = (feature.properties as LeafletStopInfos).pop;
    if (pop || emp) {
      const radius: number = Math.sqrt(pop + emp * Math.pow(100, 2) / this.maxStopInfos);
      const angle: number = 360 * pop / (emp + pop);
      const popStr: string = this.translate.instant('POPEMP_TOOLTIP.POP', {value: pop});
      if (!emp) {
        return L.layerGroup(
          [L.circle(latlng, PopempLayerService.styleStopInfos(radius)).bindTooltip(popStr, PopempLayerService.styleTooltip())]
        );
      } else {
        const empStr: string = this.translate.instant('POPEMP_TOOLTIP.EMP', {value: emp});
        return L.layerGroup([
          (L as any).semiCircle(latlng, PopempLayerService.styleStopInfos(radius))
            .setDirection(angle / 2, angle)
            .bindTooltip(popStr, PopempLayerService.styleTooltip()),
          (L as any).semiCircle(latlng, PopempLayerService.styleStopInfos(radius, true))
            .setDirection(angle + ((360 - angle) / 2), 360 - angle)
            .bindTooltip(empStr, PopempLayerService.styleTooltip()),
        ]);
      }
    }
    return null;
  }

  showPopEmp(): void {
    this.popEmpLayer = MapService.createLeafletData(this.stopsInfos,
      (feature: GeoJsonStopInfosFeature, latlng: LatLng) => this.createStopInfos(feature, latlng),
      null
    );
    this.mapService.show(this.popEmpLayer);
  }

  hidePopEmp(): void {
    if (this.popEmpLayer) {
      this.mapService.hide(this.popEmpLayer);
    }
  }

  private extractMax(stopsInfos: GeoJsonStopInfos): number {
    return Math.max(
      ...stopsInfos.features.map(
        (feature: GeoJsonStopInfosFeature) => (feature.properties as LeafletStopInfos).emp + (feature.properties as LeafletStopInfos).pop
      )
    );
  }

  private updateTooltip(mapZoom: number): void {
    if (this.popEmpLayer) {
      this.popEmpLayer.eachLayer((l: GeoJSON) => {
        const layers: Layer[] = l.getLayers();
        const feature: GeoJsonStopInfosFeature = (l as any).feature;
        let popTooltipLayer: Layer;
        let empTooltipLayer: Layer;
        if (layers.length > 0) {
          popTooltipLayer = layers[0] ? layers[0] : null;
          empTooltipLayer = layers.length > 1 ? layers[1] : null;
          const radius: number = (popTooltipLayer as Circle).options.radius;
          const offset: number = 0.15 * radius * (mapZoom - 14);
          const permanent: boolean = this.mapService.map.getBounds().contains((popTooltipLayer as Circle).getLatLng()) &&
            (mapZoom > 16.5 || (mapZoom > 15.5 && radius > 25));
          if (popTooltipLayer) {
            this.updatePopTooltip(popTooltipLayer, feature, offset, permanent);
          }
          if (empTooltipLayer) {
            this.updateEmpTooltip(empTooltipLayer, feature, offset, permanent);
          }
        }

      });
    }
  }

  private updatePopTooltip(popTooltipLayer: Layer, feature: GeoJsonStopInfosFeature, offset: number, permanent: boolean): void {
    popTooltipLayer.unbindTooltip();
    const tooltipStr: string = this.translate.instant('POPEMP_TOOLTIP.POP', {value: feature.properties.pop});
    popTooltipLayer.bindTooltip(tooltipStr,
      PopempLayerService.styleTooltip(
        permanent, offset, (popTooltipLayer as any).options.startAngle, (popTooltipLayer as any).options.stopAngle
      ));
  }

  private updateEmpTooltip(empTooltipLayer: Layer, feature: GeoJsonStopInfosFeature, offset: number, permanent: boolean): void {
    empTooltipLayer.unbindTooltip();
    const tooltipStr: string = this.translate.instant('POPEMP_TOOLTIP.EMP', {value: feature.properties.emp});
    empTooltipLayer.bindTooltip(tooltipStr,
      PopempLayerService.styleTooltip(
        permanent, offset, (empTooltipLayer as any).options.startAngle, (empTooltipLayer as any).options.stopAngle
      ));
  }
}
