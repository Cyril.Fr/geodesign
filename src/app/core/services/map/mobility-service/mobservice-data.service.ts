import {Injectable} from '@angular/core';
import {DataService} from '../../data.service';
import {ApiService} from '../../api.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GeoJsonServiceMob, GeoJsonServiceMobFeature} from '../../../models/map/service-mob.model';

@Injectable({
  providedIn: 'root',
})
export class MobserviceDataService extends DataService {

  serviceMobs: GeoJsonServiceMob;
  allServiceMobs: GeoJsonServiceMob;
  currentFilter: string;


  constructor(apiService: ApiService) {
    super(apiService);
  }

  importServiceMobs(): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/importPGServiceMob.php', {}, 'text').pipe(
      map((geoServiceMobsString: string) => JSON.parse(geoServiceMobsString)),
      map((geoServiceMobs: GeoJsonServiceMobFeature) => this.serviceMobs = new GeoJsonServiceMob(geoServiceMobs))
    );
  }

  // TODO move to data
  private filterServiceMobs(): GeoJsonServiceMob {
    if (!this.currentFilter) {
      return this.allServiceMobs;
    }
    return {
      ...this.allServiceMobs,
      features: this.allServiceMobs.features ? this.allServiceMobs.features.filter(
        (smFeature: GeoJsonServiceMobFeature) => (smFeature.properties.type === this.currentFilter
          || smFeature.properties.level === this.currentFilter)) : [],
    };
  }
}
