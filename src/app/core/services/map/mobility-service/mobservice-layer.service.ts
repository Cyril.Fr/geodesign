import {Injectable} from '@angular/core';
import {DataService} from '../../data.service';
import * as L from 'leaflet';
import {GeoJSON, LatLng, LeafletMouseEvent, Marker} from 'leaflet';
import {MapService} from '../map.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {ApiService} from '../../api.service';
import {GeoJsonServiceMob, GeoJsonServiceMobFeature} from '../../../models/map/service-mob.model';
import {LayerService} from '../layer.service';

@Injectable({
  providedIn: 'root',
})
export class MobserviceLayerService extends LayerService {

  serviceMobsLayer: GeoJSON;
  sliderValue: number;
  featureId: number;

  private _displayPopup: boolean = false;
  displayPopup$: BehaviorSubject<any> = new BehaviorSubject<any>([this._displayPopup, null]);

  constructor(private dataService: DataService,
              private mapService: MapService,
              private apiService: ApiService) {
    super();
    this.dataService.serviceMobs$.subscribe((serviceMob: GeoJsonServiceMob) => this.allServiceMobs = serviceMob);
  }

  createServiceMob(feature: any, latlng: LatLng): Marker {
    const options: any = feature.properties.type === 'pem' ? {
      pane: 'serviceMob',
    } : null;
    return L.marker(latlng, options);
  }

  showServiceMobs(): void {
    this.hideServiceMobs();
    this.serviceMobs = this.filterServiceMobs();
    this.serviceMobsLayer = MapService.createLeafletData(
      this.serviceMobs,
      this.createServiceMob,
      (feature: any, layer: Marker) => this.setServiceMobIcon(feature, layer)
    );
    this.mapService.showServiceMobs(this.serviceMobsLayer);
  }

  hideServiceMobs(): void {
    if (this.serviceMobsLayer) {
      this.mapService.hideServiceMobs(this.serviceMobsLayer);
    }
  }

  // TODO stay here
  setServiceMobIcon(feature: any, layer: Marker): void {
    const serviceMobIcon: L.Icon = this.getServiceMobIcon(feature, this.sliderValue);
    layer.setIcon(serviceMobIcon);
    layer.on({
      click: (e: LeafletMouseEvent): void => this.showPopup(e),
    });
  }

  // TODO stay here
  private getServiceMobIcon(feature: GeoJsonServiceMobFeature, sliderValue: number): L.Icon {
    const taille: number = feature.properties.level === 1 ? 3 : 2;
    const type: string = feature.properties.type;
    const mapZoom: number = this.mapService.getMapZoom();
    let pixel: number;
    if (mapZoom > 14) {
      pixel = 10 + taille * 10;
    } else if (mapZoom > 12 || !mapZoom) {
      pixel = Math.floor(7.5 + taille * 7.5);
    } else {
      pixel = 5 + taille * 5;
    }
    pixel *= 0.5 + (sliderValue / 4);
    pixel = type === 'pem' ? pixel * 1.5 : pixel;
    return L.icon({
      iconUrl: `/assets/images/iconServiceMob/${type}.png`,
      iconSize: [pixel, pixel],
      iconAnchor: [pixel / 2, pixel / 2],
    });
  }

  // TODO move to popup
  changeServiceMobInfos(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/changeServiceMobInfo.php', body, 'text');
  }

  // TODO move to popup
  private showPopup(e: LeafletMouseEvent): void {
    const newFeatureId: number = e.target.feature.properties.id;
    this._displayPopup = this.featureId === newFeatureId ? !this._displayPopup : true;
    this.featureId = newFeatureId;
    this.displayPopup$.next([this._displayPopup, e]);
  }

  // TODO move to popup
  closePopup(): void {
    this._displayPopup = false;
    this.displayPopup$.next([this._displayPopup, null]);
  }

  // TODO move to popup
  updateFeature(clef: string, value: any): GeoJsonServiceMobFeature {
    const updatedFeature: GeoJsonServiceMobFeature = this.serviceMobs.features.find(
      (serviceMob: GeoJsonServiceMobFeature) => serviceMob.properties.id === this.featureId);
    updatedFeature.properties[clef] = value;
    return updatedFeature;
  }
}
