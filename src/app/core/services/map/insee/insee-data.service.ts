import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GeoJsonInsee, GeoJsonInseeFeature} from '../../../models/map/insee.model';
import {DataService} from '../../data.service';
import {ApiService} from '../../api.service';
import * as geostats from '../../../../../assets/lib/geostats-rework/geostats.min.js';

@Injectable({
  providedIn: 'root',
})
export class InseeDataService extends DataService {

  private _insee: GeoJsonInsee;
  geoStatsPopSerie: any;
  geoStatsEmpSerie: any;
  popList: number[] = [];
  empList: number[] = [];

  set insee(value: GeoJsonInsee) {
    this._insee = value;
    if (this._insee && this._insee.features) {
      this.createListAndSerie();
    }
  }

  get insee(): GeoJsonInsee {
    return this._insee;
  }

  constructor(apiService: ApiService) {
    super(apiService);
  }

  public getGeostatsSerie(key: string): any {
    return key === 'p' ? this.geoStatsPopSerie : this.geoStatsEmpSerie;
  }

  private createListAndSerie(): void {
    this.insee.features.forEach((f: GeoJsonInseeFeature) => {
      if (f.properties.e && f.properties.e !== 0) {
        this.empList.push(f.properties.e);
      }
      if (f.properties.p && f.properties.p !== 0) {
        this.popList.push(f.properties.p);
      }
    });
    this.createSeries();
  }

  importInsee(): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/importPGInsee.php', {}, 'text').pipe(
      map((inseeStr: string) => JSON.parse(inseeStr)),
      map((insees: GeoJsonInseeFeature) => this.insee = new GeoJsonInsee(insees))
    );
  }

  public createSeries(): void {
    this.geoStatsPopSerie = new geostats(this.popList);
    this.geoStatsPopSerie.getClassGeometricProgression(5);
    this.geoStatsEmpSerie = new geostats(this.empList);
    this.geoStatsEmpSerie.getClassGeometricProgression(5);
  }
}
