import {Injectable} from '@angular/core';
import * as L from 'leaflet';
import {GeoJSON, Layer} from 'leaflet';
import {LayerService} from '../layer.service';
import {InseeDataService} from './insee-data.service';

@Injectable({
  providedIn: 'root',
})
export class InseeLayerService extends LayerService {

  inseeLayerObjects: { p: GeoJSON, e: GeoJSON } = {
    p: null,
    e: null,
  };
  colors: string[] = ['#ffffb2', '#fecc5c', '#fd8d3c', '#f03b20', '#bd0026'];

  constructor(private inseeDataService: InseeDataService) {
    super();
  }

  getLayer(key: string): Layer {
    if (!this.inseeLayerObjects[key]) {
      this.initLayer(key);
    }
    return this.inseeLayerObjects[key];
  }

  private initLayer(key: string): void {
    if (this.inseeDataService.insee && key) {
      this.inseeLayerObjects[key] = (L as any).vectorGrid.slicer(
        this.inseeDataService.insee,
        {
          rendererFactory: (L.canvas as any).tile,
          interactive: false,
          minZoom: 11,
          maxZoom: 18,
          zIndex: 5,
          vectorTileLayerStyles: {
            sliced: (properties: any, zoom: any): object => {
              const p: number = properties[key];
              const opa: any = p !== 0 ? 0.3 : 0;
              const wei: any = p !== 0 ? 1 : 0;
              return {
                fillColor: this.colors[this.inseeDataService.getGeostatsSerie(key).getRangeNum(p)],
                fillOpacity: opa,
                fill: true,
                color: 'grey',
                weight: wei,
              };
            },
          },
        }
      );
    }
  }
}
