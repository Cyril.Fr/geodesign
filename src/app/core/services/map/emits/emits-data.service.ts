import {Injectable} from '@angular/core';
import {GeoJsonEmitAndGen, GeoJsonEmitAndGenFeature} from '../../../models/map/emit-gen.model';
import {Observable} from 'rxjs';
import {DataService} from '../../data.service';
import {ApiService} from '../../api.service';
import {EmitsPopupService} from './emits-popup.service';
import {LoggerService} from '../../logger.service';
import {GeoJsonFeature} from '../../../models/map/map.model';

@Injectable({
  providedIn: 'root',
})
export class EmitsDataService extends DataService {

  feature: GeoJsonFeature;
  public emits: GeoJsonEmitAndGen;
  private _allEmits: GeoJsonEmitAndGen;
  set allEmits(value: GeoJsonEmitAndGen) {
    this._allEmits = value;
  }

  get allEmits(): GeoJsonEmitAndGen {
    return this._allEmits;
  }

  private _filter: string = undefined;
  set filter(value: string) {
    if (value !== this._filter) {
      this._filter = value;
      this.filterEmits();
    }
  }

  get filter(): string {
    return this._filter;
  }

  constructor(apiService: ApiService,
              private emitsPopupService: EmitsPopupService,
              private logger: LoggerService) {
    super(apiService);
  }

  importEmitAndGen(): Observable<any> {
    const url: string = '/api/importPGMarqueur.php';
    const createFn: any = (data: any): GeoJsonEmitAndGen => this.allEmits = new GeoJsonEmitAndGen(data);
    return this.importData({}, url, createFn);
  }

  updateEmit(key: string, value: any): GeoJsonEmitAndGenFeature {
    const modifiedFeature: GeoJsonEmitAndGenFeature = this.emits.features.find(
      (feature: GeoJsonEmitAndGenFeature) =>
        feature.properties.id === this.feature.properties.id);
    modifiedFeature.properties[key] = value;
    this.feature = modifiedFeature;
    return modifiedFeature;
  }

  async updateMarkerInfos(clef: string, valeur: string): Promise<void> {
    const id: number = this.feature.properties.id;
    const body: object = {
      cle: clef,
      valeur,
      id,
    };
    try {
      await this.changeMarkerInfos(body).toPromise();
    } catch (e) {
      this.logger.error(e);
    }
  }

  public updateFeature(updatedFeature: GeoJsonEmitAndGenFeature): void {
    this.feature = updatedFeature;
  }

  changeMarkerInfos(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/changeMarqueurInfo.php', body, 'text');
  }

  private filterEmits(): void {
    this.emits = this.filter === null ? this.allEmits : ({
      ...this.allEmits,
      features: this.allEmits.features.filter(
        (emit: GeoJsonEmitAndGenFeature) => emit.properties.type === this.filter
          || emit.properties.projet === this.filter),
    });
  }
}
