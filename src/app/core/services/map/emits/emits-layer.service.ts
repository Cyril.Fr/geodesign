import {Injectable} from '@angular/core';
import {ApiService} from '../../api.service';
import * as L from 'leaflet';
import {LatLng, Layer, Marker} from 'leaflet';
import {SERVICE_ICON_TO_NUMBER} from '../../../constants/panel-data.const';
import {GeoJsonEmitAndGenFeature} from '../../../models/map/emit-gen.model';
import {LayerService} from '../layer.service';
import {EmitsDataService} from './emits-data.service';
import {EmitsPopupService} from './emits-popup.service';

@Injectable({
  providedIn: 'root',
})
export class EmitsLayerService extends LayerService {

  sliderValue: number;
  emitsLayer: Layer;
  mapZoom: number;

  constructor(private apiService: ApiService,
              private emitsDataService: EmitsDataService,
              private emitsPopupService: EmitsPopupService) {
    super();
  }

  createEmit(feature: any, latlng: LatLng): Layer {
    return L.marker(latlng);
  }

  setIcon(feature: any, layer: Marker, sliderValue: number): void {
    const icon: L.Icon = this.getEmitIcon(feature, sliderValue);
    layer.setIcon(icon);
    this.emitsPopupService.createEventListener(layer);
    }

  getEmitIcon(feature: GeoJsonEmitAndGenFeature, sliderValue: number): L.Icon {
    let iconNb: number = SERVICE_ICON_TO_NUMBER[feature.properties.type];
    if (feature.properties.projet && feature.properties.projet !== 'f' && iconNb !== 19) {
      iconNb--;
    }
    const iconStr: string = iconNb < 10 ? `0${iconNb.toString()}` : iconNb.toString();
    let pixel: number;
    const taille: number = feature.properties.taille;
    if (this.mapZoom > 14) {
      pixel = 10 + taille * 10; // [20,30,40]
    } else if (this.mapZoom > 12) {
      pixel = Math.floor(7.5 + taille * 7.5); // [15,22,30]
    } else {
      pixel = 5 + taille * 5; // [10,15,20]
    }
    pixel *= 0.5 + (sliderValue / 4);
    return L.icon({
      iconUrl: '/assets/images/iconMarqueur/icon-' + iconStr + '.png',
      iconSize: [pixel, pixel],
      iconAnchor: [pixel / 2, pixel / 2],
    });
  }

  getLayer(forceRecreate: boolean = false): Layer {
    if (!this.emitsLayer || forceRecreate) {
      this.initLayer();
    }
    return this.emitsLayer;
  }

  private initLayer(): void {
    this.emitsLayer = this.createLeafletData(this.emitsDataService.emits,
      this.createEmit,
      (feature: any, layer: Marker) => this.setIcon(feature, layer, this.sliderValue));
  }
}
