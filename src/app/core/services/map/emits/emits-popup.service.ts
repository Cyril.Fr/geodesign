import {Injectable} from '@angular/core';
import {Layer, LeafletMouseEvent} from 'leaflet';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EmitsPopupService {

  eventListenerSet: boolean = false;
  private _displayPopup: boolean;
  currentPopupId: number;
  leafletMouseEvent: LeafletMouseEvent;
  public displayPopup$: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([this._displayPopup, 0, 0, {}]);

  constructor() {
  }

  closePopup(): void {
    this._displayPopup = false;
    this.displayPopup$.next([this._displayPopup, null, null, null]);
  }

  showPopup(e: LeafletMouseEvent): void {
    const markerId: number = e.target.feature.properties.id;
    this._displayPopup = (markerId !== this.currentPopupId) ? true : !this._displayPopup;
    this.currentPopupId = markerId;
    this.leafletMouseEvent = e;
    this.displayPopup$.next([this._displayPopup, e.containerPoint.x, e.containerPoint.y, e.target.feature]);
  }

  createEventListener(layer: Layer): void {
    if (!this.eventListenerSet) {
      layer.on({
        click: (e: LeafletMouseEvent): void => this.showPopup(e),
      });
    }
  }
}
