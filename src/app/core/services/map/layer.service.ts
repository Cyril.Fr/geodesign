import {Injectable} from '@angular/core';
import * as L from 'leaflet';
import {GeoJSON, LatLng, Layer} from 'leaflet';
import {GeoJson} from '../../models/map/map.model';

@Injectable({
  providedIn: 'root',
})
export class LayerService {

  constructor() {
  }

  createLeafletData(data: GeoJson,
                    layerFn: (feature: any, latlng: LatLng) => Layer,
                    eventFn: (feature: any, layer: Layer) => void,
                    styleFn?: (feature: any) => any): GeoJSON {
    return L.geoJSON(data, {
      pointToLayer: layerFn,
      onEachFeature: eventFn,
      style: styleFn,
    });
  }

  styleTooltip(permanent: boolean = false, direction: string = 'center'): object {
    return {
      permanent,
      direction,
      className: 'mv-tooltip',
    };
  }

  // Use undefined to trigger default value
  styleLayer(color: string = '#3388ff',
             fillColor: string,
             opacity: number = 1.0,
             fillOpacity: number = 0.2,
             radius: number = 10,
             weight: number = 3): object {
    return {
      color,
      fillColor,
      opacity,
      fillOpacity,
      radius,
      weight,
    };
  }
}
