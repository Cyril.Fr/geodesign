import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CourseService {

  constructor(private apiService: ApiService) {
  }

  updateCourse(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/changeCourse.php', body);
  }
}
