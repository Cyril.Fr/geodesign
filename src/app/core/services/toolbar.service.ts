import {Injectable} from '@angular/core';
import { MapService } from './map/map.service';

@Injectable({
  providedIn: 'root',
})
export class ToolbarService {

  constructor(private mapService: MapService) {
  }

  toggleFullScreen(): void {
    this.mapService.toggleFullScreen();
  }

  setNav(): void {
    alert('Mode navigation');
  }

  undo(): void {
    alert('Undo');
  }

  toggleDarkMode(): void {
    this.mapService.toggleDarkMode();
  }

  displaySearchStop(): void {
    alert('Display Search');
  }
}
