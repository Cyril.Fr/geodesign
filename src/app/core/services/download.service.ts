import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DownloadService {

  constructor(private apiService: ApiService) {
  }

  downloadFile(filename: string): Observable<any> {
    return this.apiService.prepareRequest('GET', `/exportfile/${filename}`, null, 'arraybuffer');
  }

  downloadLink(blob: Blob, name: string): void {
    const dataUrl: string = window.URL.createObjectURL(blob);
    const link: HTMLAnchorElement = document.createElement('a');
    link.href = dataUrl;
    link.download = name;
    link.click();
    setTimeout(() => {
      window.URL.revokeObjectURL(dataUrl);
    }, 100);
  }
}
