import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SpeedService {

  constructor(private apiService: ApiService) {
  }

  updateSpeed(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/changeSpeed.php', body);
  }
}
