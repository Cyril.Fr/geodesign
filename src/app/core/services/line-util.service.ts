import {Injectable} from '@angular/core';
import {Line} from '../models/line.model';
import {OperatingInfo} from '../models/operatingInfo.model';
import {ParametersService} from './parameters.service';
import {PartielForLine} from '../models/partiel-for-line.model';
import {NbJours} from '../models/nb-jours.model';
import {ParamMap} from '@angular/router';
import {LineService} from './line.service';

// TODO Voir parce que Math.ceil sur ligne partiel + ligne cause un souci.
@Injectable({
  providedIn: 'root',
})
export class LineUtilService {

  constructor() {
  }

  /**
   * Return true if the line is a round trip
   * @param line Line
   */
  static isRoundTripLine(line: Line): boolean {
    return line.aller_retour === 't';
  }

  /**
   * Convert kilometer / hours in meter / minutes
   * @param speed number
   */
  static convertKmHInMMin(speed: number): number {
    return speed * 1000 / 60;
  }

  /**
   * Return the length of the line
   * @param line Line
   */
  static getLengthLine(line: Line | PartielForLine): number {
    return (line instanceof Line) && LineUtilService.isRoundTripLine(line)
      ? line.longueur + line.longueur2
      : line.longueur * 2;
  }

  /**
   * Return the time of the rotation for a line
   * @param line Line
   */
  static getRotationTimeLine(line: Line): number {
    return LineUtilService.getLengthLine(line) / LineUtilService.convertKmHInMMin(line.speed);
  }

  /**
   * Return false if the mode is hours, return course instead
   * @param line Line
   */
  static checkIfLineHoursOrCourse(line: Line): boolean | number[] {
    return line.modecourse && line.course;
  }

  /**
   * Return the number of course for a time slot
   * @param operatingInfo OperatingInfo
   */
  static calcNbCourse(operatingInfo: OperatingInfo): number {
    return Math.ceil((operatingInfo.fin - operatingInfo.debut) / (operatingInfo.frequence / 60));
  }

  /**
   * Return the number of bus per hour
   * @param line Line
   * @param operatingInfo OperatingInfo
   */
  static getNbBusPerHour(line: Line | PartielForLine, operatingInfo: OperatingInfo): number[] {
    const nbBusPerHour: number[] = [];
    for (let i: number = 0; i < 25; i++) {
      if (i >= Math.round(operatingInfo.debut) && i < Math.round(operatingInfo.fin)) {
        nbBusPerHour.push(Math.round(LineUtilService.getNbBusFromTimeSlot(line, operatingInfo.frequence) * 100) / 100);
      } else {
        nbBusPerHour.push(0);
      }
    }

    return nbBusPerHour;
  }

  /**
   * Return the total course for a period
   * @param line Line | PartielForLine
   * @param period string
   */
  static getNbCourseFromPeriod(line: Line | PartielForLine, period: string): number {
    const nbCourseFromPeriod: number = line.operating_info[period].reduce(
      (totalNbCourse: number, operatingInfo: OperatingInfo) => totalNbCourse + LineUtilService.calcNbCourse(operatingInfo), 0);

    return nbCourseFromPeriod > 0 ? nbCourseFromPeriod + 1 : 0;
  }

  /**
   * Return the bus number for a time slot
   * @param line Line
   * @param frequence number
   */
  static getNbBusFromTimeSlot(line: Line | PartielForLine, frequence: number): number {
    return LineUtilService.getCycleTimeLine(line) / frequence;
  }

  /**
   * Return the bus number needs from a period
   * @param line Line
   * @param period string
   */
  static getNbBusFromPeriod(line: Line | PartielForLine, period: string): number {
    return line.operating_info[period].reduce((currentValue: number, op: OperatingInfo) => {
      const nbBus: number = +(LineUtilService.getNbBusFromTimeSlot(line, op.frequence).toFixed(2));
      return currentValue +
        Math.ceil(nbBus) * (Math.floor(op.fin - op.debut));
    }, 0);
  }

  // Voir pour faire les mêmes calculs avec battement pour tad
  // Voir pour fusionner rotation time avec cycle time
  /**
   * Return the cycle time for a line
   * @param line Line
   */
  static getCycleTimeLine(line: Line | PartielForLine): number {
    const distanceLine: number = LineUtilService.getLengthLine(line);
    const speed: number = LineUtilService.convertKmHInMMin(line.speed);
    if (line instanceof Line) {
      return LineUtilService.isTadLine(line) ? distanceLine / speed : distanceLine > 0 ?
        (distanceLine / speed) * (ParametersService.BATTEMENT_PERCENT / 100)
        < ParametersService.BATTEMENT_T && line.id > 0
          ? distanceLine / speed + ParametersService.BATTEMENT_T
          : (distanceLine / speed) * (1 + (ParametersService.BATTEMENT_PERCENT / 100))
        : 0;
    } else {
      return distanceLine > 0
        ? (distanceLine / speed) * (1 + (ParametersService.BATTEMENT_PERCENT / 100))
        : 0;
    }
  }

  /**
   * Return commercial hours for the line
   * @param line Line | PartielForLine
   */
  static getCommercialHours(line: Line | PartielForLine): number {
    return Object.keys(line.operating_info).reduce((previousValue: number, currentValue: string, currentIndex: number) => {
      const nbJour: number = ParametersService.NB_JOUR_PERIODE[currentIndex].nb;
      const lineCourse: boolean | number[] = (line instanceof Line) ? LineUtilService.checkIfLineHoursOrCourse(line) : line.course;
      const nbCourse: number = lineCourse ? lineCourse[currentIndex] : LineUtilService.getNbCourseFromPeriod(line, currentValue);
      return previousValue + nbJour * this.getCycleTimeLine(line) * nbCourse;
    }, 0);
  }

  /**
   * TODO Passer le déclenchement!
   * Return commercial kilometers for the line
   * @param line Line | PartielForLine
   * @param declenchement number
   */
  static getCommercialKm(line: Line | PartielForLine, declenchement: number = 1): number {
    return Object.keys(line.operating_info).reduce((previousValue: number, currentValue: string, currentIndex: number) => {
      const nbJour: number = ParametersService.NB_JOUR_PERIODE[currentIndex].nb;
      const lineCourse: boolean | number[] = (line instanceof Line) ? LineUtilService.checkIfLineHoursOrCourse(line) : line.course;
      const nbCourse: number = lineCourse ? lineCourse[currentIndex] : LineUtilService.getNbCourseFromPeriod(line, currentValue);
      return previousValue + nbJour * this.getLengthLine(line) * nbCourse * declenchement;
    }, 0);
  }

  /**
   * Return the max number of bus needed for the line
   * @param line Line
   */
  static getTotalBusMax(line: Line | PartielForLine): number {
    let busMax: number = 0;
    Object.keys(line.operating_info).forEach((key: string) => {
      line.operating_info[key].forEach((operatingInfo: OperatingInfo) => {
        const nbBus: number = LineUtilService.getNbBusFromTimeSlot(line, operatingInfo.frequence);
        if (nbBus > busMax) {
          busMax = nbBus;
        }
      });
    });

    return busMax;
  }

  /**
   * Return the drive hours for the line
   * @param line Line
   */
  static getDriveHours(line: Line | PartielForLine): number {
    return Object.keys(line.operating_info).reduce((driveHours: number, period: string, currentIndex: number) => {
      const nbJour: number = ParametersService.NB_JOUR_PERIODE[currentIndex].nb;
      return driveHours + nbJour * Math.ceil(LineUtilService.getNbBusFromPeriod(line, period));
    }, 0);
  }

  /**
   * Return the drive cost for the line
   * @param line Line
   */
  static getDriveCost(line: Line | PartielForLine): number {
    return LineUtilService.getDriveHours(line) * ParametersService.TX_HORAIRE
      + LineUtilService.getCommercialKm(line) / 1000 * ParametersService.TX_KM;
  }

  static async asyncForEach(array, callback): Promise<void> {
    for (let index: number = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  /**
   * Return if the line is a tad
   * @param line Line
   */
  static isTadLine(line: Line): boolean {
    return !!line.declenchement;
  }
}
