import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class LanguagesService {

  constructor(private translateService: TranslateService) {
  }

  changeLanguage(lang: string): void {
    this.translateService.use(lang);
  }
}
