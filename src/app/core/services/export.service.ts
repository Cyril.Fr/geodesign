import {Injectable} from '@angular/core';
import {DownloadService} from './download.service';

@Injectable({
  providedIn: 'root',
})
export class ExportService {

  constructor(private downloadService: DownloadService) {
  }

  async exportAndDownload(filename: string, url: string, type: string): Promise<void> {
    const downloadFile: any = await this.downloadService.downloadFile(url).toPromise();
    const blob: Blob = new Blob([downloadFile], {
      type,
    });
    this.downloadService.downloadLink(blob, filename);
  }
}
