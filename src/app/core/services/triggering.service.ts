import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {Line} from '../models/line.model';
import {NetworkService} from './network.service';

@Injectable({
  providedIn: 'root',
})
export class TriggeringService {

  constructor(private apiService: ApiService) {
  }

  changeTriggering(line: Line, triggeringValue: number): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/updateDatabaseField.php', {
      table: 'service_tad',
      column: 'declenchement',
      whereValue: line.id,
      whereField: 'id',
      value: triggeringValue,
      schema: NetworkService.getNetworkName(),
    });
  }
}
