import {Injectable} from '@angular/core';
import * as log from 'loglevel';

@Injectable({
  providedIn: 'root',
})
export class LoggerService {
  constructor() {
  }

  // Reuse loglevel functions
  setLevel: any = log.setLevel;
  trace: any = log.trace;
  debug: any = log.debug;
  info: any = log.info;
  warn: any = log.warn;

  error(error: string | Error | ErrorEvent, opts?: {}): void {
    log.error(error, opts);
  }
}
