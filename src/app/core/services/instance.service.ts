import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InstanceService {

  // The current instance of the application. For example "Annemasse".
  static instance: string;

  constructor() {
  }

}
