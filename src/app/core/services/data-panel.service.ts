import {Injectable} from '@angular/core';
import {MapService} from './map/map.service';

@Injectable({
  providedIn: 'root',
})
export class DataPanelService {

  constructor(private mapService: MapService) {
  }

  showLayer(key: string, forceRecreate: boolean = false, additionalKey?: string): void {
    this.mapService.showLayer(key, forceRecreate, additionalKey);
  }

  hideLayer(key: string, additionalKey?: string): void {
    this.mapService.hideLayer(key, additionalKey);
  }
}
