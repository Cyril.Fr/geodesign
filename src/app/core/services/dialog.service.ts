import {Injectable, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {ComponentType} from '@angular/cdk/portal';

/**
 * Interface for dialog component
 */
export class IDialog {
  /**
   * Constructor to rewrite
   * @param data Data to add to the dialog
   * @param dialogRef Reference of the dialog
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, dialogRef: MatDialogRef<any>) {
  }
}

/**
 * Service to display or hide the dialog
 */
@Injectable({
  providedIn: 'root',
})
export class DialogService {

  private _dialogRef: MatDialogRef<any>;

  /**
   * Constructor for the dialog service
   * @param dialog MatDialog
   */
  constructor(private dialog: MatDialog) {
  }

  /**
   * Function to show the dialog box
   * @param component Component which extends the IDialog interface
   * @param config Data to send to the component
   */
  public show<T extends IDialog>(component: ComponentType<T>, config?: any): Observable<any> {
    this._dialogRef = this.dialog.open(component, this.buildConfig(config));
    return this._dialogRef.afterClosed();
  }

  /**
   * Function to close the dialog box
   */
  public hide(): void {
    this._dialogRef.close();
  }

  /**
   * Initialize a configuration to add data in the emit-popup
   * @param config Data to send to the component
   */
  private buildConfig(config): MatDialogConfig {
    if (config) {
      const matConfig: MatDialogConfig = new MatDialogConfig();
      if (config.data) {
        matConfig.data = config.data;
      }
      if (config.disableClose) {
        matConfig.disableClose = config.disableClose;
      }
      return matConfig;
    }
    return null;
  }
}
