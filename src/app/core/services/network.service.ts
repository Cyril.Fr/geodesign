import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Network, NetworkStats} from '../models/network.model';
import {Options} from '../models/options.model';
import {ApiService} from './api.service';
import {map} from 'rxjs/operators';
import {Thermometer} from '../models/thermometer.model';
import {InstanceService} from './instance.service';
import {Desserve} from '../models/desserve.model';

@Injectable({
  providedIn: 'root',
})
export class NetworkService {

  public static NETWORK_POP: number = 0;

  networks: Network[];
  // Get the current network from the localStorage.
  static getNetworkName(): string {
    return localStorage.getItem(`last_schema_${InstanceService.instance}`);
  }

  // Update the current network in the localStorage.
  static setNetworkName(networkName: string, instance: string = InstanceService.instance): void {
    localStorage.setItem(`last_schema_${instance}`, networkName);
  }

  constructor(private apiService: ApiService) {
  }

  createNetwork(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/createNewSchema.php', body, 'text');
  }

  deleteNetwork(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/deleteSchema.php', body, 'text');
  }

  duplicateNetwork(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/copySchema.php', body, 'text');
  }

  getNetworks(): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/getSchemaName.php', {}).pipe(
      map((networks: Network[]) => this.networks = networks)
    );
  }

  getNetworksInfos(): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/getReseaux.php', {}).pipe(
      map((networkArray: Network[]) => networkArray.map((network: Network) => new Network(network)))
    );
  }

  exportToGIS(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/exportToGIS.php', body, 'text');
  }

  exportToKML(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/exportToKML.php', body, 'text');
  }

  exportToChainage(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/exportToChainage.php', body, 'text');
  }

  exportToCsv(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/exportToCSV.php', body, 'text');
  }

  writeCalendarAgency(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/GTFS/writeCalendarAgency.php', body, 'text');
  }

  writeStops(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/GTFS/writeStops.php', body, 'text');
  }

  writeRoutes(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/GTFS/writeRoutes.php', body, 'text');
  }

  writeTrips(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/GTFS/writeTrips.php', body, 'text');
  }

  writeStopTimes(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/GTFS/writeStopTimes.php', body, 'text');
  }

  writeShapes(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/GTFS/writeShapes.php', body, 'text');
  }

  writeGTFS(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/GTFS/writeGTFS.php', body, 'text');
  }

  getTherm(body: any): Observable<Thermometer[]> {
    return this.apiService.prepareRequest('POST', '/api/getTherm.php', body);
  }

  mixNetworks(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/mixSchema.php', body, 'text');
  }

  modifyNetworks(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/modifySchema.php', body, 'text');
  }

  getDesserve(body: any): Observable<Desserve[]> {
    return this.apiService.prepareRequest('POST', '/api/getDesserve.php', body).pipe(
      map((desserves: Desserve[]) => desserves ? desserves.map((desserve: Desserve) => new Desserve(desserve)) : [])
    );
  }

  getNetworkPop(): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/getNetworkPop.php', {}).pipe(
      map((pop: any) => {
        NetworkService.NETWORK_POP = parseInt(pop, 10);
        return pop;
      })
    );
  }

  initializeNetwork(networks: Network[]): Options[] {
    const options: Options[] = [];
    let currentDir: string;
    networks.forEach((network: Network) => {
      if (['public', 'pg_catalog', 'information_schema', 'indic', null].indexOf(network.name) < 0) {
        if (currentDir !== network.dir) {
          options.push({
            directory: network.dir,
            suboptions: [],
          });
        }
        currentDir = network.dir;
        options.find((option: Options) => option.directory === currentDir).suboptions.push({
          label: network.name,
          value: network.name,
        });
      }
    });

    return options;
  }

  initializeCompareNetworks(networks: Network[]): object {
    const dirNetworks: object = {};
    for (const network of networks) {
      const currentDir: string = network.dir === '' ? 'defaut' : network.dir;
      if (!dirNetworks[currentDir]) {
        dirNetworks[currentDir] = [network];
      } else {
        dirNetworks[currentDir].push(network);
      }
    }
    return dirNetworks;
  }

  extractSelectedNetworks(networkCheckboxes: object, selectedNetworks: string[]): string[] {
    // Create array with name of each selected checkbox
    const newSelectedNetworks: string[] = Object.keys(networkCheckboxes)
      .filter((networkName: string) => networkCheckboxes[networkName]);
    if (selectedNetworks.length === 0) {
      // If no selected network return array with first element
      return newSelectedNetworks;
    } else {
      // longer array between previous selectedNetworks and newSelectedNetworks
      const longerArray: string[] = selectedNetworks.length > newSelectedNetworks.length ? selectedNetworks : newSelectedNetworks;
      // smaller array between previous selectedNetworks and newSelectedNetworks
      const smallerArray: string[] = selectedNetworks.length > newSelectedNetworks.length ? newSelectedNetworks : selectedNetworks;
      // network element that need to be add/remove
      const currentNetwork: string = longerArray.find((network: string) => !smallerArray.includes(network));
      // Create a copy of previous selected networks
      const selectedArray: string[] = [...selectedNetworks];
      // Add/Remove the wanted element
      (newSelectedNetworks === longerArray) ? selectedArray.push(currentNetwork)
        : selectedArray.splice(selectedNetworks.indexOf(currentNetwork), 1);
      return selectedArray;
    }
  }

  getUniquesDirectories(networks: Network[]): string[] {
    const options: Options[] = this.initializeNetwork(networks);
    return [...new Set(options.map((x: Options) => x.directory))].filter((value: string) => value !== '');
  }

  trimAndReplaceAndLowerCase(value: string): string {
    return value.trim().replace(/\s/g, '_').toLowerCase();
  }

  extractNetworkStats(selectedNetworks: object): NetworkStats[] {
    return Object.values(selectedNetworks).map((network: Network) => {
      const networkStats: NetworkStats = {
        name: network.name,
        coutTotal: network.cout,
        popTotal: network.pop,
        lineNbr: network.ligne,
        busNbr: network.parc,
        kmTotal: network.km,
      };
      return networkStats;
    });
  }

  extractComparedStats(networkStat: NetworkStats, refNetworkStat: NetworkStats, isRefNetwork: boolean): NetworkStats {
    const comparedStats: NetworkStats = {} as NetworkStats;
    if (!isRefNetwork) {
      // Compare with refNetworkStat to determine % of dif
      Object.entries(networkStat).forEach(([key, value]: [string, number]) => {
        if (typeof value === 'number') {
          // Calculate the percentage of dif between current & ref network
          const refDifference: number = (value - refNetworkStat[key]) / (refNetworkStat[key]);
          // Assign the percentage to the component attribute
          comparedStats[key] = refDifference;
        } else {
          // Copy the name if key = name else put null
          comparedStats[key] = key === 'name' ? networkStat[key] : null;
        }
      });
    } else {
      // Set compare to 0 dif
      Object.keys(networkStat).forEach((key: string) => comparedStats[key] = (key === 'name') ? networkStat[key] : 0);
    }
    return comparedStats;
  }

}
