import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ApiService} from './api.service';
import {Params} from '../models/params.model';
import {NbJours} from '../models/nb-jours.model';
import {map} from 'rxjs/operators';
import {ParamsCommun} from '../models/params-commun.model';

@Injectable({
  providedIn: 'root',
})
export class ParametersService {

  public static TX_HORAIRE: number = 0;
  public static TX_KM: number = 0;
  public static BATTEMENT_T: number = 0;
  public static BATTEMENT_PERCENT: number = 0;
  public static NB_JOUR_PERIODE: NbJours[] = [];
  public static PARAMS_UO: ParamsCommun;
  public static PARAMS_SD: ParamsCommun;

  public nbJours$: BehaviorSubject<NbJours[]> = new BehaviorSubject(ParametersService.NB_JOUR_PERIODE);
  public paramsUo$: BehaviorSubject<ParamsCommun> = new BehaviorSubject(ParametersService.PARAMS_UO);
  public paramsSd$: BehaviorSubject<ParamsCommun> = new BehaviorSubject(ParametersService.PARAMS_SD);
  public reloadLineInformation$: BehaviorSubject<void> = new BehaviorSubject(null);

  constructor(private apiService: ApiService) {
  }

  static saveBattementT(battementT: number): void {
    ParametersService.BATTEMENT_T = battementT;
  }

  static saveBattementPercent(battementPercent: number): void {
    ParametersService.BATTEMENT_PERCENT = battementPercent;
  }

  static saveTxKm(txKm: number): void {
    ParametersService.TX_KM = txKm;
  }

  static saveTxHoraire(txHoraire: number): void {
    ParametersService.TX_HORAIRE = txHoraire;
  }

  getParam(body: any): Observable<Params[]> {
    return this.apiService.prepareRequest('POST', '/api/getParam.php', body).pipe(
      map((params: any) => {
        ParametersService.TX_HORAIRE = parseFloat(params[0].value);
        ParametersService.TX_KM = parseFloat(params[1].value);
        ParametersService.BATTEMENT_T = parseFloat(params[2].value);
        ParametersService.BATTEMENT_PERCENT = parseFloat(params[3].value);
        return params;
      })
    );
  }

  getNbJour(body: any): Observable<NbJours[]> {
    return this.apiService.prepareRequest('POST', '/api/getNbJour.php', body).pipe(
      map((nbJours: any) => {
        ParametersService.NB_JOUR_PERIODE = [];
        nbJours.forEach((nbJour: any) => {
          ParametersService.NB_JOUR_PERIODE.push(new NbJours(nbJour));
        });
        return nbJours;
      })
    );
  }

  getParamCommun(body: any = {}): Observable<ParamsCommun[]> {
    return this.apiService.prepareRequest('POST', '/api/getParamCommun.php', body).pipe(
      map((paramsCommun: any) => {
        this.sendParamsUo(paramsCommun.find((paramCommun: ParamsCommun) => paramCommun.type === 'data_uo'));
        this.sendParamsSd(paramsCommun.find((paramCommun: ParamsCommun) => paramCommun.type === 'data_sd'));
        return paramsCommun;
      })
    );
  }

  changeParamCommun(body: any): Observable<void> {
    return this.apiService.prepareRequest('POST', '/api/changeParamCommun.php', body);
  }

  changeParam(body: any): Observable<void> {
    return this.apiService.prepareRequest('POST', '/api/changeParam.php', body);
  }

  sendNbJours(nbJours: NbJours[]): void {
    ParametersService.NB_JOUR_PERIODE = nbJours;
    this.nbJours$.next(nbJours);
  }

  sendParamsUo(paramsUo: ParamsCommun): void {
    ParametersService.PARAMS_UO = paramsUo;
    this.paramsUo$.next(paramsUo);
  }

  sendParamsSd(paramsSd: ParamsCommun): void {
    ParametersService.PARAMS_SD = paramsSd;
    this.paramsSd$.next(paramsSd);
  }

  reloadInformationLine(): void {
    this.reloadLineInformation$.next();
  }
}
