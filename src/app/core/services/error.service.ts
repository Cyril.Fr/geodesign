import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {NgControl} from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class ErrorService {

  constructor(private translateService: TranslateService) {
  }

  getErrorTxt(control: NgControl): string {
    if (control.hasError('required')) {
      return this.translate('REQUIRED');
    } else if (control.hasError('maxlength')) {
      return this.translate('MAXLENGTH', {maxNbr: control.getError('maxlength').requiredLength});
    } else if (control.hasError('pattern') && control.getError('pattern').requiredPattern === '^[0-9]*$') {
      return this.translate('INT_PATTERN');
    } else if (control.hasError('pattern') && control.getError('pattern').requiredPattern === '^[+-]?([0-9]*[.])?[0-9]+$') {
      return this.translate('FLOAT_PATTERN');
    } else if (control.hasError('nameExist')) {
      return this.translate('NAME_EXIST');
    } else if (control.hasError('abbrExist')) {
      return this.translate('ABBR_EXIST');
    } else {
      return '';
    }
  }

  translate(key: string, param?: any): string {
    return this.translateService.instant(`ERRORS.${key}`, param);
  }
}
