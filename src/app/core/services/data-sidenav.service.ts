import {Injectable} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataSidenavService {
  private sidenav: MatSidenav;
  private sidenavLine: MatSidenav;
  public sidenav$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {
  }

  public setSidenav(sidenav: MatSidenav): void {
    this.sidenav = sidenav;
  }

  public setSidenavLine(sidenavLine: MatSidenav): void {
    this.sidenavLine = sidenavLine;
  }

  public toggle(): void {
    this.sidenav$.next(true);
    this.sidenav.toggle();
    this.sidenav$.next(this.dataSidenavIsOpen());
  }

  public dataSidenavIsOpen(): boolean {
    return this.sidenav.opened;
  }

  public toggleSidenavLine(): void {
    this.sidenavLine.toggle();
  }

  isOpenSidenavLine(): boolean {
    return this.sidenavLine.opened;
  }
}
