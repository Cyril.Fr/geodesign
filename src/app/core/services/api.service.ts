import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {InstanceService} from './instance.service';

@Injectable({providedIn: 'root'})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  prepareRequest<T>(
    method: 'GET' | 'PATCH' | 'PUT' | 'POST' | 'DELETE',
    url: string,
    body?: T | FormData | null,
    responseType: string = 'json',
    useFormData: boolean = true,
    useInstance: boolean = true
  ): Observable<any> {

    const options: any = {responseType};

    if (useFormData && body) {
      const formData: FormData = new FormData();
      this.transformObjectToFormData(formData, body, '');

      if (useInstance && InstanceService.instance) {
        formData.append('instance', InstanceService.instance);
      } else if (useInstance && !InstanceService.instance) {
        return of(null);
      }

      body = formData;
    }

    switch (method) {
      case 'GET':
        return this.http.get<T>(url, options);
      case 'PATCH':
        return this.http.patch<T>(url, body, options);
      case 'PUT':
        return this.http.put<T>(url, body, options);
      case 'POST':
        return this.http.post<T>(url, body, options);
      case 'DELETE':
        if (body) {
          options.body = body;
        }
        return this.http.delete<T>(url, options);
      default:
        return of(null);
    }
  }

  transformObjectToFormData(formData, data, previousKey): any {
    if (data instanceof Object) {
      Object.keys(data).forEach((key: any) => {
        const value: any = data[key];
        if (value instanceof Object && !Array.isArray(value)) {
          return this.transformObjectToFormData(formData, value, key);
        }
        if (previousKey) {
          key = `${previousKey}[${key}]`;
        }
        if (Array.isArray(value)) {
          value.forEach((val: any) => {
            formData.append(`${key}[]`, val);
          });
        } else {
          formData.append(key, value);
        }
      });
    }
  }
}
