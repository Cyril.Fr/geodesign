import {Injectable} from '@angular/core';
import {NetworkService} from './network.service';
import {Network} from '../models/network.model';
import {InstanceService} from './instance.service';
import {GeoJson} from '../models/map/map.model';
import {BehaviorSubject, forkJoin, Observable} from 'rxjs';
import {DataService} from './data.service';
import {GeoJsonStop, GeoJsonStopFeature} from '../models/map/stops.model';
import {GeoJsonLines} from '../models/map/lines.model';
import {GeoJsonEmitAndGen} from '../models/map/emit-gen.model';
import {GeoJsonServiceMob} from '../models/map/service-mob.model';
import {GeoJsonMouvements} from '../models/map/mouvements.model';
import {GeoJsonIsochrones} from '../models/map/isochrones.model';
import {GeoJsonNbCourses} from '../models/map/nb-courses.model';
import {GeoJsonStopInfos} from '../models/map/popemp.model';
import {GeoJsonInsee} from '../models/map/insee.model';
import {NbcoursesDataService} from './map/nb-courses/nbcourses-data.service';
import {CoversDataService} from './map/covers/covers-data.service';
import {EmitsDataService} from './map/emits/emits-data.service';
import {InseeDataService} from './map/insee/insee-data.service';

@Injectable({
  providedIn: 'root',
})
export class ImportDataService {

  private _networks: Network[];
  networks$: BehaviorSubject<Network[]> = new BehaviorSubject<Network[]>(this._networks);

  get networks(): Network[] {
    return this._networks;
  }

  set networks(value: Network[]) {
    this._networks = value;
    this.networks$.next(this._networks);
  }

  private _desserveStopsGrouped: GeoJsonStop;
  desserveStopsGrouped$: BehaviorSubject<GeoJsonStop> = new BehaviorSubject<GeoJsonStop>(this._desserveStopsGrouped);

  set desserveStopsGrouped(value: GeoJsonStop) {
    this._desserveStopsGrouped = value;
    this.desserveStopsGrouped$.next(this._desserveStopsGrouped);
  }

  get desserveStopsGrouped(): GeoJsonStop {
    return this._desserveStopsGrouped;
  }

  constructor(private networkService: NetworkService,
              private dataService: DataService,
              private nbCoursesDataService: NbcoursesDataService,
              private coversDataService: CoversDataService,
              private emitsDataService: EmitsDataService,
              private inseeDataService: InseeDataService) {
  }

  // CALL THIS METHOD FROM APP COMPONENT INIT
  async importData(): Promise<void> {
    this.initNetworkName();
    this.networks = await this.networkService.getNetworks().toPromise();
    const currentEnquete: string = await this.dataService.importActualEnquete().toPromise();
    const linesCall: Observable<GeoJsonLines> = this.dataService.importLines();
    const stopsCall: Observable<GeoJsonStop> = this.dataService.importStops();
    const emitAndGenCall: Observable<GeoJsonEmitAndGen> = this.emitsDataService.importEmitAndGen();
    const coversCall: Observable<GeoJson> = this.coversDataService.importCovers();
    const serviceMobsCall: Observable<GeoJsonServiceMob> = this.dataService.importServiceMobs();
    const mouvementsCall: Observable<GeoJsonMouvements> = this.dataService.importMouvements(currentEnquete);
    const stopsInfosCall: Observable<GeoJsonStopInfos> = this.dataService.importPopEmp();
    const nbCoursesCall: Observable<GeoJsonNbCourses> = this.nbCoursesDataService.importNbCourses();
    const inseeCall: Observable<GeoJsonInsee> = this.inseeDataService.importInsee();
    const callResults: GeoJson[] = await forkJoin(
      [stopsCall, linesCall, emitAndGenCall, coversCall, serviceMobsCall, mouvementsCall, stopsInfosCall, nbCoursesCall, inseeCall]
    ).toPromise();
    this.desserveStopsGrouped = await this.dataService.importAllDesserveStops({
      schema: NetworkService.getNetworkName(),
    }).toPromise();
  }

  initNetworkName(): void {
    if (!NetworkService.getNetworkName()) {
      NetworkService.setNetworkName('', InstanceService.instance);
    }
  }

  importIsochrones(stopFeature: GeoJsonStopFeature): Observable<GeoJsonIsochrones> {
    const point: number[][] = stopFeature.geometry.coordinates;
    // DEFAULT VALUE TO CHANGE TODO
    const body: object = {
      duration: [30, 60, 90],
      periode: 'PSCO',
      heure: 8,
      schema: NetworkService.getNetworkName(),
      point: `POINT(${point[0]} ${point[1]})`,
    };
    return this.dataService.importIsochrones(body);
  }
}
