import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PdfService {

  constructor() {
  }

  getTextWidth(txt, fontSize): number {
    const c: HTMLCanvasElement = document.createElement('canvas');
    const ctx: CanvasRenderingContext2D = c.getContext('2d');
    ctx.font = 'bold ' + fontSize + 'px Helvetica';
    return ctx.measureText(txt).width / 2.9;
  }

  hexToRGBArray(hex: string): any[] {
    const result: any[] = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16),
      ] :
      null;
  }

  createPDFLine(nb: any, abbr: string, color: string[], arret: any[], nom: string, corres: any, doc: any): any {
    if (nb % 3 === 0 && nb > 0) {
      doc.addPage();
    }
    nb = parseInt(nb, 10) - (Math.floor((parseInt(nb, 10)) / 3)) * 3;
    const nbArret: number = arret.length;
    const pond: number = nbArret > 50 ? 0.6 : nbArret > 40 ? 0.75 - (nbArret - 40) / 66 : nbArret > 30 ? 1 - (nbArret - 30) / 40 : 1;
    const lineEnd: number = nbArret > 30 ? 262 : 5 + nbArret * 8.5;

    // Line name
    doc.setFillColor(color[0], color[1], color[2]);
    const widthName: number = this.getTextWidth(nom, 15.5) / 2;
    doc.roundedRect(100 - widthName, 10 + nb * 60, 10, 10, 2, 2, 'F');
    doc.setFontSize(15.5);
    doc.text(112 - widthName, 17.5 + nb * 60, nom);
    doc.setTextColor(255);
    doc.setFontType('bold');
    doc.text(105 - widthName, 17.5 + nb * 60, abbr, null, null, 'center');
    doc.setFontType('normal');

    // Line
    doc.setLineWidth(1.5 * pond);
    doc.setDrawColor(color[0], color[1], color[2]);
    doc.line(5, 50 + nb * 60, lineEnd, 50 + nb * 60);

    // Term Points
    doc.setFillColor(color[0], color[1], color[2]);
    doc.circle(5, 50 + nb * 60, 2.5 * pond, 'F');
    doc.circle(lineEnd, 50 + nb * 60, 2.5 * pond, 'F');
    // Term Rect + texts
    doc.setLineCap('butt');
    doc.setLineWidth(5 * pond);
    doc.setDrawColor(60, 80, 100);
    doc.setFontType('bold');
    doc.setFontSize(12 * pond);
    doc.setTextColor(255);
    if (this.getTextWidth(arret[arret.length - 1][0], 12 * pond) > 30) {
      const nbMot: number = arret[arret.length - 1][0].split(' ').length;
      const txt1: string = arret[arret.length - 1][0].split(' ').slice(0, Math.ceil(nbMot / 2)).join(' ');
      const txt2: string = arret[arret.length - 1][0].split(' ').slice(Math.ceil(nbMot / 2)).join(' ');
      const newTextWidth: number =
        this.getTextWidth(txt1, 12 * pond) > this.getTextWidth(txt2, 12 * pond) ?
          this.getTextWidth(txt1, 12 * pond)
          : this.getTextWidth(txt2, 12 * pond);
      doc.setLineWidth(11 * pond);
      doc.line(lineEnd + 5 - 2 * pond,
        52 - 5 * pond + nb * 60,
        lineEnd + 4.5 + 2 * pond + Math.cos(Math.PI / 6) * newTextWidth,
        52.5 - 7 * pond + nb * 60 - Math.cos(Math.PI / 3) * newTextWidth);
      doc.setLineWidth(5 * pond);
      doc.text(lineEnd + 5 - pond, 52 - (6 * pond) + nb * 60, txt1, null, 30);
      doc.text(lineEnd + 5 + pond, 52 - (2 * pond) + nb * 60, txt2, null, 30);
      // doc.text(179.5, 50.5 - (4 * pond) + nb * 60, txt1, null, 30);
      // doc.text(181, 53 - (4 * pond) + nb * 60, txt2, null, 30);
    } else {
      doc.line(lineEnd + 3 - 2 * pond, 50 - 5 * pond + nb * 60,
        lineEnd + 2.5 + 2 * pond + Math.cos(Math.PI / 6) * this.getTextWidth(arret[arret.length - 1][0], 12 * pond),
        50.5 - 7 * pond + nb * 60 - Math.cos(Math.PI / 3) * this.getTextWidth(arret[arret.length - 1][0], 12 * pond));
      doc.text(lineEnd + 3, 50 - (4 * pond) + nb * 60, arret[arret.length - 1][0], null, 30);
    }
    doc.line(5 - 2 * pond, 50 - 5 * pond + nb * 60,
      5 + 2 * pond + Math.cos(Math.PI / 6) * this.getTextWidth(arret[0][0], 12 * pond),
      50 - 7 * pond + nb * 60 - Math.cos(Math.PI / 3) * this.getTextWidth(arret[0][0], 12 * pond));
    doc.text(5, 50 - (4 * pond) + nb * 60, arret[0][0], null, 30);

    doc.setTextColor(20);

    // Stops
    const dInterStop: number = (lineEnd - 5) / (nbArret - 1);
    doc.setFillColor(color[0], color[1], color[2]);
    for (let i: number = 1; i < nbArret - 1; i++) {
      doc.circle(5 + i * dInterStop, 50 + nb * 60, 2 * pond, 'F');
    }

    doc.setFontType('normal');
    doc.setFontSize(11 * pond);
    for (let i: number = 1; i < nbArret - 1; i++) {
      if (this.getTextWidth(arret[i][0], 11 * pond) > 49) {
        doc.setFontSize(11 * pond * (49 / this.getTextWidth(arret[i][0], 11 * pond)));
      }
      doc.text(5 + i * (dInterStop), 50 - (3 * pond) + nb * 60, arret[i][0], null, 30);
      doc.setFontSize(11 * pond);
      // doc.text(5-pond + i * (dInterStop), 50+pond + nb * 60, '>');
      doc.setFillColor(255, 255, 255);
      if (arret[i][1] === 1) {
        doc.triangle(5.1 - pond + i * dInterStop,
          50 - pond + nb * 60,
          5.1 - pond + i * dInterStop,
          50 + pond + nb * 60,
          5.1 + pond + i * dInterStop,
          50 + nb * 60,
          'F');
      } else if (arret[i][1] === 2) {
        doc.triangle(4.9 + pond + i * dInterStop,
          50 - pond + nb * 60,
          4.9 + pond + i * dInterStop,
          50 + pond + nb * 60,
          4.9 - pond + i * dInterStop,
          50 + nb * 60, 'F');
      }
    }
  }
}
