import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {StopInfos} from '../models/stops.model';
import {LineUtilService} from './line-util.service';
import {Line} from '../models/line.model';
import {ParametersService} from './parameters.service';
import {StopStats} from '../models/stop-stats.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class StopService {

  constructor(private apiService: ApiService) {
  }

  getStopProxForLine(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/getStopProxForLine.php', body);
  }

  getInfoStopsStats(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/getInfoStopsStats.php', body).pipe(
      map((value: any[]) => {
        return {
          stopStats: value[0].map((stopStat: StopStats) => new StopStats(stopStat)),
          lineStats: value[1].map((line: Line) => new Line(line)),
        };
      })
    );
  }

  setDesserve(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/setDesserve.php', body);
  }

  getStopInfos(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/getInfoStops.php', body, 'text').pipe(
      map((geoJsonStopInfoString: string) => JSON.parse(geoJsonStopInfoString)),
      map((geoJsonStopInfos: any[]) => {
        const stopInfo: StopInfos = new StopInfos(geoJsonStopInfos[0]);
        let nbCourses: number;
        if (geoJsonStopInfos.length > 1) {
          const opeInfosLinesArray: any[] = geoJsonStopInfos.splice(1, geoJsonStopInfos.length - 1).map(
            (opInfos: any) => JSON.parse(opInfos.operating_info)
          );
          nbCourses = opeInfosLinesArray.reduce(
            (accNbCourse: number, opeLine: any) => accNbCourse + LineUtilService.getNbCourseFromPeriod({operating_info: opeLine} as Line,
              ParametersService.NB_JOUR_PERIODE[0].periode), 0);
        }
        return {
          population: stopInfo.sum,
          job: stopInfo.emp,
          nbCourses,
        };
      })
    );
  }

  setStopInfo(body: any): Observable<any> {
    return this.apiService.prepareRequest('POST', '/api/saveStopSetting.php', body, 'text');
  }
}
