export const SERVICE_ICON_TO_NUMBER: object = {
  Santés: 10,
  Scolaires: 12,
  'Habitats dense': 4,
  'Habitats pavillonaire': 6,
  Transports: 14,
  Commerces: 2,
  ZI: 18,
  'ZA tertiaire': 16,
  Loisirs: 8,
  'Non défini': 19,
};

export const GEO_JSON_ISOCHRONES_IDX: number[] = [1, 2, 3];
