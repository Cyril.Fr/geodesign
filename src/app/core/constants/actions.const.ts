import {CreationNetworkDialogComponent} from '../header/dialogs/creation-network-dialog/creation-network-dialog.component';
import {MixNetworkDialogComponent} from '../header/dialogs/mix-network-dialog/mix-network-dialog.component';
import {DuplicateNetworkDialogComponent} from '../header/dialogs/duplicate-network-dialog/duplicate-network-dialog.component';
import {DeleteNetworkDialogComponent} from '../header/dialogs/delete-network-dialog/delete-network-dialog.component';
import {ChangeNetworkDialogComponent} from '../header/dialogs/change-network-dialog/change-network-dialog.component';
import {CompareNetworkDialogComponent} from '../header/dialogs/compare-network-dialog/compare-network-dialog.component';
import {ExportNetworkDialogComponent} from '../header/dialogs/export-network-dialog/export-network-dialog.component';
import {WorkUnitsDialogComponent} from '../header/dialogs/work-units-dialog/work-units-dialog.component';
import {SocioDemoDialogComponent} from '../header/dialogs/socio-demo-dialog/socio-demo-dialog.component';
import {UnservicedStopsDialogComponent} from '../header/dialogs/unserviced-stops-dialog/unserviced-stops-dialog.component';
import {HelpDialogComponent} from '../header/dialogs/help-dialog/help-dialog.component';
import {StatsDialogComponent} from '../header/dialogs/stats-dialog/stats-dialog.component';

export const SCENARIO_ACTIONS: { label: string; picto: string, component: any}[] = [
  {
    label: 'ACTIONS.CREATE',
    picto: 'icon-partao_creer2',
    component: CreationNetworkDialogComponent,
  }, {
    label: 'ACTIONS.DUPLICATE',
    picto: 'icon-partao_dupliquer',
    component: DuplicateNetworkDialogComponent,
  }, {
    label: 'ACTIONS.MIX',
    picto: 'icon-partao_mixer',
    component: MixNetworkDialogComponent,
  }, {
    label: 'ACTIONS.DELETE',
    picto: 'icon-partao_supprimer',
    component: DeleteNetworkDialogComponent,
  }, {
    label: 'ACTIONS.CHANGE',
    picto: 'icon-partao_changer',
    component: ChangeNetworkDialogComponent,
  }, {
    label: 'ACTIONS.COMPARE',
    picto: 'icon-partao_comparer',
    component: CompareNetworkDialogComponent,
  }, {
    label: 'ACTIONS.EXPORT',
    picto: 'icon-partao_exporter',
    component: ExportNetworkDialogComponent,
  }, {
    label: 'ACTIONS.STATS',
    picto: 'icon-partao_stats',
    component: StatsDialogComponent,
  },
];

export const SETTINGS_ACTIONS: { label: string; component: any }[] = [
  {
    label: 'ACTIONS.UNIT_WORK',
    component: WorkUnitsDialogComponent,
  }, {
    label: 'ACTIONS.DEMO',
    component: SocioDemoDialogComponent,
  }, {
    label: 'ACTIONS.STOP',
    component: UnservicedStopsDialogComponent,
  },
];

export const HELP_ACTIONS: { component: any } = {component: HelpDialogComponent};
