export const LINE_TYPE: any[] = [
  {
    label: 'LINE_TYPE.TRAIN',
    value: 'train',
  },
  {
    label: 'LINE_TYPE.METRO_TRAM',
    value: 'metro_tram',
  },
  {
    label: 'LINE_TYPE.ESSENTIELLE',
    value: 'essentielle',
  },
  {
    label: 'LINE_TYPE.PRINCIPALE',
    value: 'principale',
  },
  {
    label: 'LINE_TYPE.LOCALE',
    value: 'locale',
  },
  {
    label: 'LINE_TYPE.HORS_PERIMETRE',
    value: 'hors_peri',
  },
];

export const OFFER_MODE: any[] = [
  {
    label: 'LINE_DIALOG.TH_INTERVAL',
    value: false,
  },
  {
    label: 'LINE_DIALOG.NB_TRIP',
    value: true,
  },
];

export const TAD_TYPE: any[] = [
  {
    label: 'LINE_TYPE.VIRTUAL_LINE',
    value: 'ligne_virtuelle',
  },
];

export const DRAW_MODE: any[] = [
  {
    label: 'LINE_DIALOG.DRAW_ASSIST',
    value: 'route',
  },
  {
    label: 'LINE_DIALOG.DRAW_FREE',
    value: 'free',
  },
];
