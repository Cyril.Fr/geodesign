export const STOPS_POPUP_SIZE: {X: number, Y: number, Y_EXTENDED: number} = {
  X: 350,
  Y: 220,
  Y_EXTENDED: 500,
};

export const DATA_SIDENAV_WIDTH: number = 450;
