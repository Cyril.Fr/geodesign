export enum PERIODS {
  PSCO = 0,
  PVS = 1,
  ETE = 2,
  DJF = 3,
}
