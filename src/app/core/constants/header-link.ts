export const HEADER_LINK: any[] = [{
  key: 'HEADER.HOME',
  link: 'https://marketer.transdev.com/accueil.aspx',
}, {
  key: 'HEADER.MY_DATA',
  link: 'https://marketer.transdev.com/Integration/PlansActions.aspx',
}, {
  key: 'HEADER.DATABASE',
  link: 'https://marketer.transdev.com/Integration/BddMutualisee.aspx',
}, {
  key: 'HEADER.TREND',
  link: 'https://marketer.transdev.com/Integration/Tendances.aspx',
}, {
  key: 'HEADER.DATA_TECH',
  link: 'https://marketer.transdev.com/Integration/DataTech.aspx',
}, {
  key: 'HEADER.CARTA_TECH',
  // TODO Replace with a good link
  link: 'https://marketer.transdev.com/Integration/BddMutualisee.aspx',
}, {
  key: 'HEADER.LIBRARY',
  link: 'https://marketer.transdev.com/Integration/Biblio.aspx',
}];
