import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable, Observer} from 'rxjs';
import {Injectable} from '@angular/core';
import {NetworkService} from '../services/network.service';
import {Network} from '../models/network.model';
import {InstanceService} from '../services/instance.service';

@Injectable()
export class NetworkResolver implements Resolve<Observable<any>> {
  constructor(
    private networkService: NetworkService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    InstanceService.instance = route.params && route.params.instance;
    return new Observable<any>((observer: Observer<any>): any => {
      if (!NetworkService.getNetworkName()) {
        // Initialize the network name in the localStorage if it doesn't exists.
        NetworkService.setNetworkName('', InstanceService.instance);
      }
      this.networkService.getNetworks().subscribe(
        (networksResult: Network[]) => {
          observer.next({networks: networksResult});
        },
        (error: Error) => observer.error(error),
        () => observer.complete());
    });
  }
}
