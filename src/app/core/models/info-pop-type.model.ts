export class InfoPopType {
  linetype: string;
  pop: number;
  popsco: number;

  constructor(infoPopType: any) {
    this.linetype = infoPopType.linetype;
    this.pop = parseInt(infoPopType.pop, 10);
    this.popsco = parseInt(infoPopType.popsco, 10);
  }
}
