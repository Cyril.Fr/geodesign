export class NbJours {
  periode: string;
  nb: number;
  id?: number;

  constructor(nbJours: any) {
    this.periode = nbJours.periode;
    this.nb = parseFloat(nbJours.nb);
    this.id = nbJours.id ? parseInt(nbJours.id, 10) : null;
  }
}
