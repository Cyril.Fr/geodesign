export class Network {
  name: string;
  dir: string;
  cout?: number;
  parc?: number;
  km?: number;
  // tslint:disable-next-line:variable-name
  heure_volant?: number;
  // tslint:disable-next-line:variable-name
  heure_commerciale?: number;
  pop?: number;
  ligne?: number;
  creation: string;
  createur: string;
  modification: string;
  modificateur: string;

  constructor(network: any) {
    this.name = network.nom;
    this.dir = network.dir;
    this.creation = network.creation;
    this.createur = network.createur;
    this.modification = network.modification;
    this.modificateur = network.modificateur;
    this.cout = parseInt(network.cout, 10) || null;
    this.parc = parseInt(network.parc, 10) || null;
    this.km = parseInt(network.km, 10) || null;
    this.heure_volant = parseInt(network.heure_volant, 10) || null;
    this.heure_commerciale = parseInt(network.heure_commerciale, 10) || null;
    this.pop = parseInt(network.pop, 10) || null;
    this.ligne = parseInt(network.ligne, 10) || null;
  }
}

export interface NetworkStats {
  name: string;
  coutTotal: number;
  lineNbr: number;
  kmTotal: number;
  busNbr: number;
  popTotal: number;
}
