export class StopStats {
  name: string;
  id: number;
  nbCourse: number;
  sum: number;

  constructor(stopStats: any) {
    this.name = stopStats.name;
    this.sum = parseInt(stopStats.sum, 10);
    this.id = parseInt(stopStats.id, 10);
  }
}
