export interface Thermometer {
  id: string;
  abbr: string;
  nom: string;
  couleur: string;
  arrets: string;
}
