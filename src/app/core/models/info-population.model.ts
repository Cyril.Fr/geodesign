export class InfoPopulation {
  id: number;
  pop: number;
  'pop_sco': number;
  pop65: number;
  emp: number;

  constructor(infoPop: any) {
    this.id = infoPop.id ? parseInt(infoPop.id, 10) : 0;
    this.pop = parseInt(infoPop.pop, 10);
    this.pop_sco = parseInt(infoPop.pop_sco, 10);
    this.pop65 = parseInt(infoPop.pop65, 10);
    this.emp = parseInt(infoPop.emp, 10);
  }
}
