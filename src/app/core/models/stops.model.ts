export class StopInfos {
  sum: number;
  emp: number;

  constructor(stopInfos: any) {
    this.sum = parseInt(stopInfos.sum, 10);
    this.emp = parseInt(stopInfos.emp, 10);
  }
}

export class OperatingInfos {
  debut: number;
  fin: number;
  frequence: number;
}

export interface StopPopupInfo {
  population: number;
  job: number;
  nbCourses: number;
}
