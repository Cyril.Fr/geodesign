export class Desserve {
  'id_line': number;
  abbr: string;
  nom: string;
  stops: number[];
  names: string[];

  constructor(desserve: any) {
    this.id_line = parseInt(desserve.id_line, 10);
    this.abbr = desserve.abbr;
    this.nom = desserve.abbr;
    this.stops = JSON.parse(desserve.stops);
    this.names = JSON.parse(desserve.names);
  }
}
