export interface Params {
  id: string;
  param: string;
  value: string;
}
