export class PartielForLine {
  speed: number;
  'operating_info': any;
  longueur: number;
  course: any;

  constructor(partielForLine: any) {
    this.speed = parseFloat(partielForLine.speed);
    this.operating_info = partielForLine.op;
    this.longueur = parseFloat(partielForLine.lon);
    this.course = JSON.parse(partielForLine.course);
  }

}
