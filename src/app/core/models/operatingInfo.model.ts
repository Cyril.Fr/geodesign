export interface OperatingInfo {
  debut: number;
  fin: number;
  frequence: number;
  veh?: number;
}
