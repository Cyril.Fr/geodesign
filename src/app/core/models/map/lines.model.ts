import {GeoJson, GeoJsonFeature} from './map.model';

export class GeoJsonLines extends GeoJson {
  constructor(lines: any) {
    super();
    this.features = lines.flatMap(
      (line: any) => line.aller_retour === 't' ?
        [new GeoJsonLineFeature(line), new GeoJsonLineFeature(line, true)] :
        new GeoJsonLineFeature(line)
    );
  }

  type: any;
  features: GeoJsonLineFeature[];
}

export class GeoJsonLineFeature extends GeoJsonFeature {
  constructor(geoJsonLine: any, isRetour: boolean = false) {
    super();
    this.type = 'Feature';
    this.geometry = JSON.parse(isRetour ? geoJsonLine.geojson2 : geoJsonLine.geojson1);
    this.properties = new LeafletLine(geoJsonLine, isRetour);
  }

  properties: LeafletLine;
}

export class LeafletLine {
  name: string;
  id: number;
  linetype: string;
  waypoints: any[];
  couleur: string;
  drawMode: string;
  sens: string;

  constructor(leafletLine: any, isRetour: boolean = false) {
    this.name = leafletLine.nom;
    this.id = parseInt(leafletLine.id, 10);
    this.linetype = leafletLine.linetype;
    this.waypoints = JSON.parse(isRetour ? leafletLine.waypoints2 : leafletLine.waypoints);
    this.couleur = leafletLine.couleur;
    this.drawMode = leafletLine.mode;
    this.sens = isRetour ? 'retour' : 'aller';
  }
}
