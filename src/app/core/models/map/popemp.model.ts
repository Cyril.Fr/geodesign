import {GeoJson, GeoJsonFeature} from './map.model';

export class GeoJsonStopInfos extends GeoJson {
  constructor(stopInfos: any) {
    super();
    this.features = stopInfos.map(
      (stopInfo: any) => new GeoJsonStopInfosFeature(stopInfo)
    );
  }

  type: string;
  features: GeoJsonStopInfosFeature[];
}

export class GeoJsonStopInfosFeature extends GeoJsonFeature {
  constructor(geoJsonStopInfos: any) {
    super();
    this.type = 'Feature';
    this.geometry = JSON.parse(geoJsonStopInfos.geojson);
    this.properties = new LeafletStopInfos(geoJsonStopInfos);
  }

  properties: LeafletStopInfos;
}

export class LeafletStopInfos {
  id: number;
  name: string;
  pop: number;
  emp: number;

  constructor(props) {
    this.id = parseInt(props.id, 10);
    this.name = props.name;
    this.emp = parseInt(props.emp300, 10);
    this.pop = parseInt(props.pop300, 10);
  }
}
