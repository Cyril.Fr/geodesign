import {GeoJson, GeoJsonFeature} from './map.model';

export class GeoJsonEmitAndGen extends GeoJson {
  constructor(emitAndGen: any) {
    super();
    if (emitAndGen && emitAndGen.length > 0) {
      this.features = emitAndGen.map((gen: any) => new GeoJsonEmitAndGenFeature(gen));
    }
  }
}

export class GeoJsonEmitAndGenFeature extends GeoJsonFeature {
  type: string;
  properties: LeafletEmitAndGen;

  constructor(emitAndGen: any) {
    super();
    this.type = 'Feature';
    this.geometry = JSON.parse(emitAndGen.geojson);
    this.properties = new LeafletEmitAndGen(emitAndGen);
  }
}

export class LeafletEmitAndGen {
  id: number;
  type: string;
  taille: number;
  projet: string;

  constructor(emitAndGen: any) {
    this.id = parseInt(emitAndGen.id, 10);
    this.type = emitAndGen.type;
    this.taille = parseInt(emitAndGen.taille, 10);
    this.projet = emitAndGen.projet;
  }
}
