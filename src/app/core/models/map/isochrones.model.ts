import {GEO_JSON_ISOCHRONES_IDX} from '../../constants/panel-data.const';
import {GeoJson, GeoJsonFeature} from './map.model';

export class GeoJsonIsochrones extends GeoJson {
  constructor(isochrone: any) {
    super();
    this.features = GEO_JSON_ISOCHRONES_IDX.map((idx: number) => new GeoJsonIsochronesFeature(isochrone, idx));
  }
}

export class GeoJsonIsochronesFeature extends GeoJsonFeature {
  constructor(isochrone: any, index: number) {
    super();
    this.type = 'Feature';
    const key: string = `geom${index < 2 ? '' : index.toString()}`;
    this.geometry = JSON.parse(isochrone[key]);
    this.properties = new LeafletIsochrones(index);
  }
}

export class LeafletIsochrones {
  isochroneIndex: number;

  constructor(index: number) {
    this.isochroneIndex = index;
  }
}
