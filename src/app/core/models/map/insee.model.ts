import {GeoJson, GeoJsonFeature} from './map.model';

export class GeoJsonInsee extends GeoJson {
  constructor(insees: any) {
    super();
    this.type = 'FeatureCollection';
    this.features = insees.map((insee: any) => new GeoJsonInseeFeature(insee));
  }
}

export class GeoJsonInseeFeature extends GeoJsonFeature {
  constructor(insee: any) {
    super();
    this.type = 'Feature';
    this.geometry = JSON.parse(insee.geom);
    this.properties = new LeafletInsee(insee);
  }
}

export class LeafletInsee {
  e: number;
  p: number;

  constructor(insee: any) {
    this.p = parseInt(insee.p, 10);
    this.e = parseInt(insee.e, 10);
  }
}
