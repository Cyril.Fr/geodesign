import {FeatureGeometry, GeoJson, GeoJsonFeature} from './map.model';

export class GeoJsonCovers extends GeoJson {
  constructor(featureGeometry: FeatureGeometry) {
    super();
    this.features = [new GeoJsonCoversFeature(featureGeometry)];
  }
}

export class GeoJsonCoversFeature extends GeoJsonFeature {
  constructor(featureGeometry: FeatureGeometry) {
    super();
    this.type = 'Feature';
    this.geometry = featureGeometry;
  }
}
