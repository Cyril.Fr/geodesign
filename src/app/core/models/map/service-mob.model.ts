import {GeoJson, GeoJsonFeature} from './map.model';

export class GeoJsonServiceMob extends GeoJson {
  constructor(serviceMobs: any) {
    super();
    if (serviceMobs) {
      this.features = serviceMobs.map((serviceMob: any) => new GeoJsonServiceMobFeature(serviceMob));
    }
  }
}

export class GeoJsonServiceMobFeature extends GeoJsonFeature {
  constructor(serviceMob: any) {
    super();
    this.type = 'Feature';
    this.geometry = JSON.parse(serviceMob.geojson);
    this.properties = new LeafletServiceMob(serviceMob);
  }
}

export class LeafletServiceMob {
  id: number;
  type: string;
  level: number;

  constructor(serviceMob: any) {
    this.id = parseInt(serviceMob.id, 10);
    this.type = serviceMob.type;
    this.level = parseInt(serviceMob.level, 10);
  }
}
