import {GeoJson, GeoJsonFeature} from './map.model';

export class GeoJsonStop extends GeoJson {
  constructor(stops: any, allStops?: GeoJsonStop) {
    super();
    if (Array.isArray(stops)) {
      this.features = stops.map(
        (stop: any) => allStops ?
          new GeoJsonStopFeature(stop, allStops.features.find((el: GeoJsonStopFeature) => el.properties.id === parseInt(stop.id, 10)))
          : new GeoJsonStopFeature(stop)
      );
    }
  }
}

export class GeoJsonStopFeature extends GeoJsonFeature {
  constructor(geoJsonStop: any, geoJsonStopFeature?: GeoJsonStopFeature) {
    super();
    this.type = 'Feature';
    this.geometry = geoJsonStopFeature ? geoJsonStopFeature.geometry : JSON.parse(geoJsonStop.geojson);
    this.properties = new LeafletStop(geoJsonStop);
  }

  properties: LeafletStop;
}

export class LeafletStop {
  name: string;
  id: number;
  color: string;

  constructor(props) {
    this.name = props.name;
    this.id = parseInt(props.id, 10);
    this.color = props.color || '#FFFFFF';
  }
}
