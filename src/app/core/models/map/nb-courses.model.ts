import {GeoJson, GeoJsonFeature} from './map.model';
import {LineUtilService} from '../../services/line-util.service';
import {Line} from '../line.model';

export class GeoJsonNbCourses extends GeoJson {
  constructor(nbCourses: any) {
    super();
    this.features = nbCourses.map((nc: any) => new GeoJsonNbCoursesFeature(nc));
  }
}

export class GeoJsonNbCoursesFeature extends GeoJsonFeature {
  constructor(nbCourses: any) {
    super();
    this.type = 'Feature';
    this.geometry = JSON.parse(nbCourses.st_asgeojson);
    this.properties = new LeafletNbCourses(nbCourses);
  }

  properties: LeafletNbCourses;
}

export class LeafletNbCourses {
  id: number;
  name: string;
  nbCourses: number;

  constructor(nbCourses: any) {
    this.id = parseInt(nbCourses.id, 10);
    this.name = nbCourses.name;
    this.nbCourses = this.calculateNbCourses(nbCourses);
  }

  private calculateNbCourses(nbCourses: any): number {
    const operatingInfos: any = JSON.parse(nbCourses.operating_infos).map((ope: any) => JSON.parse(ope));
    const modeCourse: boolean = nbCourses.mode_course === 't';
    if (modeCourse) {
      return nbCourses.course[0] || 0;
    } else {
      return operatingInfos.reduce(
        (accNbCourses: number, operatingInfo: any) => accNbCourses +
          LineUtilService.getNbCourseFromPeriod({operating_info: operatingInfo} as Line, Object.keys(operatingInfo)[0]), 0);
    }
  }
}
