import {GeoJson, GeoJsonFeature} from './map.model';

export class GeoJsonMouvements extends GeoJson {
  constructor(mouvements: any) {
    super();
    if (mouvements) {
      this.features = mouvements.map((mv: any) => new GeoJsonMouvementFeature(mv));
    }
  }
}

export class GeoJsonMouvementFeature extends GeoJsonFeature {
  constructor(mouvement: any) {
    super();
    this.type = 'Feature';
    this.geometry = JSON.parse(mouvement.geojson);
    this.properties = new LeafletMouvement(mouvement);
  }
}

export class LeafletMouvement {
  name: string;
  id: number;
  montee: number;
  desc: number;
  total: number;

  constructor(mouvement: any) {
    this.name = mouvement.name;
    this.id = parseInt(mouvement.id, 10);
    this.montee = parseInt(mouvement.montee, 10);
    this.desc = parseInt(mouvement.descente, 10);
    this.total = this.desc + this.montee;
  }
}
