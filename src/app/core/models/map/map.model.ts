export class GeoJson {
  type: any = 'Feature';
  features: GeoJsonFeature[];
}

export class GeoJsonFeature {
  type: any;
  geometry: FeatureGeometry;
  properties: any;
}

export interface FeatureGeometry {
  type: any;
  coordinates: any;
}
