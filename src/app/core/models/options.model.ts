export interface Options {
  directory?: string;
  suboptions: { label: string, value: string }[];
}
