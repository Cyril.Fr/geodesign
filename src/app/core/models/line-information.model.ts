export class LineInformation {
  name?: string;
  length: number;
  roundTripTime: number;
  busNeed: number;
  commercialKm: number;
  pop?: number;
}
