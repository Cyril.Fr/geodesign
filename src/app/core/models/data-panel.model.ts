export class Option {
  label: string;
  value: string;
}

export class StopOption extends Option {
  constructor() {
    super();
  }
  display: boolean;
  add: boolean;
  load: boolean;
  param: boolean;
}
