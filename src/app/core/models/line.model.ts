import {PartielForLine} from './partiel-for-line.model';

export class Line {
  id: number;
  abbr: string;
  nom: string;
  'operating_info': any;
  speed: number;
  longueur: number;
  longueur2: number;
  couleur: string;
  count: number;
  'aller_retour': string;
  linetype: string;
  modecourse: boolean;
  course: number[];
  ordre: string;
  declenchement?: number;
  networkName?: string;
  partiels: PartielForLine[];

  constructor(line: any) {
    this.id = parseInt(line.id, 10);
    this.abbr = line.abbr;
    this.nom = line.nom;
    this.operating_info = JSON.parse(line.operating_info) || null;
    this.speed = parseFloat(line.speed) || 0;
    this.longueur = line.longueur ? parseFloat(line.longueur) : 0;
    this.longueur2 = line.longueur2 ? parseFloat(line.longueur2) : 0;
    this.couleur = line.couleur;
    this.count = parseInt(line.count, 10) || 0;
    this.aller_retour = line.aller_retour;
    this.linetype = line.linetype;
    this.modecourse = line.mode_course === 't';
    this.course = line.course ? JSON.parse(line.course) : null;
    this.ordre = line.ordre;
    this.declenchement = line.declenchement ? parseInt(line.declenchement, 10) : 0;
    this.partiels = [];
    if (line.partiels) {
      JSON.parse(line.partiels).forEach((partiel: string) => {
        this.partiels.push(new PartielForLine(JSON.parse(partiel)));
      });
    }
  }
}
