import {Component, OnInit} from '@angular/core';
import {HEADER_LINK} from '../constants/header-link';
import {LANGUAGES} from '../constants/languages';
import {FormControl, FormGroup} from '@angular/forms';
import {LanguagesService} from '../services/languages.service';
import {ApiService} from '../services/api.service';
import {DialogService} from '../services/dialog.service';
import {ModifyNetworkDialogComponent} from './dialogs/modify-network-dialog/modify-network-dialog.component';
import {InstanceService} from '../services/instance.service';
import {NetworkService} from '../services/network.service';

@Component({
  selector: 'geodesign-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  headerLinks: any[] = HEADER_LINK;
  languages: any[] = LANGUAGES;
  instance: string = InstanceService.instance;
  network: string = NetworkService.getNetworkName();

  languageFormGroup: FormGroup = new FormGroup({
    language: new FormControl(''),
  });

  constructor(
    private languageService: LanguagesService,
    private apiService: ApiService,
    private dialogService: DialogService) {
  }

  ngOnInit(): void {
    this.languageFormGroup.get('language').valueChanges.subscribe((res: string) => {
      this.languageService.changeLanguage(res);
    });
  }

  showEditNetworkDialog(): void {
    this.dialogService.show(ModifyNetworkDialogComponent).subscribe();
  }

}
