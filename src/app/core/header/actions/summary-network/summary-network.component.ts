import {LoggerService} from '../../../services/logger.service';
import {LineUtilService} from '../../../services/line-util.service';
import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {LineService} from 'src/app/core/services/line.service';
import {ParametersService} from 'src/app/core/services/parameters.service';
import {ParamsCommun} from 'src/app/core/models/params-commun.model';
import {Line} from 'src/app/core/models/line.model';
import {InfoPopulation} from 'src/app/core/models/info-population.model';
import {NetworkService} from 'src/app/core/services/network.service';
import {InfoPopType} from 'src/app/core/models/info-pop-type.model';
import {LineInformation} from '../../../models/line-information.model';
import {DialogService} from '../../../services/dialog.service';
import {GraphParcComponent} from '../../dialogs/graph-parc/graph-parc.component';

@Component({
  selector: 'geodesign-summary-network',
  templateUrl: './summary-network.component.html',
  styleUrls: ['./summary-network.scss'],
})
export class SummaryNetworkComponent implements OnInit {
  buses: number = 0;
  @Output() busesEvent: EventEmitter<number> = new EventEmitter();
  commercialKm: number = 0;
  pop: number = 0;
  popPercent: number = 0;
  popSco: number = 0;
  pop65: number = 0;
  emp: number = 0;
  train: number = 0;
  metro: number = 0;
  essentielle: number = 0;
  principale: number = 0;
  locale: number = 0;
  horsPerimetre: number = 0;
  trainPercent: number = 0;
  metroPercent: number = 0;
  essentiellePercent: number = 0;
  principalePercent: number = 0;
  localePercent: number = 0;
  horsPerimetrePercent: number = 0;

  showCommercialKm: boolean = false;
  showPop: boolean = false;
  showPopSco: boolean = false;
  showPop65: boolean = false;
  showEmp: boolean = false;
  showTrain: boolean = false;
  showMetro: boolean = false;
  showEssentielle: boolean = false;
  showPrincipale: boolean = false;
  showLocale: boolean = false;
  showHorsPerimetre: boolean = false;

  constructor(private lineService: LineService,
              private parametersService: ParametersService,
              private logger: LoggerService,
              private dialogService: DialogService) {
  }

  ngOnInit(): void {
    if (ParametersService.PARAMS_UO) {
      this.showInformationFromParams(ParametersService.PARAMS_UO);
    }
    this.parametersService.paramsUo$.subscribe((paramsUo: ParamsCommun) => {
      if (paramsUo) {
        this.showCommercialKm = false;
        this.showInformationFromParams(paramsUo);
      }
    });
    if (ParametersService.PARAMS_SD) {
      this.showInformationFromParams(ParametersService.PARAMS_SD);
    }
    this.parametersService.paramsSd$.subscribe((paramsSd: ParamsCommun) => {
      if (paramsSd) {
        this.showPop = false;
        this.showPopSco = false;
        this.showPop65 = false;
        this.showEmp = false;
        this.showInformationFromParams(paramsSd);
      }
    });

    this.parametersService.reloadLineInformation$.subscribe(() => this.calcStat(LineService.currentLines));

  }

  showInformationFromParams(params: ParamsCommun): void {
    params.value.split(', ').forEach((value: string) => {
      switch (value) {
        case 'nbKm':
          this.showCommercialKm = true;
          break;
        case 'pop':
          this.showPop = true;
          break;
        case 'popSco':
          this.showPopSco = true;
          break;
        case 'pop65':
          this.showPop65 = true;
          break;
        case 'emp':
          this.showEmp = true;
          break;
      }
    });
  }

  async calcStat(lines: Line[]): Promise<void> {
    this.getInformationLine(lines);
    await this.getPopulation();
    await this.getPopulationForLineType();
  }

  getInformationLine(lines: Line[]): void {
    this.buses = 0;
    lines.forEach((line: Line) => {
      const informationLine: LineInformation = this.lineService.getInformationLine(line);
      this.buses += informationLine.busNeed;
      this.commercialKm += informationLine.commercialKm;
      this.busesEvent.emit(this.buses);
    });
  }

  async getPopulation(): Promise<void> {
    try {
      const infoPop: InfoPopulation = await this.lineService.getInfoPopTot({
        schema: NetworkService.getNetworkName(),
      }).toPromise();
      this.pop = infoPop.pop;
      this.popPercent = this.pop / NetworkService.NETWORK_POP;
      this.popSco = infoPop.pop_sco;
      this.pop65 = infoPop.pop65;
      this.emp = infoPop.emp;
    } catch (e) {
      this.logger.error(e);
    }
  }

  async getPopulationForLineType(): Promise<void> {
    try {
      const infoPopType: InfoPopType[] = await this.lineService.getInfoPopType({
        schema: NetworkService.getNetworkName(),
      }).toPromise();
      infoPopType.forEach((element: any) => {
        switch (element.linetype) {
          case 'train':
            this.showTrain = true;
            this.train = element.pop;
            this.trainPercent = this.train / NetworkService.NETWORK_POP;
            break;
          case 'metro':
            this.showMetro = true;
            this.metro = element.pop;
            this.metroPercent = this.metro / NetworkService.NETWORK_POP;
            break;
          case 'essentielle':
            this.showEssentielle = true;
            this.essentielle = element.pop;
            this.essentiellePercent = this.essentielle / NetworkService.NETWORK_POP;
            break;
          case 'principale':
            this.showPrincipale = true;
            this.principale = element.pop;
            this.principalePercent = this.principale / NetworkService.NETWORK_POP;
            break;
          case 'locale':
            this.showLocale = true;
            this.locale = element.pop;
            this.localePercent = this.locale / NetworkService.NETWORK_POP;
            break;
          case 'hors_perimetre':
            this.showHorsPerimetre = true;
            this.horsPerimetre = element.pop;
            this.horsPerimetrePercent = this.horsPerimetre / NetworkService.NETWORK_POP;
            break;
        }
      });
    } catch (e) {
      this.logger.error(e);
    }
  }

  displayGraphParc(): void {
    this.dialogService.show(GraphParcComponent);
  }
}
