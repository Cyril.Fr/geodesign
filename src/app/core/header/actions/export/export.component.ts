import {Component, OnInit} from '@angular/core';
import {NetworkService} from '../../../services/network.service';
import {ApiService} from '../../../services/api.service';
import {LoggerService} from '../../../services/logger.service';
import {DownloadService} from '../../../services/download.service';
import * as jsPDF from 'jspdf';
import {Thermometer} from '../../../models/thermometer.model';
import {ExportService} from '../../../services/export.service';
import {PdfService} from '../../../services/pdf.service';
import * as _ from 'lodash';
import {LineService} from '../../../services/line.service';
import {Line} from '../../../models/line.model';
import {LineUtilService} from '../../../services/line-util.service';
import {TranslateService} from '@ngx-translate/core';
import {InfoPopulation} from '../../../models/info-population.model';
import {ParametersService} from '../../../services/parameters.service';
import {NbJours} from '../../../models/nb-jours.model';
import {OperatingInfo} from '../../../models/operatingInfo.model';
import {InstanceService} from '../../../services/instance.service';
import {LineInformation} from '../../../models/line-information.model';

@Component({
  selector: 'geodesign-export',
  templateUrl: './export.component.html',
})
export class ExportComponent implements OnInit {

  isKMLLoaded: boolean = false;
  isSIGLoaded: boolean = false;
  isCSVLoaded: boolean = false;
  isChainageLoaded: boolean = false;
  isGTFSLoaded: boolean = false;
  isThermLoaded: boolean = false;

  constructor(private networkService: NetworkService,
              private lineService: LineService,
              private apiService: ApiService,
              private logger: LoggerService,
              private downloadService: DownloadService,
              private exportService: ExportService,
              private pdfService: PdfService,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }

  async exportToKML(e): Promise<void> {
    e.stopPropagation();
    this.isKMLLoaded = true;
    const schema: string = NetworkService.getNetworkName();
    try {
      await this.networkService.exportToKML({schema}).toPromise();
      await this.exportService.exportAndDownload(
        `${schema.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}.kml`,
        `${InstanceService.instance}/export/${schema.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}.kml`,
        'application/vnd.google-earth.kml+xml');
    } catch (e) {
      this.logger.error(e);
    }
    this.isKMLLoaded = false;
  }

  async exportSIG(e): Promise<void> {
    e.stopPropagation();
    this.isSIGLoaded = true;
    const schema: string = NetworkService.getNetworkName();
    try {
      await this.networkService.exportToGIS({schema}).toPromise();
      await this.exportService.exportAndDownload(
        'GIS_export.zip',
        `${InstanceService.instance}/export/GIS_export.zip`,
        'application/zip');
    } catch (e) {
      this.logger.error(e);
    }
    this.isSIGLoaded = false;
  }

  async exportToCsv(e): Promise<void> {
    e.stopPropagation();
    this.isCSVLoaded = true;
    const schema: string = NetworkService.getNetworkName();
    let data: string = '';
    const translation: any = this.translateService.instant([
      'LINE_INFO.NAME',
      'LINE_INFO.LONGUEUR',
      'LINE_INFO.TIME',
      'LINE_INFO.PARK',
      'LINE_INFO.COMMERCIAL_KM',
      'LINE_INFO.ACCESSIBLE_POP',
      'LINE_INFO.ACCESSIBLE_SCHOOL_POP',
      'LINE_INFO.65_POP',
      'LINE_INFO.ACCESSIBLE_EMPLOYMENT',
    ]);
    Object.values(translation).forEach((translate: string, index: number) => {
      data += translate;
      (index < Object.values(translation).length - 1) ? data += ';' : data += '\n';
    });
    try {
      // TODO Replace with the real buffer when parameters will be ready
      const infoPop: InfoPopulation[] = await this.lineService.getInfoPop({
        buffer: [1000, 700, 400, 300, 300, 500],
        schema,
      }).toPromise();

      LineService.currentLines.forEach((line: Line, index: number) => {
        const informationLine: LineInformation = this.lineService.getInformationLine(line);
        data += line.nom + ';';
        data += informationLine.length + ';';
        data += informationLine.roundTripTime + ';';
        data += informationLine.busNeed + ';';
        data += informationLine.commercialKm + ';';
        data += (infoPop[index] ? infoPop[index].pop : '...') + ';';
        data += (infoPop[index] ? infoPop[index].pop_sco : '...') + ';';
        data += (infoPop[index] ? infoPop[index].pop65 : '...') + ';';
        data += (infoPop[index] ? infoPop[index].emp : '...') + '\n';
      });

      await this.networkService.exportToCsv({schema, data}).toPromise();
      await this.exportService.exportAndDownload(
        `${schema}.csv`,
        `${InstanceService.instance}/export/${schema}.csv`,
        'text/csv;charset=utf-8;');
    } catch (e) {
      this.logger.error(e);
    }

    this.isCSVLoaded = false;
  }

  async exportGTFS(e): Promise<void> {
    e.stopPropagation();
    this.isGTFSLoaded = true;
    const schema: string = NetworkService.getNetworkName();
    try {
      await this.networkService.writeCalendarAgency({schema}).toPromise();
      await this.networkService.writeStops({schema}).toPromise();
      await this.networkService.writeRoutes({schema}).toPromise();
      let content: string = 'route_id,service_id,trip_id,direction_id,shape_id\r\n';
      const lines: Line[] = await this.lineService.getLines({schema}).toPromise();
      lines.forEach((line: Line) => {
        const listPeriod: NbJours[] = ParametersService.NB_JOUR_PERIODE;
        listPeriod.forEach((period: NbJours) => {
          if (line.operating_info[period.periode].length > 0) {
            let nbCourse: number = line.operating_info[period.periode].reduce(
              (accumulator: number, currentValue: OperatingInfo) => accumulator + LineUtilService.calcNbCourse(currentValue), 0);
            nbCourse = nbCourse === 0 ? 0 : nbCourse + 1;
            for (let i: number = 1; i <= nbCourse; i++) {
              content += `${line.id},${period.periode},${line.id}_${period.periode}_0_${i},0,${line.nom}_0\r\n`;
              content += `${line.id},${period.periode},${line.id}_${period.periode}_1_${i},1,${line.nom}_1\r\n`;
            }
          }
        });
      });
      await this.networkService.writeTrips({file: content}).toPromise();
      await this.networkService.writeStopTimes({schema}).toPromise();
      await this.networkService.writeShapes({schema}).toPromise();
      await this.networkService.writeGTFS({schema}).toPromise();
      await this.exportService.exportAndDownload(
        'gtfs.zip',
        `${InstanceService.instance}/export/gtfs.zip`,
        'application/zip');
    } catch (e) {
      this.logger.error(e);
    }
    this.isGTFSLoaded = false;
  }

  async exportChainage(e): Promise<void> {
    e.stopPropagation();
    this.isChainageLoaded = true;
    const schema: string = NetworkService.getNetworkName();
    try {
      await this.networkService.exportToChainage({schema}).toPromise();
      await this.exportService.exportAndDownload(
        `${schema}_chainage.csv`,
        `${InstanceService.instance}/export/${schema}_chainage.csv`,
        'text/csv;charset=utf-8;');
    } catch (e) {
      this.logger.error(e);
    }
    this.isChainageLoaded = false;
  }

  // TODO Refacto one day...
  async exportToTherm(e): Promise<void> {
    e.stopPropagation();
    this.isThermLoaded = true;
    const schema: string = NetworkService.getNetworkName();
    try {
      const therms: Thermometer[] = await this.networkService.getTherm({schema}).toPromise();
      const doc: any = new jsPDF('landscape');
      const stopCorres: any = {};
      therms.forEach((therm: Thermometer) => {
        JSON.parse(therm.arrets).forEach((arret: string) => {
          if (stopCorres[arret]) {
            if (stopCorres[arret].indexOf(therm.abbr) === -1) {
              stopCorres[arret].push(therm.abbr);
            }
          } else {
            stopCorres[arret] = [therm.abbr];
          }
        });
      });

      let skip: boolean = false;
      let index: number = 0;
      therms.forEach((therm: Thermometer, indexTherm: number) => {
        if (skip) {
          skip = false;
        } else {
          let stopListV2: any[] = [];
          const arrets: string[] = JSON.parse(therm.arrets);
          arrets.map((arret: string) => arret === null ? '' : arret);
          if (indexTherm + 1 < therms.length && therms[indexTherm + 1].id === therm.id) {
            const arrets1: string[] = JSON.parse(therms[indexTherm + 1].arrets);
            arrets1.map((arret: string) => arret === null ? '' : arret);
            stopListV2 = this.mergeArrayTherm(arrets, arrets1.reverse());
            skip = true;
          } else {
            arrets.forEach((arret: string) => {
              stopListV2.push([arret, 0]);
            });
          }
          this.pdfService.createPDFLine(
            index,
            therm.abbr,
            this.pdfService.hexToRGBArray(therm.couleur),
            stopListV2,
            therm.nom,
            stopCorres,
            doc
          );
          index++;
        }
      });

      doc.save('thermometres.pdf');

    } catch (e) {
      this.logger.error(e);
    }
    this.isThermLoaded = false;
  }

  // TODO Refacto one day...
  mergeArrayTherm(arr1: any[], arr2: any[]): any[] {
    const arr3: any[] = [];

    arr1.forEach((item: any) => {
      const diffArray: any[] = _.concat(_.difference(arr1, arr2), _.difference(arr2, arr1));
      diffArray.includes(item) ? arr3.push([item, 1]) : arr3.push([item, 3]);
    });

    arr3.map((item: any) => arr2.includes(item[0]) ? item[1] = 0 : item);
    const conArr3: any[] = arr3.map((item: any) => item[0]);

    let test: boolean = true;
    let c: number = 0;
    arr2.forEach((item: any, index: number) => {
      if (!conArr3.includes(item)) {
        const newIndex: number = index + c;
        arr3.splice(newIndex, 0, [arr2[index], 2]);
        test = false;
      } else {
        if (!test || conArr3.indexOf(arr2[index]) > (index + c)) {
          c++;
        }
      }
    });

    return arr3;
  }
}
