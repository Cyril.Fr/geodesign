import {Component, OnInit} from '@angular/core';
import {HELP_ACTIONS, SCENARIO_ACTIONS, SETTINGS_ACTIONS} from '../../constants/actions.const';
import {DialogService} from '../../services/dialog.service';
import {DataSidenavService} from '../../services/data-sidenav.service';

@Component({
  selector: 'geodesign-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss'],
})
export class ActionsComponent implements OnInit {

  actions: { label: string; picto: string, component: any }[] = SCENARIO_ACTIONS;
  actionsSettings: { label: string, component: any }[] = SETTINGS_ACTIONS;
  actionsHelp: { component: any } = HELP_ACTIONS;

  buses: number;

  receiveBuses($event): void {
    this.buses = $event;
  }

  constructor(private dialogService: DialogService,
              private dataSidenavService: DataSidenavService) {
  }

  ngOnInit(): void {
  }

  showDialog(component: any, picto?: string): void {
    this.dialogService.show(component, picto === 'icon-partao_stats' ? {data: {tab: 'networks'}} : {});
  }

  handleDataSidenav(): void {
    this.dataSidenavService.toggle();
  }

  trackByFn(index: number): number {
    return index;
  }
}
