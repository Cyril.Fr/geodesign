import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../services/dialog.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'geodesign-export-network-dialog',
  templateUrl: './export-network-dialog.component.html',
})
export class ExportNetworkDialogComponent implements OnInit, IDialog {

  formGroup: FormGroup = new FormGroup({
    networkToDuplicate: new FormControl('', Validators.required),
    networkNewName: new FormControl('', Validators.required),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ExportNetworkDialogComponent>) {
  }

  ngOnInit(): void {
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.dialogRef.close();
  }

}
