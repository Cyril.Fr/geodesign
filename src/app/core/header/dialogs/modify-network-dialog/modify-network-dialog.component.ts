import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NetworkService} from '../../../services/network.service';
import {ApiService} from '../../../services/api.service';
import {TranslateService} from '@ngx-translate/core';
import {LoggerService} from '../../../services/logger.service';
import {Network} from '../../../models/network.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'geodesign-modify-network-dialog',
  templateUrl: './modify-network-dialog.component.html',
})
export class ModifyNetworkDialogComponent implements OnInit {
  options: string[];

  formGroup: FormGroup = new FormGroup({
    newSchema: new FormControl(NetworkService.getNetworkName(), Validators.required),
    dir: new FormControl(''),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ModifyNetworkDialogComponent>,
    private networkService: NetworkService,
    private apiService: ApiService,
    private translateService: TranslateService,
    private logger: LoggerService) {
  }

  async ngOnInit(): Promise<void> {
    try {
      const networks: Network[] = await this.networkService.getNetworks().toPromise();
      this.options = this.networkService.getUniquesDirectories(networks);
    } catch (e) {
      this.logger.error(e);
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  async onSubmit(): Promise<void> {
    // Handle invalid form submission.
    if (this.formGroup.invalid) {
      return;
    }

    this.formGroup.value.newSchema = this.networkService.trimAndReplaceAndLowerCase(this.formGroup.value.newSchema);
    this.formGroup.value.dir = this.networkService.trimAndReplaceAndLowerCase(this.formGroup.value.dir);
    this.formGroup.value.schema = NetworkService.getNetworkName();
    const res: string = await this.networkService.modifyNetworks(this.formGroup.value).toPromise();

    if (res === 'false') {
      NetworkService.setNetworkName(this.formGroup.value.newSchema);
      sessionStorage.removeItem('history');
      window.location.reload();
      this.dialogRef.close();
    } else {
      this.translateService.get('CREATE_NETWORK.ERROR').subscribe((translation: string) => {
        window.alert(translation);
      });
    }
  }

}
