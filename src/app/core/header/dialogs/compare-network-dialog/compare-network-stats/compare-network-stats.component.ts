import {Component, Input, OnInit} from '@angular/core';
import {NetworkStats} from '../../../../models/network.model';
import {NetworkService} from '../../../../services/network.service';

@Component({
  selector: 'geodesign-compare-network-stats',
  templateUrl: './compare-network-stats.component.html',
  styleUrls: ['./compare-network-stats.component.scss'],
})
export class CompareNetworkStatsComponent implements OnInit {

  @Input() networkStat: NetworkStats;
  @Input() refNetworkStat: NetworkStats;
  comparedStats: NetworkStats = {} as NetworkStats;
  isRefNetwork: boolean;

  constructor(private networkService: NetworkService) {
  }

  ngOnInit(): void {
    this.isRefNetwork = this.refNetworkStat === this.networkStat;
    this.comparedStats = this.networkService.extractComparedStats(this.networkStat, this.refNetworkStat, this.isRefNetwork);
  }

}
