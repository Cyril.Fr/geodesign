import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'geodesign-compare-network-difference-bar',
  templateUrl: './compare-network-difference-bar.component.html',
  styleUrls: ['./compare-network-difference-bar.component.scss'],
})
export class CompareNetworkDifferenceBarComponent implements OnInit {

  @Input() data: number;
  @Input() percent: number;
  get widthPercent(): number {
    const res: number = Math.round(this.percent * 100);
    if (res > 50 || isNaN(res)) {
      // If the percent is more than 50% then max it to 50 because width can't exceed 100%
      return 50;
    }
    if (res < -50) {
      // If the percent is less than -50% then min it to -50 because width can't exceed 100%
      return -50;
    }
    return res;
  }

  @Input() type: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  getWidth(): number {
    if (!this.data) {
      // If no data => percent will be 0 so the bar would be at 50% which is wront its need to be at 0
      return 0;
    }
    // 50% is the basis the percent is added
    return 50 + this.widthPercent;
  }

}
