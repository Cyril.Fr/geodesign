import {Component, Inject, OnInit} from '@angular/core';
import {DialogService, IDialog} from '../../../services/dialog.service';
import {FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NetworkService} from '../../../services/network.service';
import {CompareNetworkResultsComponent} from './compare-network-results/compare-network-results.component';
import {Network} from '../../../models/network.model';
import {maxCheckboxes, minCheckboxes} from '../../../../shared/validators/checkboxes.validator';
import {ImportDataService} from '../../../services/import-data.service';

@Component({
  selector: 'geodesign-compare-network-dialog',
  templateUrl: './compare-network-dialog.component.html',
  styleUrls: ['./compare-network-dialog.component.scss'],
})
export class CompareNetworkDialogComponent implements OnInit, IDialog {
  MAX_NETWORK: number = 4;
  MIN_NETWORK: number = 2;
  formGroup: FormGroup = new FormGroup({}, [minCheckboxes(this.MIN_NETWORK), maxCheckboxes(this.MAX_NETWORK)]);
  private resultComponent: any = CompareNetworkResultsComponent;
  dirNetworks: {};
  networks: Network[] = [];
  selectedNetworks: string[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<CompareNetworkDialogComponent>,
    private importService: ImportDataService,
    private dialogService: DialogService,
    private networkService: NetworkService) {
  }

  async ngOnInit(): Promise<void> {
    this.importService.networks$.subscribe((networks: Network[]) => this.networks = networks);
    for (const network of this.networks) {
      this.formGroup.addControl(network.name, new FormControl(false));
    }
    this.dirNetworks = this.networkService.initializeCompareNetworks(this.networks);
    this.formGroup.valueChanges.subscribe((changes: object) => {
      this.touchForm();
      this.selectedNetworks = this.networkService.extractSelectedNetworks(changes, this.selectedNetworks);
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  touchForm(): void {
    if (this.formGroup.untouched) {
      this.formGroup.markAsTouched();
    }
  }

  onSubmit(): void {
    this.touchForm();
    if (this.formGroup.valid) {
      this.dialogService.show(this.resultComponent, {
        data: {
          networks: this.networks,
          selectedNetworks: this.selectedNetworks,
        },
      });
    }
  }

  trackByFn(index: number): number {
    return index;
  }
}
