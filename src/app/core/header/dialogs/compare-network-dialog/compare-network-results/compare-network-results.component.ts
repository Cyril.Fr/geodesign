import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../../services/dialog.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Network, NetworkStats} from '../../../../models/network.model';
import {NetworkService} from '../../../../services/network.service';

@Component({
  selector: 'geodesign-compare-network-results',
  templateUrl: './compare-network-results.component.html',
  styleUrls: ['./compare-network-results.component.scss'],
})
export class CompareNetworkResultsComponent implements OnInit, IDialog {

  selectedNetworks: object = {};
  networkStats: NetworkStats[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: { networks: Network[], selectedNetworks: string[] },
              public dialogRef: MatDialogRef<CompareNetworkResultsComponent>, private networkService: NetworkService) {
  }

  ngOnInit(): void {
    this.data.selectedNetworks.forEach((networkName: string) =>
      this.selectedNetworks[networkName] = this.data.networks.find((el: Network) => el.name === networkName));
    this.networkStats = this.networkService.extractNetworkStats(this.selectedNetworks);
  }

  onClose(): void {
    this.dialogRef.close();
  }

  trackByFn(index: number): number {
    return index;
  }
}
