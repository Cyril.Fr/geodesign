import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../services/dialog.service';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ParametersService} from '../../../services/parameters.service';
import {NbJours} from '../../../models/nb-jours.model';
import {LoggerService} from '../../../services/logger.service';
import {NetworkService} from '../../../services/network.service';
import {ParamsCommun} from '../../../models/params-commun.model';

@Component({
  selector: 'geodesign-work-units-dialog',
  templateUrl: './work-units-dialog.component.html',
  styleUrls: ['./work-units-dialog.component.scss'],
})
export class WorkUnitsDialogComponent implements OnInit, IDialog {

  totalDay: number = 365;
  selectedDay: number = 0;
  isLoading: boolean = false;
  isError: boolean = false;

  formGroup: FormGroup = new FormGroup({
    calendar: new FormGroup({
      periods: new FormArray([
        new FormGroup({
          periode: new FormControl(''),
          nb: new FormControl('', [Validators.pattern('^[0-9]*$')]),
        }),
        new FormGroup({
          periode: new FormControl(''),
          nb: new FormControl('', [Validators.pattern('^[0-9]*$')]),
        }),
        new FormGroup({
          periode: new FormControl(''),
          nb: new FormControl('', [Validators.pattern('^[0-9]*$')]),
        }),
        new FormGroup({
          periode: new FormControl(''),
          nb: new FormControl('', [Validators.pattern('^[0-9]*$')]),
        }),
        new FormGroup({
          periode: new FormControl(''),
          nb: new FormControl('', [Validators.pattern('^[0-9]*$')]),
        }),
        new FormGroup({
          periode: new FormControl(''),
          nb: new FormControl('', [Validators.pattern('^[0-9]*$')]),
        }),
      ]),
    }),
    params: new FormGroup({
      battementPercent: new FormControl(ParametersService.BATTEMENT_PERCENT,
        [Validators.required, Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')]
      ),
      battementT: new FormControl(ParametersService.BATTEMENT_T, [Validators.required, Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')]),
      coutH: new FormControl(ParametersService.TX_HORAIRE, [Validators.required, Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')]),
      coutKm: new FormControl(ParametersService.TX_KM, [Validators.required, Validators.pattern('^[+-]?([0-9]*[.])?[0-9]+$')]),
    }),
    lineStats: new FormGroup({
      dist: new FormControl(false),
      time: new FormControl(false),
      nbBusMax: new FormControl(false),
      nbKm: new FormControl(false),
    }),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<WorkUnitsDialogComponent>,
    private parametersService: ParametersService,
    private logger: LoggerService) {
  }

  ngOnInit(): void {
    const paramsUO: string[] = ParametersService.PARAMS_UO.value.split(', ');
    paramsUO.forEach((param: string) => {
      if (this.formGroup.get('lineStats').get(param)) {
        this.formGroup.get('lineStats').get(param).patchValue(true);
      }
    });
    this.selectedDay = this.calculateNbDaysSelected(ParametersService.NB_JOUR_PERIODE, 'nb');
    ParametersService.NB_JOUR_PERIODE.forEach((nbJour: NbJours, index: number) => {
      (this.formGroup.get('calendar').get('periods') as FormArray).at(index).get('periode').patchValue(nbJour.periode);
      (this.formGroup.get('calendar').get('periods') as FormArray).at(index).get('nb').patchValue(nbJour.nb);
    });

    this.formGroup.get('calendar').get('periods').valueChanges.subscribe((values: any) => {
      this.selectedDay = this.calculateNbDaysSelected(values, 'nb');
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  async onSubmit(): Promise<void> {
    this.isError = false;
    this.isLoading = true;
    this.formGroup.markAllAsTouched();
    if (!this.formGroup.invalid) {
      let params: any = {};
      params = {
        ...this.formGroup.value.params,
        schema: NetworkService.getNetworkName(),
      };
      this.formGroup.value.calendar.periods.forEach((period: any, index: number) => {
        if (period.periodNbJour !== '' && period.periode !== '') {
          params = {
            ...params,
            [this.transformFormToPeriods(index, true)]: period.periode,
            [this.transformFormToPeriods(index, false)]: period.nb,
          };
        }
      });

      try {
        const paramsUo: string = Object.keys(this.formGroup.value.lineStats).filter(
          (key: string) => this.formGroup.value.lineStats[key] === true
        ).join(', ');
        const currentParamsUo: ParamsCommun = ParametersService.PARAMS_UO;
        currentParamsUo.value = paramsUo;
        await this.parametersService.changeParam(params).toPromise();
        await this.parametersService.changeParamCommun({
          data_sd: ParametersService.PARAMS_SD.value,
          data_uo: paramsUo,
        }).toPromise();
        this.parametersService.sendNbJours(this.formGroup.value.calendar.periods
          .filter((period: NbJours) => period.periode !== '' && period.nb)
          .map((period: NbJours) => new NbJours(period)));
        this.parametersService.sendParamsUo(currentParamsUo);
        ParametersService.saveBattementPercent(this.formGroup.value.params.battementPercent);
        ParametersService.saveBattementT(this.formGroup.value.params.battementT);
        ParametersService.saveTxHoraire(this.formGroup.value.params.coutH);
        ParametersService.saveTxKm(this.formGroup.value.params.coutKm);
        this.parametersService.reloadInformationLine();
        this.dialogRef.close();
      } catch (e) {
        this.logger.error(e);
        this.isError = true;
      }
    }
    this.isLoading = false;
  }

  calculateNbDaysSelected(values: any, key: string): number {
    return values.reduce((accumulator: number, currentValue: NbJours) =>
        currentValue[key] && !isNaN(currentValue[key]) ? accumulator + parseInt(currentValue[key], 10) : accumulator
      , 0);
  }

  // TODO Remove and review PHP
  transformFormToPeriods(period: number, id: boolean): any {
    return id ? `period[_periods][${period}][_id]` : `period[_periods][${period}][_nbJours]`;
  }

  trackByFn(index: number): number {
    return index;
  }
}
