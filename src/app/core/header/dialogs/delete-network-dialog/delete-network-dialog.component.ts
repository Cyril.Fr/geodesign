import {TranslateService} from '@ngx-translate/core';
import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../services/dialog.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NetworkService} from '../../../services/network.service';
import {Network} from '../../../models/network.model';
import {Options} from '../../../models/options.model';
import {LoggerService} from '../../../services/logger.service';

@Component({
  selector: 'geodesign-delete-network-dialog',
  templateUrl: './delete-network-dialog.component.html',
})
export class DeleteNetworkDialogComponent implements OnInit, IDialog {
  options: Options[] = [];
  isError: boolean = false;
  isLoading: boolean = false;

  formGroup: FormGroup = new FormGroup({
    schema: new FormControl('', Validators.required),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DeleteNetworkDialogComponent>,
    private networkService: NetworkService,
    private translateService: TranslateService,
    private logger: LoggerService) {
  }

  ngOnInit(): void {
    this.initializeNetwork();
  }

  onClose(): void {
    this.dialogRef.close();
  }

  async initializeNetwork(): Promise<void> {
    try {
      const networks: Network[] = await this.networkService.getNetworks().toPromise();
      this.options = this.networkService.initializeNetwork(networks);
    } catch (e) {
      this.logger.error(e);
    }
  }

  async onSubmit(): Promise<void> {
    this.formGroup.markAllAsTouched();
    this.isError = false;
    this.isLoading = true;
    if (this.formGroup.valid) {
      this.formGroup.value.schema = this.networkService.trimAndReplaceAndLowerCase(this.formGroup.value.schema);
      try {
        await this.networkService.deleteNetwork(this.formGroup.value).toPromise();
        this.initializeNetwork();
      } catch (e) {
        this.logger.error(e);
        this.isError = true;
      }
    }
    this.isLoading = false;
  }
}
