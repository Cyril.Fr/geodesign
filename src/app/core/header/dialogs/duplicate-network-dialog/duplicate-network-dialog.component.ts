import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../services/dialog.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Network} from '../../../models/network.model';
import {NetworkService} from '../../../services/network.service';
import {LoggerService} from '../../../services/logger.service';
import {Options} from '../../../models/options.model';

@Component({
  selector: 'geodesign-duplicate-network-dialog',
  templateUrl: './duplicate-network-dialog.component.html',
  styleUrls: ['./duplicate-network-dialog.component.scss'],
})
export class DuplicateNetworkDialogComponent implements OnInit, IDialog {

  isError: boolean = false;
  formGroup: FormGroup = new FormGroup({
    schema: new FormControl(null, [Validators.required]),
    schemaCopy: new FormControl('', [Validators.required]),
  });

  options: Options[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DuplicateNetworkDialogComponent>,
    private networkService: NetworkService,
    private logger: LoggerService
  ) {
  }

  async ngOnInit(): Promise<void> {
    try {
      const networks: Network[] = await this.networkService.getNetworks().toPromise();
      this.options = this.networkService.initializeNetwork(networks);
    } catch (e) {
      this.logger.error(e);
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  async onSubmit(): Promise<void> {
    this.isError = false;
    this.formGroup.markAllAsTouched();
    if (this.formGroup.valid) {
      try {
        this.formGroup.value.schemaCopy = this.networkService.trimAndReplaceAndLowerCase(this.formGroup.value.schemaCopy);
        const res: string = await this.networkService.duplicateNetwork(this.formGroup.value).toPromise();

        if (res === 'false') {
          NetworkService.setNetworkName(this.formGroup.value.schema);
          sessionStorage.removeItem('history');
          window.location.reload();
          this.dialogRef.close();
        } else {
          this.isError = true;
        }
      } catch (e) {
        this.logger.error(e);
        this.isError = true;
      }
    }
  }

}
