import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../services/dialog.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NetworkService} from '../../../services/network.service';
import {ApiService} from '../../../services/api.service';
import {TranslateService} from '@ngx-translate/core';
import {Network} from '../../../models/network.model';
import {LoggerService} from '../../../services/logger.service';

@Component({
  selector: 'geodesign-creation-network-dialog',
  templateUrl: './creation-network-dialog.component.html',
  styleUrls: ['./creation-network-dialog.component.scss'],
})
export class CreationNetworkDialogComponent implements OnInit, IDialog {
  options: string[];
  isError: boolean = false;

  formGroup: FormGroup = new FormGroup({
    schema: new FormControl('', Validators.required),
    dir: new FormControl(''),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<CreationNetworkDialogComponent>,
    private networkService: NetworkService,
    private apiService: ApiService,
    private translateService: TranslateService,
    private logger: LoggerService) {
  }

  async ngOnInit(): Promise<void> {
    try {
      const networks: Network[] = await this.networkService.getNetworks().toPromise();
      this.translateService.get('CHANGE_NETWORK.DEFAULT_SELECT').subscribe((res: string) => {
        this.options = this.networkService.getUniquesDirectories(networks);
      });
    } catch (e) {
      this.logger.error(e);
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  async onSubmit(): Promise<void> {
    this.isError = false;
    // Handle invalid form submission.
    this.formGroup.markAllAsTouched();
    if (this.formGroup.invalid) {
      return;
    }

    this.formGroup.value.schema = this.networkService.trimAndReplaceAndLowerCase(this.formGroup.value.schema);
    this.formGroup.value.dir = this.networkService.trimAndReplaceAndLowerCase(this.formGroup.value.dir);
    try {
      const res: string = await this.networkService.createNetwork(this.formGroup.value).toPromise();

      if (res === 'false') {
        NetworkService.setNetworkName(this.formGroup.value.schema);
        sessionStorage.removeItem('history');
        window.location.reload();
        this.dialogRef.close();
      } else {
        this.isError = true;
      }
    } catch (e) {
      this.isError = true;
      this.logger.error(e);
    }
  }

}
