import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../services/dialog.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Network} from '../../../models/network.model';
import {Options} from '../../../models/options.model';
import {NetworkService} from '../../../services/network.service';
import {LoggerService} from '../../../services/logger.service';

@Component({
  selector: 'geodesign-change-network-dialog',
  templateUrl: './change-network-dialog.component.html',
})
export class ChangeNetworkDialogComponent implements OnInit, IDialog {

  formGroup: FormGroup = new FormGroup({
    schema: new FormControl('', Validators.required),
  });

  options: Options[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ChangeNetworkDialogComponent>,
    private networkService: NetworkService,
    private logger: LoggerService
  ) {
  }

  async ngOnInit(): Promise<void> {
    try {
      const networks: Network[] = await this.networkService.getNetworks().toPromise();
      this.options = this.networkService.initializeNetwork(networks);
    } catch (e) {
      this.logger.error(e);
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.formGroup.markAllAsTouched();
    if (this.formGroup.valid) {
      // TODO Save network!!
      NetworkService.setNetworkName(this.formGroup.value.schema);
      sessionStorage.removeItem('history');
      window.location.reload();
      this.dialogRef.close();
    }
  }

}
