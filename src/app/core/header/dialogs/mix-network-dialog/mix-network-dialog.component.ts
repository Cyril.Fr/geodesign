import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../services/dialog.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Network} from '../../../models/network.model';
import {NetworkService} from '../../../services/network.service';
import {LoggerService} from '../../../services/logger.service';
import {TranslateService} from '@ngx-translate/core';
import {ApiService} from '../../../services/api.service';
import {Options} from '../../../models/options.model';
import {LineService} from '../../../services/line.service';
import {Line} from '../../../models/line.model';

@Component({
  selector: 'geodesign-mix-network-dialog',
  templateUrl: './mix-network-dialog.component.html',
  styleUrls: ['./mix-network-dialog.component.scss'],
})
export class MixNetworkDialogComponent implements OnInit, IDialog {

  formGroup: FormGroup = new FormGroup({
    schemaCopy: new FormControl('', Validators.required),
    schema: new FormControl('', Validators.required),
  });

  options: Options[] = [];
  lines: Line[] = [];
  linesSelected: Line[] = [];
  isError: boolean = false;
  isLoading: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<MixNetworkDialogComponent>,
    private networkService: NetworkService,
    private logger: LoggerService,
    private translateService: TranslateService,
    private apiService: ApiService,
    private lineService: LineService
  ) {
  }

  async ngOnInit(): Promise<void> {
    try {
      const networks: Network[] = await this.networkService.getNetworks().toPromise();
      this.options = this.networkService.initializeNetwork(networks);
    } catch (e) {
      this.logger.error(e);
    }

    this.formGroup.get('schemaCopy').valueChanges.subscribe(async (value: any) => {
      if (value) {
        this.lines = await this.lineService.getLines({schema: value}).toPromise();
        this.lines.map((line: Line) => line.networkName = value);
      }
    });
  }

  addLine(line: Line): void {
    this.linesSelected.push(line);
  }

  removeLine(line: Line): void {
    this.linesSelected.splice(this.linesSelected.findIndex((lineObject: Line) => lineObject.id === line.id), 1);
  }

  onClose(): void {
    this.dialogRef.close();
  }

  async onSubmit(): Promise<void> {
    this.isError = false;
    this.isLoading = true;
    this.formGroup.markAllAsTouched();
    if (this.formGroup.valid) {
      // TODO Save network
      try {
        const network: any = await this.networkService.createNetwork(
          {schema: this.networkService.trimAndReplaceAndLowerCase(this.formGroup.value.schema), dir: 'defaut'}
        ).toPromise();

        if (network !== 'false') {
          this.isError = true;
        } else {
          let nbNetwork: number = 0;
          const lineNetwork: any = {};
          this.linesSelected.forEach((line: Line) => {
            if (lineNetwork[line.networkName]) {
              lineNetwork[line.networkName].push(line.id);
            } else {
              lineNetwork[line.networkName] = [line.id];
              nbNetwork++;
            }
          });

          const allRequest: any[] = [];

          for (const [key, value] of Object.entries(lineNetwork)) {
            allRequest.push(this.networkService.mixNetworks({
              schema: this.formGroup.value.schema,
              schemaCopy: key,
              lignes: value,
              nbSchema: nbNetwork,
            }).toPromise());
          }

          Promise.all(allRequest).then(() => {
            NetworkService.setNetworkName(this.formGroup.value.schema);
            sessionStorage.removeItem('history');
            window.location.reload();
            this.dialogRef.close();
          }).catch((e: Error) => {
            this.logger.error(e);
          });
        }
      } catch (e) {
        this.logger.error(e);
        this.isError = true;
      }
    }
    this.isLoading = false;
  }

  trackByFn(index: number): number {
    return index;
  }
}
