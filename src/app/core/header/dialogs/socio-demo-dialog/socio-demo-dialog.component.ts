import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../services/dialog.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ParametersService} from '../../../services/parameters.service';
import {LoggerService} from '../../../services/logger.service';
import {ParamsCommun} from '../../../models/params-commun.model';

@Component({
  selector: 'geodesign-socio-demo-dialog',
  templateUrl: './socio-demo-dialog.component.html',
  styleUrls: ['./socio-demo-dialog.component.scss'],
})
export class SocioDemoDialogComponent implements OnInit, IDialog {

  formGroup: FormGroup = new FormGroup({
    bufferDistance: new FormGroup({
      train: new FormControl(1000),
      underground: new FormControl(700),
      essentialLines: new FormControl(400),
      principalLines: new FormControl(300),
      localLines: new FormControl(300),
      outScope: new FormControl(500),
    }),
    territoryVariable: new FormGroup({
      pop: new FormControl(false),
      popSco: new FormControl(false),
      pop65: new FormControl(false),
      emp: new FormControl(false),
    }),
  });

  isLoading: boolean = false;
  isError: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<SocioDemoDialogComponent>,
    private parametersService: ParametersService,
    private logger: LoggerService) {
  }

  ngOnInit(): void {
    const paramsSD: string[] = ParametersService.PARAMS_SD.value.split(', ');
    paramsSD.forEach((param: string) => {
      this.formGroup.get('territoryVariable').get(param).patchValue(true);
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  async onSubmit(): Promise<void> {
    this.formGroup.markAllAsTouched();
    this.isLoading = true;
    this.isError = false;
    if (!this.formGroup.invalid) {
      const territoryVariable: any = this.formGroup.value.territoryVariable;
      try {
        const paramsSd: string = Object.keys(territoryVariable).filter((key: string) => territoryVariable[key] === true).join(', ');
        const currentParamsSd: ParamsCommun = ParametersService.PARAMS_SD;
        currentParamsSd.value = paramsSd;
        await this.parametersService.changeParamCommun({
          data_sd: paramsSd,
          data_uo: ParametersService.PARAMS_UO.value,
        }).toPromise();
        this.parametersService.sendParamsSd(currentParamsSd);
        this.dialogRef.close();
      } catch (e) {
        this.logger.error(e);
        this.isError = true;
      }
    }

    this.isLoading = false;
  }

}
