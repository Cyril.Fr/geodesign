import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NetworkService} from '../../../services/network.service';
import {ApiService} from '../../../services/api.service';
import {TranslateService} from '@ngx-translate/core';
import {LoggerService} from '../../../services/logger.service';
import {DialogService} from '../../../services/dialog.service';
import {CreationNetworkDialogComponent} from '../creation-network-dialog/creation-network-dialog.component';
import {ChangeNetworkDialogComponent} from '../change-network-dialog/change-network-dialog.component';
import {DuplicateNetworkDialogComponent} from '../duplicate-network-dialog/duplicate-network-dialog.component';

@Component({
  selector: 'geodesign-start-network-dialog',
  templateUrl: './start-network-dialog.component.html',
  styleUrls: ['./start-network-dialog.component.scss'],
})
export class StartNetworkDialogComponent implements OnInit {
  options: string[];

  formGroup: FormGroup = new FormGroup({});

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<StartNetworkDialogComponent>,
    private networkService: NetworkService,
    private apiService: ApiService,
    private translateService: TranslateService,
    private logger: LoggerService,
    private dialogService: DialogService) {
  }

  ngOnInit(): void {
  }

  openCreate(): void {
    this.dialogService.show(CreationNetworkDialogComponent);
  }

  openSwitch(): void {
    this.dialogService.show(ChangeNetworkDialogComponent);
  }

  openDuplicate(): void {
    this.dialogService.show(DuplicateNetworkDialogComponent);
  }
}
