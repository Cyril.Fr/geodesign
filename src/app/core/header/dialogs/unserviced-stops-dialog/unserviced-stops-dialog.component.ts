import {Component, Inject, OnInit} from '@angular/core';
import {IDialog} from '../../../services/dialog.service';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NetworkService} from '../../../services/network.service';
import {StopService} from '../../../services/stop.service';
import {LineService} from '../../../services/line.service';
import {Line} from '../../../models/line.model';
import {Desserve} from '../../../models/desserve.model';
import {LineUtilService} from '../../../services/line-util.service';
import {LoggerService} from '../../../services/logger.service';

@Component({
  selector: 'geodesign-unserviced-stops-dialog',
  templateUrl: './unserviced-stops-dialog.component.html',
  styleUrls: ['./unserviced-stops-dialog.component.scss'],
})
export class UnservicedStopsDialogComponent implements OnInit, IDialog {

  formGroup: FormGroup = new FormGroup({
    desserve: new FormArray([]),
  });

  isLoading: boolean = false;
  optionLines: { label: string, value: number, disabled: boolean }[] = [];
  optionsStops: { label: string, value: number }[][] = [];
  visible: boolean = true;
  selectable: boolean = true;
  lines: Line[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<UnservicedStopsDialogComponent>,
    private networkService: NetworkService,
    private stopService: StopService,
    private lineService: LineService,
    private logger: LoggerService) {
  }

  get desserve(): FormArray {
    return (this.formGroup.get('desserve') as FormArray);
  }

  async ngOnInit(): Promise<void> {
    this.isLoading = true;
    try {
      this.lines = await this.lineService.getLines({schema: NetworkService.getNetworkName()}).toPromise();
      const desserves: Desserve[] = await this.networkService.getDesserve({schema: NetworkService.getNetworkName()}).toPromise();
      this.createOptionLine(desserves.map((desserve: Desserve) => desserve.id_line));
      await this.initializationForm(desserves);
    } catch (e) {
      this.logger.error(e);
    }
    this.isLoading = false;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  async onSubmit(): Promise<void> {
    this.formGroup.markAllAsTouched();
    if (!this.formGroup.invalid) {
      try {
        const stopsInput: any = {};
        this.formGroup.getRawValue().desserve.forEach((value: any, index: number) => {
          stopsInput[index] = value.stops;
        });

        const params: any = {
          schema: NetworkService.getNetworkName(),
          line_input: this.formGroup.getRawValue().desserve.map((line: any) => line.line),
          stops_input: stopsInput,
        };
        await this.stopService.setDesserve(params).toPromise();
        this.dialogRef.close();
      } catch (e) {
        this.logger.error(e);
      }
    }
  }

  addLine(line?: number, stops?: number[], disabled: boolean = false): void {
    this.desserve.push(new FormGroup({
      line: new FormControl({value: line ? line : '', disabled}, [Validators.required]),
      stops: new FormControl(stops ? stops : [], [Validators.required]),
    }));
  }

  removeLine(i: number): void {
    this.desserve.removeAt(i);
    this.optionsStops.slice(i, 1);
    this.createOptionLine(this.formGroup.getRawValue().desserve.map((desserve: any) => desserve.line));
  }

  createOptionLine(desserves: number[]): void {
    this.optionLines = this.lines.map((line: Line) =>
      ({
        label: line.nom, value: line.id, disabled: !!desserves
          .find((desserve: number) => desserve === line.id),
      }));
  }

  async initializationForm(desserves: Desserve[]): Promise<void> {
    await LineUtilService.asyncForEach(desserves, async (desserve: Desserve, index: number) => {
      const stopProx: any = await this.stopService.getStopProxForLine({
        schema: NetworkService.getNetworkName(),
        dataId: desserve.id_line,
      }).toPromise();

      this.optionsStops[index] = stopProx.map((stop: any) => ({label: stop.name, value: parseInt(stop.id, 10)}));
      this.addLine(desserve.id_line, desserve.stops, true);
    });
  }

  async getStopProx(e, index): Promise<void> {
    this.desserve.at(index).get('stops').setValue([]);
    this.createOptionLine(this.formGroup.getRawValue().desserve.map((desserve: any) => desserve.line));
    const stopProx: any = await this.stopService.getStopProxForLine({
      schema: NetworkService.getNetworkName(),
      dataId: e,
    }).toPromise();
    this.optionsStops[index] = stopProx.map((stop: any) => ({label: stop.name, value: parseInt(stop.id, 10)}));
  }

  trackByFn(index: number): number {
    return index;
  }
}
