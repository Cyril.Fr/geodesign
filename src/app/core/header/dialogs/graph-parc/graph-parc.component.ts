import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {IDialog} from '../../../services/dialog.service';
import {ChartDataSets, ChartOptions} from 'chart.js';
import {Label} from 'ng2-charts';
import {TranslateService} from '@ngx-translate/core';
import {LineService} from '../../../services/line.service';
import {Line} from '../../../models/line.model';
import {OperatingInfo} from '../../../models/operatingInfo.model';
import {LineUtilService} from '../../../services/line-util.service';

@Component({
  selector: 'geodesign-graph-parc',
  templateUrl: './graph-parc.component.html',
})
export class GraphParcComponent implements OnInit, IDialog {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<GraphParcComponent>,
    private translateService: TranslateService) {
  }

  public lineChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'right',
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        gridLines: {
          drawBorder: false,
          display: false,
        },
        scaleLabel: {
          display: true,
          labelString: this.translateService.instant('PARC.HOURS'),
        },
      }], yAxes: [{
        scaleLabel: {
          display: true,
          labelString: this.translateService.instant('PARC.BUS_NUMBER'),
        },
      }],
    },
  };
  public lineChartLabels: Label[] = [];
  public lineChartData: ChartDataSets[] = [];

  ngOnInit(): void {
    for (let i: number = 0; i < 25; i++) {
      this.lineChartLabels.push(i.toString());
    }

    const sumPeriodBus: any = [];
    // TODO Refacto and add partiel line
    LineService.currentLines.forEach((line: Line) => {
      Object.entries(line.operating_info).forEach(([key, value]: [string, OperatingInfo[]]) => {
        value.forEach((operatingInfo: OperatingInfo) => {
          if (sumPeriodBus[key]) {
            sumPeriodBus[key] = LineUtilService.getNbBusPerHour(line, operatingInfo).map(
              (nbBus: number, index: number) => Math.ceil(sumPeriodBus[key][index] + nbBus)
            );
          } else {
            sumPeriodBus[key] = LineUtilService.getNbBusPerHour(line, operatingInfo);
          }
        });
      });
    });

    Object.entries(sumPeriodBus).forEach(([key, value]: [string, any]) => {
      this.lineChartData.push({
        data: value,
        label: key,
        fill: false,
      });
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.dialogRef.close();
  }

}
