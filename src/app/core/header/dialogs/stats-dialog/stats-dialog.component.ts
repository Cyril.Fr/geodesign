import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, Sort} from '@angular/material';
import {StopService} from '../../../services/stop.service';
import {NetworkService} from '../../../services/network.service';
import {StopStats} from '../../../models/stop-stats.model';
import {LineUtilService} from '../../../services/line-util.service';
import {ParametersService} from '../../../services/parameters.service';
import {Line} from '../../../models/line.model';
import {Network} from '../../../models/network.model';
import {LineService} from '../../../services/line.service';
import {LineInformation} from '../../../models/line-information.model';
import {InfoPopulation} from '../../../models/info-population.model';
import {LoggerService} from '../../../services/logger.service';

@Component({
  selector: 'geodesign-stats-dialog',
  templateUrl: './stats-dialog.component.html',
  styleUrls: ['./stats-dialog.component.scss'],
})
export class StatsDialogComponent implements OnInit {

  stopsStats: StopStats[] = [];
  networks: Network[] = [];
  linesStats: LineInformation[] = [];
  isLoading: boolean = false;
  selectedTab: number = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<StatsDialogComponent>,
              private stopService: StopService,
              private networkService: NetworkService,
              private lineService: LineService,
              private logger: LoggerService) {
  }

  async ngOnInit(): Promise<void> {
    try {
      if (this.data && this.data.tab) {
        this.selectedTab = this.data.tab === 'lines' ? 1 : this.data.tab === 'networks' ? 2 : 0;
      }
      this.isLoading = true;
      const res: any = await this.stopService.getInfoStopsStats({
        schema: NetworkService.getNetworkName(),
      }).toPromise();

      // TODO Replace with the real buffer when parameters will be ready
      const infoPops: InfoPopulation[] = await this.lineService.getInfoPop({
        buffer: [1000, 700, 400, 300, 300, 500],
        schema: NetworkService.getNetworkName(),
      }).toPromise();

      this.linesStats = LineService.currentLines.map((line: Line) => {
        const infoPopFound: InfoPopulation = infoPops.find((infoPop: InfoPopulation) => line.id === infoPop.id);
        return {
          ...this.lineService.getInformationLine(line),
          name: line.nom,
          pop: infoPopFound ? infoPopFound.pop : 0,
        };
      });
      this.networks = await this.networkService.getNetworksInfos().toPromise();
      this.stopsStats = this.calcNbCourseForStops(res.stopStats, res.lineStats);
    } catch (e) {
      this.logger.error(e);
    }
    this.isLoading = false;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  sortStopsData(sort: Sort): number {
    if (!sort.active || sort.direction === '') {
      return;
    }

    this.stopsStats = this.stopsStats.sort((a: StopStats, b: StopStats) => {
      const isAsc: boolean = sort.direction === 'asc';
      switch (sort.active) {
        case 'name':
          return this.compare(a.name, b.name, isAsc);
        case 'nbCourse':
          return this.compare(a.nbCourse, b.nbCourse, isAsc);
        case 'sum':
          return this.compare(a.sum, b.sum, isAsc);
        default:
          return 0;
      }
    });
  }

  sortLineData(sort: Sort): number {
    if (!sort.active || sort.direction === '') {
      return;
    }

    this.linesStats = this.linesStats.sort((a: LineInformation, b: LineInformation) => {
      const isAsc: boolean = sort.direction === 'asc';
      switch (sort.active) {
        case 'name':
          return this.compare(a.name, b.name, isAsc);
        case 'length':
          return this.compare(a.length, b.length, isAsc);
        case 'roundTrip':
          return this.compare(a.roundTripTime, b.roundTripTime, isAsc);
        case 'vehicule':
          return this.compare(a.busNeed, b.busNeed, isAsc);
        case 'comKm':
          return this.compare(a.commercialKm, b.commercialKm, isAsc);
        default:
          return 0;
      }
    });
  }

  sortNetworkData(sort: Sort): number {
    if (!sort.active || sort.direction === '') {
      return;
    }

    this.networks = this.networks.sort((a: Network, b: Network) => {
      const isAsc: boolean = sort.direction === 'asc';
      switch (sort.active) {
        case 'name':
          return this.compare(a.name, b.name, isAsc);
        case 'nbLines':
          return this.compare(a.ligne, b.ligne, isAsc);
        case 'pop':
          return this.compare(a.pop, b.pop, isAsc);
        case 'km':
          return this.compare(a.km, b.km, isAsc);
        case 'parc':
          return this.compare(a.parc, b.parc, isAsc);
        default:
          return 0;
      }
    });
  }

  calcNbCourseForStops(stopStats: StopStats[], lineStats: Line[]): StopStats[] {
    return stopStats.map((stopStat: StopStats) => {
      return {
        ...stopStat,
        nbCourse: lineStats.reduce((accumulator: number, currentValue: Line) =>
            stopStat.id === currentValue.id ?
              accumulator + LineUtilService.getNbCourseFromPeriod(currentValue, ParametersService.NB_JOUR_PERIODE[0].periode) :
              accumulator
          , 0),
      };
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean): number {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  trackByFn(index: number): number {
    return index;
  }
}
