import { SummaryNetworkComponent } from './header/actions/summary-network/summary-network.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {TranslateModule} from '@ngx-translate/core';
import {LogoComponent} from './header/logo/logo.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ActionsComponent} from './header/actions/actions.component';
import {CreationNetworkDialogComponent} from './header/dialogs/creation-network-dialog/creation-network-dialog.component';
import {MixNetworkDialogComponent} from './header/dialogs/mix-network-dialog/mix-network-dialog.component';
import {DuplicateNetworkDialogComponent} from './header/dialogs/duplicate-network-dialog/duplicate-network-dialog.component';
import {DeleteNetworkDialogComponent} from './header/dialogs/delete-network-dialog/delete-network-dialog.component';
import {ChangeNetworkDialogComponent} from './header/dialogs/change-network-dialog/change-network-dialog.component';
import {CompareNetworkDialogComponent} from './header/dialogs/compare-network-dialog/compare-network-dialog.component';
import {ExportNetworkDialogComponent} from './header/dialogs/export-network-dialog/export-network-dialog.component';
import {WorkUnitsDialogComponent} from './header/dialogs/work-units-dialog/work-units-dialog.component';
import {SocioDemoDialogComponent} from './header/dialogs/socio-demo-dialog/socio-demo-dialog.component';
import {UnservicedStopsDialogComponent} from './header/dialogs/unserviced-stops-dialog/unserviced-stops-dialog.component';
import {HelpDialogComponent} from './header/dialogs/help-dialog/help-dialog.component';
import {MatButtonModule} from '@angular/material/button';
import {
  CompareNetworkResultsComponent
} from './header/dialogs/compare-network-dialog/compare-network-results/compare-network-results.component';
import {CompareNetworkStatsComponent} from './header/dialogs/compare-network-dialog/compare-network-stats/compare-network-stats.component';
import {CoutTotalPipe} from './pipes/cout-total.pipe';
import {KmTotalPipe} from './pipes/km-total.pipe';
import {
  CompareNetworkDifferenceBarComponent
} from './header/dialogs/compare-network-dialog/compare-network-difference-bar/compare-network-difference-bar.component';
import {ExportComponent} from './header/actions/export/export.component';
import {ModifyNetworkDialogComponent} from './header/dialogs/modify-network-dialog/modify-network-dialog.component';
import {StartNetworkDialogComponent} from './header/dialogs/start-network-dialog/start-network-dialog.component';
import {AngularMaterialModule} from '../angular-material.module';
import {CustomPercentPipe} from './pipes/custom-percent.pipe';
import { HelpComponent } from './header/help/help.component';
import { GraphParcComponent } from './header/dialogs/graph-parc/graph-parc.component';
import {ChartsModule} from 'ng2-charts';
import { StatsDialogComponent } from './header/dialogs/stats-dialog/stats-dialog.component';

@NgModule({
  declarations: [
    HeaderComponent,
    LogoComponent,
    ActionsComponent,
    CreationNetworkDialogComponent,
    MixNetworkDialogComponent,
    DuplicateNetworkDialogComponent,
    DeleteNetworkDialogComponent,
    ChangeNetworkDialogComponent,
    CompareNetworkDialogComponent,
    ExportNetworkDialogComponent,
    WorkUnitsDialogComponent,
    SocioDemoDialogComponent,
    UnservicedStopsDialogComponent,
    SocioDemoDialogComponent,
    UnservicedStopsDialogComponent,
    WorkUnitsDialogComponent,
    HelpDialogComponent,
    ExportComponent,
    ModifyNetworkDialogComponent,
    StartNetworkDialogComponent,
    CompareNetworkResultsComponent,
    CompareNetworkStatsComponent,
    CoutTotalPipe,
    KmTotalPipe,
    CompareNetworkDifferenceBarComponent,
    CustomPercentPipe,
    CoutTotalPipe,
    SummaryNetworkComponent,
    HelpComponent,
    GraphParcComponent,
    StatsDialogComponent,
  ],
  exports: [
    HeaderComponent,
    CreationNetworkDialogComponent,
    MixNetworkDialogComponent,
    DuplicateNetworkDialogComponent,
    DeleteNetworkDialogComponent,
    ChangeNetworkDialogComponent,
    CompareNetworkDialogComponent,
    ExportNetworkDialogComponent,
    SocioDemoDialogComponent,
    UnservicedStopsDialogComponent,
    WorkUnitsDialogComponent,
    ModifyNetworkDialogComponent,
    StartNetworkDialogComponent,
    GraphParcComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    AngularMaterialModule,
    ChartsModule,
  ],
  entryComponents: [
    CreationNetworkDialogComponent,
    MixNetworkDialogComponent,
    DuplicateNetworkDialogComponent,
    DeleteNetworkDialogComponent,
    ChangeNetworkDialogComponent,
    CompareNetworkDialogComponent,
    ExportNetworkDialogComponent,
    SocioDemoDialogComponent,
    UnservicedStopsDialogComponent,
    WorkUnitsDialogComponent,
    HelpDialogComponent,
    ModifyNetworkDialogComponent,
    StartNetworkDialogComponent,
    CompareNetworkResultsComponent,
    GraphParcComponent,
    StatsDialogComponent,
  ],
})
export class CoreModule {
}
